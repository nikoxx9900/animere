<?php
session_start();
?>
<!DOCTYPE html>
<html>
<body>

<?php 
try{  
  session_unset(); 
  session_destroy();
  setcookie('id_pc', null, -1, '/'); 
  setcookie('auth_hash', null, -1, '/');
  header('location: index.php?sesion=cerrada');
}catch (Exception $e) {
  echo "linea" . $e->getLine();
}	
  
?>

</body>
</html>