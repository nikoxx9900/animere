<form name='frmSearch' action='' method='post'>
    <div class="container">
        <div class="row title justify-content-center">
            <h3 class="mt-2"><i class="fas fa-search"></i> Explorar Series</h3>
        </div>
        <div class="row anime-grid justify-content-sm-center">
            <?php
            if (!empty($result)) {
                foreach ($result as $crow) {
                    $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
                    if ($crow['estado1'] == "Finalizado") {
                        $color_info = "dc3545";
                    } else {
                        $color_info = "28a745";
                    }
                    $fechaFormatInit_aPage1 = $crow['StrFechaEstreno'];
                    $fechaFormat_aPage1 = date("d/m/Y", strtotime($fechaFormatInit_aPage1));
                    setlocale(LC_ALL, "es_ES");
                    $string = $fechaFormat_aPage1;
                    $date = DateTime::createFromFormat("d/m/Y", $string);
                    $fechaFormat_aPage = strftime("%b %Y", $date->getTimestamp());
                    if ($crow['tipo'] == 0) {
                        $tipoA = "TV";
                    } else if ($crow['tipo'] == 1) {
                        $tipoA = "película";
                    } else if ($crow['tipo'] == 2) {
                        $tipoA = "OVA";
                    } else if ($crow['tipo'] == 3) {
                        $tipoA = "ONA";
                    } else {
                        $tipoA = "Especial";
                    }

                    ?>
                    <div class="col-are-3 anime-card m-1">
                        <div class="card">
                            <a href="../../<?php echo $pageVersion1; ?>/<?php echo url($crow["Id"], $nombreFiltrado); ?>">
                                <p class="a_description more"><?php echo $crow['StrSinopsis']; ?></p>
                                <div class="div_img_s"><img src="<?php echo $crow['StrImagen']; ?>" class="card-img-top rounded-0 lazyload" alt="..."></div>
                                <div class="are_info_s">
                                    <h1 style="font-size:1rem;" class="are_s_title pt-1"><?php echo $crow['StrNombre']; ?></h1>
                                    <span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;"><?php echo ucfirst($fechaFormat_aPage); ?> • </span>
                                    <span style="color:#<?php echo $color_info; ?>;font-size:0.8rem;left:5px;position:relative;"><?php echo $crow['estado1']; ?></span>
                                    <span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;"> • <?php echo $tipoA; ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
            <?php
                }
            }
            ?>
            <?php echo $per_page_html; ?>
        </div>
    </div>
    </div>
</form>