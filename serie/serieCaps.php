<div class="container">
    <div class="row">
        <div class="w-100">
            <div class="panel-heading">
                <span style="padding:3px;color:#ebcc34;font-size:1.6rem;"><i class="fas fa-th-list"></i> Lista de Capitulos</span>
            </div>
            <div id="caps_scrollbar" class="col-lg-12 col-md-12 col-sm-12 col-12 tabla_episodios">
                <div class="panel panel-default">
                    <table class="table">
                        <tr style="display:flex;flex-direction:column;" class="td-cap">
                            <?php
                            try {
                                $id = $_GET['Id'];
                                $nombre = $_GET['StrNombre'];
                                $sql = "SELECT series.Id,series.StrSinopsis,series.DiaEmision,series.estado1,capitulos.StrNombre,capitulos.IdRel,capitulos.Id FROM series INNER JOIN capitulos ON series.Id = capitulos.IdRel WHERE IdRel = " . $id . " ORDER BY capitulos.nCap DESC";
                                $resultado = $base->query($sql);
                                $crow = $resultado->fetch(PDO::FETCH_ASSOC);
                                if ($crow['estado1'] != "En Emision") {
                                    echo "";
                                } else {
                                    if ($crow['DiaEmision'] == "1") {
                                        $d = "sabado  ";
                                        $ds = strtotime("next Saturday");
                                    } else if ($crow['DiaEmision'] == "2") {
                                        $d = "domingo  ";
                                        $ds = strtotime("next Sunday");
                                    } else if ($crow['DiaEmision'] == "3") {
                                        $d = "lunes  ";
                                        $ds = strtotime("next Monday");
                                    } else if ($crow['DiaEmision'] == "4") {
                                        $d = "martes  ";
                                        $ds = strtotime("next Tuesday");
                                    } else if ($crow['DiaEmision'] == "5") {
                                        $d = "miercoles  ";
                                        $ds = strtotime("next Wednesday");
                                    } else if ($crow['DiaEmision'] == "6") {
                                        $d = "jueves  ";
                                        $ds = strtotime("next Thursday");
                                    } else {
                                        $d = "viernes  ";
                                        $ds = strtotime("next Friday");
                                    }
                                    echo "<td class='proximo_ep'><b><i class='fas fa-arrow-circle-right'></i>  Proximo episodio el " . $d . "</b><i class='far fa-calendar-alt'> </i> " . date('d-m-Y', $ds) . "</td>";
                                }
                            } catch (Exception $e) {
                                echo "Error en linea: " . $e->getMessage();
                            }
                            ?>
                            <?php
                            try {
                                $id = $_GET['Id'];
                                $nombre = $_GET['StrNombre'];
                                $sql = "SELECT series.Id,series.StrSinopsis,capitulos.StrNombre,capitulos.IdRel,capitulos.Id,capitulos.visitas FROM series INNER JOIN capitulos ON series.Id = capitulos.IdRel WHERE IdRel = " . $id . " ORDER BY capitulos.nCap DESC";
                                $resultado = $base->query($sql);
                                $hayEpisodios = 0; //Establece el valor inicial de 0 capitulos
                                while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                                    $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
                                    //Comprobamos capitulos vistos
                                    if (isset($_SESSION["usuario"])) { //Comprobamos si el usuario esta logeado
                                        $idUser = $_SESSION["idUser"];         //Id del usuario en la bd						
                                        $sqlVisto = "SELECT * FROM vistos WHERE id_usuario = :usuario AND id_capitulo = :capitulo";
                                        $usuario = htmlentities(addslashes($idUser));
                                        $capitulo = htmlentities(addslashes($crow['Id']));
                                        $resultadoVisto = $base->prepare($sqlVisto);
                                        $resultadoVisto->bindValue(':usuario', $usuario);
                                        $resultadoVisto->bindValue(':capitulo', $capitulo);
                                        $resultadoVisto->execute();
                                        $visto = "";
                                        if ($resultadoVisto->rowCount() > 0) {
                                            $visto .= "• <div class='badge badge-pills badge-are-c'>Visto <i class='fas fa-check'></i></div>";
                                        }
                                        ++$hayEpisodios;
                                    } else {
                                        $cookie_name = $crow['Id'];
                                        $visto = "";
                                        if (!isset($_COOKIE[$cookie_name])) {
                                            $visto = "";
                                        } else {
                                            $visto .= "<div class='badge badge-pills badge-are-c'><i class='fas fa-check'></i></div>";
                                        }
                                        ++$hayEpisodios; //Si hay capitulos aumenta la variable en la cantidad de episodios
                                    }
                                    echo  "
											<td style='border-bottom: 1px solid #ebcc43;'><a href='../../ver/" . url($crow['Id'], $nombreFiltrado) . "'><i class='far fa-play-circle'></i> " . $crow['StrNombre'] . " " . $visto . " • <span style='color:#ebcc43c4;font-size:0.8rem;'>" . $crow['visitas'] . " Visitas</span></a></td>
											";
                                }
                                if ($hayEpisodios == 0) { //si no hay episodios devuelve un mensaje
                                    echo "No hay episodios agregados aun.";
                                }
                            } catch (Exception $e) {
                                echo "Error en linea: " . $e->getMessage();
                            }
                            ?>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>