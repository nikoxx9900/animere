<form role="form" method="GET" action="">
    <?php
    if ($_GET['cat'] == '') {
        $catInf = "Todos";
    } else {
        $catInf = $_GET['cat'];
    }
    try {
        $sql1 = "SELECT * FROM categorias ORDER BY Nombre ASC";
        $resultado1 = $base->prepare($sql1);
        $resultado1->execute(array());
        echo '<ul class="ks-cboxtags">';
        while ($crow1 = $resultado1->fetch(PDO::FETCH_ASSOC)) {
            echo "<li><input type='checkbox' id='checkbox" . $crow1['Nombre'] . "' value='" . $crow1['Nombre'] . "'><label for='checkbox" . $crow1['Nombre'] . "'>" . $crow1['Nombre'] . "</label></li><br>";
        }
        echo '</ul>';
    } catch (Exception $e) {
        echo "Fallo en la base de datos " . $e->getLine();
    }
    if ($crow1['pageVersion'] == "2.0") {
        $pageVersion1 = "serieV2";
    } else {
        $pageVersion1 = "serie";
    }
    ?>
    <button name="catS" type="submit" class="btn btn-success">Filtrar</button>
</form>