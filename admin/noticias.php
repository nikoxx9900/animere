<!DOCTYPE html>
<html lang="es">
<?php include 'adminProtect.php'; ?>
<?php include '../bin/core/conexion.php'; ?>
<?php include '../bin/bin/funciones.php'; ?>
<?php include '../config.php'; ?>

<head>
    <?php include '../header.php'; ?>
    <style>
        .add {
            padding: 20px;
            background-color: #007bff;
            /* Azul */
            color: white;
            margin-bottom: 15px;
        }

        .edit {
            padding: 20px;
            background-color: #ffc107;
            /* Amarillo */
            color: #222;
            margin-bottom: 15px;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }
    </style>
</head>

<body>
    <?php include '../navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-center">
            <img src="../img/r.svg" width="400" alt="">
        </div>
        <div class="row justify-content-center">
            <h1>Crear anuncio en la web</h1>
        </div>

    </div>
    <?php
    if (isset($_GET['noticia'])) {
        $envionoti = $_GET['noticia'];
        if ($envionoti == "correcto") {
            $msg_toast =
                '<div class="add container">
                            <span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
                            Noticia Agregada Correctamente
                        </div>';
        }
        echo $msg_toast;
    }
    ?>
    <div class="container">
        <div class="row">

            <div class="col-4 offset-4">
                <form action="../form/crearNoticia.php" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <textarea class="form-control" name="comentario" rows="10">Comentario</textarea>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="link" placeholder="(Puedes dejarlo en blanco) Aqui va el link que quieras poner">
                    </div>
                    <!-- <div class="form-group">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="imagen">
                                <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Selecciona Imagen para acompañar el post</label>
                            </div>
                        </div>
                    </div> -->
                    <input type="submit" value="Confirmar y Subir" class="btn btn-warning btn-block">
                </form>
                <a class="btn btn-success btn-block mt-3" href="administracion.php">Volver al Panel Admin</a>
            </div>
        </div>
    </div>


    <script>
        var close = document.getElementsByClassName("closebtn");
        var i;

        for (i = 0; i < close.length; i++) {
            close[i].onclick = function() {
                var div = this.parentElement;
                div.style.opacity = "0";
                setTimeout(function() {
                    div.style.display = "none";
                }, 600);
            }
        }
    </script>
</body>

</html>