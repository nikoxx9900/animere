<!DOCTYPE html>
<html lang="es">
<?php include 'adminProtect.php'; ?>
<?php include '../bin/core/conexion.php'; ?>
<?php include '../bin/bin/funciones.php'; ?>
<?php include '../config.php'; ?>

<head>
    <?php include '../header.php'; ?>
    <script src="jquery.js"></script>
    <script src="datos.js"></script>
</head>

<body>
    <?php include '../navbar.php'; ?>
    <div class="container mt-4">
        <div class="row">
            <div class="col-8">


                <?php
                $sql = "SELECT * FROM noticias ORDER BY id DESC";
                $resultado = $base->query($sql);
                echo '
<table class="table">
<thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Comentario</th>
        <th scope="col">Link</th>
        <th scope="col">Fecha</th>
        <th scope="col">Accion</th>
    </tr>
</thead>
<tbody>';
                while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    echo '
<tr>
<th scope="row">1</th>
<td>' . $crow['comentario'] . '</td>
<td>' . $crow['link'] . '</td>
<td>' . $crow['fecha'] . '</td>
<td>
    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalBorrar' . $crow["id"] . '">
        <i class="far fa-trash-alt"></i>
    </button>
    <div class="modal fade" id="modalBorrar' . $crow["id"] . '" tabindex="-1" role="dialog" aria-labelledby="modalBorrarTitle" aria-hidden="true">
        <form method="post" role="form" action="delNoticias.php">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modalBorrar' . $crow["id"] . 'Title">Borrar ' . $crow["StrNombre"] . '</h5>
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <h3>Seguro que quieres eliminar ' . $crow["StrNombre"] . ' ?</h3>
            </div>
            <div class="modal-footer">
                <input name="s_id" type="hidden" value="' . $crow["id"] . '" >
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button name="borrar_s" type="submit" class="btn btn-danger">Borrar (Irreversible)</button>
            </div>
        </div>
        </div>
        </form>
    </div>
</td>
</tr>
';
                }
                echo '
</tbody>
</table>
';
                ?>
                <?php
                if (isset($_POST['borrar_s'])) {
                    $id = $_POST['s_id'];
                    try {
                        include '../bin/core/conexion.php';
                        $sql = "DELETE FROM noticias WHERE noticias.id='$id'";

                        // Prepare statement
                        $stmt = $base->prepare($sql);

                        // execute the query
                        $stmt->execute();

                        // echo a message to say the UPDATE succeeded
                        echo $stmt->rowCount() . " Noticia BORRADA";
                        echo '<script>window.location.href="delNoticias.php"</script>';
                    } catch (PDOException $e) {
                        echo $sql . "<br>" . $e->getMessage();
                    }

                    $base = null;
                }


                ?>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="jumbotron">
                        <h2>Acceso Rapido: Panel de Administracion<h2>
                    </div>
                    <a class="btn btn-primary btn-block" href="<?php echo $config['base_url']; ?>admin/noticias.php" role="button">Agregar Noticia</a>
                    <a class="btn btn-success btn-block" href="<?php echo $config['base_url']; ?>admin/administracion.php" role="button">Volver al menu principal del Panel Admin</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="bootstrap.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
</body>

</html>