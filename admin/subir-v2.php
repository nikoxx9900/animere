<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../bin/bin/funciones.php';
include '../bin/core/conexion.php';
include '../config.php';
include 'adminProtect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Agregar Anime</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<script type="text/javascript" src="../js/dpdw.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<style>
		.morecontent span {
			display: none;
		}

		.morelink {
			display: block;
		}
	</style>
	<style>
		.add {
			padding: 20px;
			background-color: #007bff;
			/* Azul */
			color: white;
			margin-bottom: 15px;
		}

		.edit {
			padding: 20px;
			background-color: #ffc107;
			/* Amarillo */
			color: #222;
			margin-bottom: 15px;
		}

		.closebtn {
			margin-left: 15px;
			color: white;
			font-weight: bold;
			float: right;
			font-size: 22px;
			line-height: 20px;
			cursor: pointer;
			transition: 0.3s;
		}

		.closebtn:hover {
			color: black;
		}
	</style>
</head>

<body>
	<?php
	include '../navbar-ver.php';
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php
				$envionoti = $_GET['capitulo'];
				if ($envionoti == "correcto") {
					$msg_toast =
						'<div class="add">
						<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
						Serie subida correctamente.
					</div>';
				}
				echo $msg_toast; ?>
				<div class="content">
					<h3 class="title mb-2"><i class="fas fa-plus-square"></i> Agregar Anime <span style="color:#5deb43;background-color:#222;">Verde = Manual</span></h3>
					<div class="form-group">
						<form method="post" action="../form/up-v2.php" class="subida" enctype="multipart/form-data">
							<span id="error" style="color:#ff4500;"></span>
							<div class="form-group" style="background-color:#ff4500;color:#222;padding:0.4rem;">
								<label for="name"><i class="fas fa-fingerprint"></i> ID de AniList</label>
								<input id="animeID" type="text" placeholder="ID de AniList" onKeyUp="getAnimeInfo()">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="name"><i class="fas fa-heading"></i> Nombre del Anime</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Boku no HeroAcademia...">
								<label for="name"><i class="fas fa-heading"></i> Nombre Ingles del Anime</label>
								<input type="text" class="form-control" id="name-english" name="nombreIngles" placeholder="My Heroe Academia...">
								<label for="name"><i class="fas fa-heading"></i> Nombre Japones del Anime</label>
								<input type="text" class="form-control" id="name-native" name="nombreJapones" placeholder="ソードアート">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="name"><i class="fas fa-file-alt"></i> Sinopsis en ingles:</label>
								<span><a style="color:#ff4500;" id="sinopsis-ingles" href="" target="_blank">TRADUCCION CLICK AQUI</a></span><br>
								<label for="name"><i class="fas fa-file-alt"></i> Sinopsis</label>
								<textarea type="text" class="form-control" id="sinopsis" name="sinopsis" placeholder="Erase una vez un norteño..."></textarea>
							</div>
							<div for="imagen" class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label><i class="fas fa-image"></i> Imagen de Portada</label>
								<input type="file" name="imagen" />
								<label><i class="fas fa-images"></i> La imagen se agrega manual, puedes descargarla de el siguiente link:</label>
								<a style="font-size:1rem;color:#ff4500;" target="_blank" name="linkPortada" id="linkPortada" style="width:100%;" rows="1">Imagen Sugerida</a>
							</div>
							<div for="imagen-fondo" class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label><i class="fas fa-images"></i> Imagen de Fondo</label>
								<input type="file" name="imagen-fondo" />
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="fecha-estreno"><i class="fas fa-calendar-alt"></i> Fecha de estreno</label>
								<input id="fecha-estrenoAnim" class="form-control" type="date" name="fecha-estreno" placeholder="Fecha de estreno">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="estado1"><i class="fas fa-satellite-dish"></i> Estado de Emision</label>
								<select class="space form-control" type="text" name="estado1" id="estado">
									<option value="Finalizado">Finalizado</option>
									<option value="En Emision">En Emision</option>
								</select>
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="estado1"><i class="fas fa-person-booth"></i> Estudio Animador</label>
								<input class="form-control" placeholder="(Bones, A1-Pictures...)" type="text" name="estudio" id="estudio">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="estado1"><i class="fas fa-calendar-plus"></i> Temporada de Estreno</label>
								<input class="form-control" placeholder="(Primavera, Verano, Otoño, Invierno)" type="text" name="temporada" id="temporada">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="estado1"><i class="fas fa-book"></i> Origen</label>
								<input class="form-control" placeholder="(Manga, NL, Videojuego...)" type="text" name="origen" id="origen">
							</div>
							<div class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label for="DiaEmision"><i class="fas fa-tv"></i> Dia de Emision</label>
								<select class="form-control" type="text" name="DiaEmision">
									<option value="0">(Elije Uno)</option>
									<option value="1">Sabado</option>
									<option value="2">Domingo</option>
									<option value="3">Lunes</option>
									<option value="4">Martes</option>
									<option value="5">Miercoles</option>
									<option value="6">Jueves</option>
									<option value="7">Viernes</option>
								</select>
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="tipo"><i class="fas fa-film"></i> Tipo de Serie</label>
								<select class="form-control" type="text" name="tipo" id="tipoSerie">
									<option value="0">Serie</option>
									<option value="1">Pelicula</option>
									<option value="2">OVA</option>
									<option value="3">ONA</option>
									<option value="4">Especial</option>
								</select>
							</div>

							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="enSlider"><i class="fas fa-fire-alt" style="color:#ff4500;"></i> Añadir a Animes Populares?</p>
									<select class="form-control" type="text" name="enSlider">
										<option value="0">No (Por Defecto)</option>
										<option value="1">Si</option>
									</select>
							</div>
							<div class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label for=""><i class="fas fa-sticky-note"></i> Categorias en ingles para referencia</label>
								<p id="categorias" style="width:100%;"></p>
								<label for=""><i class="fas fa-sticky-note"></i> Categorias para el Anime</label>
								<?php
								try {
									include "../bin/core/conexion.php";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero1'><option value='0'>Default</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "</select></div>";
									echo "<div class=''><select class='space form-control' type='text' name='gnero2'><option value='0'>Default</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "</select></div>";
									echo "<div class=''><select class='space form-control' type='text' name='gnero3'><option value='0'>Default</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero4'><option value='0'>Default</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero5'><option value='0'>Default</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
								} catch (Exception $e) {
									echo "Fallo en la base de datos " . $e->getLine();
								}
								?>
							</div>
							<br>
							<input type="submit" class="btn btn-success" value="Confirmar y Subir">
						</form>
						<br><br>

					</div>
				</div>
			</div>
			<div class="col-md-4 p-4">
				<div class="row">
					<div class="jumbotron" style="background-color:#ebcc43 !important;color:#222 !important;">
						<h2>Acceso Rapido: Panel de Administracion<h2>
					</div>
					<a class="btn btn-primary btn-block" href="<?php echo $config['base_url']; ?>admin/subir-cap.php" role="button">Subir Capitulos</a>
					<a class="btn btn-danger btn-block" href="<?php echo $config['base_url']; ?>admin/add_category.php" role="button">Agregar Categorias</a>
					<a class="btn btn-success btn-block" href="<?php echo $config['base_url']; ?>admin/administracion.php" role="button">Volver al menu principal del Panel Admin</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="container-fluid justify-content-center">
			<div class="d-flex title justify-content-center">
				<h2>Ultimos Animes Agregados</h2>
			</div>
		</div>
		<div class="anime-grid row justify-content-sm-center">


			<?php
			renderSeries(30, "recientes", false);
			?>


		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<h5>Todos derechos reservados <span class="nm-footer">AnimeRE</span>.</h5>
		</div>
	</footer>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
	<script>
		function getAnimeInfo() {
			var valueInput = document.getElementById("animeID").value;
			// Here we define our query as a multi-line string
			// Storing it in a separate .graphql/.gql file is also possible
			var query = `
		query ($id: Int) { # Define which variables will be used in the query (id)
		Media (id: $id, type: ANIME
		) { # Insert our variables into the query arguments (id) (type: ANIME is hard-coded in the query)
			id
			status
			format
			genres
			season
			source
			studios {
				edges {
					node {
					name
					}
				}
			}
			coverImage {
				extraLarge
				large
				medium
				color
			}
			bannerImage
			description
			meanScore
			episodes
			title {
			romaji
			english
			native
			}
					source
					startDate {
					year
					month
					day
					}
					endDate {
					year
					month
					day
					}
					season
					studios {
					edges {
						id
					}
					}
		}
		}
		`;

			// Define our query variables and values that will be used in the query request
			var variables = {
				id: valueInput
			};

			// Define the config we'll need for our Api request
			var url = 'https://graphql.anilist.co',
				options = {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Accept': 'application/json',
					},
					body: JSON.stringify({
						query: query,
						variables: variables
					})
				};

			// Make the HTTP Api request
			fetch(url, options).then(handleResponse)
				.then(handleData)
				.catch(handleError);

			function handleResponse(response) {
				return response.json().then(function(json) {
					return response.ok ? json : Promise.reject(json);
				});
			}

			function handleData(data) {
				console.log(data);
				document.getElementById("error").innerHTML = "";
				document.getElementById("name").value = data.data.Media.title.romaji;
				document.getElementById("name-english").value = data.data.Media.title.english;
				document.getElementById("name-native").value = data.data.Media.title.native;
				document.getElementById("sinopsis-ingles").href = "https://translate.google.com/?hl=es#view=home&op=translate&sl=en&tl=es&text=" + encodeURI(data.data.Media.description);
				document.getElementById("linkPortada").href = data.data.Media.coverImage.extraLarge;
				var studios = data.data.Media.studios.edges;
				var studios1 = Object.values(studios)[0];
				document.getElementById("estudio").value = studios1.node.name;

				var temporada = data.data.Media.season;
				if (temporada == "SPRING") {
					document.getElementById("temporada").value = "Primavera";
				} else if (temporada == "SUMMER") {
					document.getElementById("temporada").value = "Verano";
				} else if (temporada == "FALL") {
					document.getElementById("temporada").value = "Otoño";
				} else if (temporada == "WINTER") {
					document.getElementById("temporada").value = "Invierno";
				}

				var origen = data.data.Media.source;
				if (origen == "VISUAL_NOVEL") {
					document.getElementById("origen").value = "Visual Novel";
				} else if (origen == "LIGHT_NOVEL") {
					document.getElementById("origen").value = "Novela Ligera";
				} else if (origen == "VIDEO_GAME") {
					document.getElementById("origen").value = "Videojuego";
				} else if (origen == "NOVEL") {
					document.getElementById("origen").value = "Novela";
				} else if (origen == "OTHER") {
					document.getElementById("origen").value = "Otro";
				} else {
					document.getElementById("origen").value = data.data.Media.source;
				}



				var monthFix = data.data.Media.startDate.month;
				var dayFix = data.data.Media.startDate.day
				if (monthFix < 10) {
					var monthFix = "0" + monthFix;
				}
				if (dayFix < 10) {
					var dayFix = "0" + dayFix;
				}
				var dateAnime = data.data.Media.startDate.year + "-" + monthFix + "-" + dayFix;
				document.getElementById("fecha-estrenoAnim").value = dateAnime;
				if (data.data.Media.status == "RELEASING") {
					document.getElementById("estado").value = "En Emision";
				} else {
					document.getElementById("estado").value = "Finalizado";
				}

				var typeAnime = data.data.Media.format;
				if (typeAnime == "TV" || typeAnime == "TV_SHORT") {
					document.getElementById("tipoSerie").value = "0";
				} else if (typeAnime = "MOVIE") {
					document.getElementById("tipoSerie").value = "1";
				} else if (typeAnime = "OVA") {
					document.getElementById("tipoSerie").value = "2";
				} else if (typeAnime = "ONA") {
					document.getElementById("tipoSerie").value = "3";
				} else if (typeAnime = "SPECIAL") {
					document.getElementById("tipoSerie").value = "4";
				} else {
					alert("No se reconoce el tipo de serie. Verifica que la ID de AniList corresponda a un Anime y no a un Manga");
				}
				document.getElementById("categorias").innerHTML = data.data.Media.genres;
			}

			function handleError(error) {
				document.getElementById("error").innerHTML = 'No se encuentra la serie';
				console.error(error);
			}
		}
	</script>
	<script>
		var close = document.getElementsByClassName("closebtn");
		var i;

		for (i = 0; i < close.length; i++) {
			close[i].onclick = function() {
				var div = this.parentElement;
				div.style.opacity = "0";
				setTimeout(function() {
					div.style.display = "none";
				}, 600);
			}
		}
	</script>
</body>

</html>