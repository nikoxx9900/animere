<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include 'adminProtect.php';
include '../bin/core/conexion.php';
include '../bin/bin/funciones.php';
include '../config.php';
?>

<!DOCTYPE html>
<html lang="es">
<?php
include '../header.php';
?>

<head>
	<script src="jquery.js"></script>
	<script src="datos.js"></script>
</head>

<body>
	<?php
	include '../navbar-ver.php';
	?>
	<div class="container">
		<div class="row">
			<div class="col-8">
				<?php
				include 'getSeries.php';
				?>
			</div>
			<div class="col-4">
				<div class="row">
					<div class="jumbotron">
						<h2>Acceso Rapido: Panel de Administracion<h2>
					</div>
					<a class="btn btn-primary btn-block" href="<?php echo $config['base_url']; ?>admin/subir.php" role="button">Subir Serie</a>
					<a class="btn btn-primary btn-block" href="<?php echo $config['base_url']; ?>admin/subir-cap.php" role="button">Subir Capitulos</a>
					<a class="btn btn-danger btn-block" href="<?php echo $config['base_url']; ?>admin/modificar-cap.php" role="button">Borrar Capitulos</a>
					<a class="btn btn-info btn-block" href="<?php echo $config['base_url']; ?>admin/add_category.php" role="button">Agregar Categorias</a>
					<a class="btn btn-success btn-block" href="<?php echo $config['base_url']; ?>admin/administracion.php" role="button">Volver al menu principal del Panel Admin</a>
				</div>
			</div>
		</div>

		<footer class="footer">
			<div class="container">
				<h5>Todos derechos reservados <span class="nm-footer">AnimeRE</span>.</h5>
			</div>
		</footer>

		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
</body>

</html>