<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../bin/core/conexion.php';
include '../bin/bin/funciones.php';
include '../config.php';
include 'adminProtect.php';
date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="en">
<?php
include '../header.php';
?>

<head>
	<script src="jquery.js"></script>
	<script src="datos.js"></script>
	<style>
		.add {
			padding: 20px;
			background-color: #007bff;
			/* Azul */
			color: white;
			margin-bottom: 15px;
		}

		.edit {
			padding: 20px;
			background-color: #ffc107;
			/* Amarillo */
			color: #222;
			margin-bottom: 15px;
		}

		.closebtn {
			margin-left: 15px;
			color: white;
			font-weight: bold;
			float: right;
			font-size: 22px;
			line-height: 20px;
			cursor: pointer;
			transition: 0.3s;
		}

		.closebtn:hover {
			color: black;
		}
	</style>
</head>

<body>
	<?php
	include '../navbar-ver.php';
	?><br><br><br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="content"><?php
										$envionoti = $_GET['envionoti'];
										$msg_toast = "";
										if ($envionoti == "correcto") {
											$msg_toast =
												'<div class="add">
						<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
						Capitulo subido correctamente. La notificacion tambien fue enviada y se publico en Facebook
					</div>';
										} else if ($envionoti == "no") {
											$msg_toast = '
					<div class="edit">
						<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
						Capitulo subido correctamente. La notificacion se selecciono no enviarla
					</div>';
										}
										echo $msg_toast; ?>
					<h3 class="title mb-2"><i class="fas fa-plus-square"></i> Añadir Capitulo</h3>
					<div class="form-group">
						<form method="post" action="../form/up-cap.php" class="subida" enctype="multipart/form-data">
							<div class="form-group" style="background-color:#007bff;;padding:0.4rem;">
								<label for="idrel" style="color:#fff;"><i class="fas fa-list"></i> Anime al que agregar el capitulo</label>
								<?php
								$sql = "SELECT * FROM series ORDER BY Id DESC";
								$resultado = $base->prepare($sql);
								$resultado->execute(array());
								echo "<div class='space'><select class='form-control' style='border:2px solid #fff;color:#fff;' name='idrel'><option value='0'>(Selecciona una Serie)</option>";
								while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
									echo "<option value='" . $crow['Id'] . ";" . $crow['StrNombre'] . "'>" . $crow['StrNombre'] . "</option>";
								}
								echo "</select></div>";

								?>
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="nCap"><i class="fas fa-list-ol"></i> Numero del Capitulo</label>
								<input type="text" class="form-control" id="name" name="nCap" placeholder="(Ejemplo: 1, 5, 24, 12, etc. solo numeros)">
							</div>

							<!--Aqui el input del HLS-->
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="hls"><i class="fab fa-youtube"></i> Opcion AnimeRE (HLS)</label>
								<input type="file" name="hls" />
							</div>
							<!--Aqui las opciones normales con embed -->
							<b>A partir de aqui, los links deben ser iframes</b>
							<div class="form-group">
								<?php players("1"); ?>
								<textarea type="text" class="form-control" id="url" name="url1" placeholder="Escriba url del Video 1"></textarea>
							</div>
							<div class="form-group">
								<?php players("2"); ?>
								<textarea type="text" class="form-control" id="url" name="url2" placeholder="Escriba url del Video 2"></textarea>
							</div>
							<div class="form-group">
								<?php players("3"); ?>
								<textarea type="text" class="form-control" id="url" name="url3" placeholder="Escriba url del Video 3"></textarea>
							</div>
							<div class="form-group">
								<?php players("4"); ?>
								<textarea type="text" class="form-control" id="url" name="url4" placeholder="Escriba url del Video 4"></textarea>
							</div>
							<div class="form-group">
								<?php players("5"); ?>
								<textarea type="text" class="form-control" id="url" name="url5" placeholder="Escriba url del Video 5"></textarea>
							</div>
							<div class="form-group">
								<?php players("6"); ?>
								<textarea type="text" class="form-control" id="url" name="url6" placeholder="Escriba url del Video 6"></textarea>
							</div>
							<div class="form-group">
								<?php players("7"); ?>
								<textarea type="text" class="form-control" id="url" name="url7" placeholder="Escriba url del Video 7"></textarea>
							</div>
							<div class="form-group">
								<?php players("8"); ?>
								<textarea type="text" class="form-control" id="url" name="urld" placeholder="Escriba url de descarga (Mediafire, Mega, Openload, etc)"></textarea>
							</div>
							<div class="form-group">
								<?php players("9"); ?>
								<textarea type="text" class="form-control" id="url" name="urld2" placeholder="Escriba url de descarga 2 (Mediafire, Mega, Openload, etc)"></textarea>
							</div>
							<div class="form-group">
								<?php players("10"); ?>
								<textarea type="text" class="form-control" id="url" name="urld3" placeholder="Escriba url de descarga 3 (Mediafire, Mega, Openload, etc)"></textarea>
							</div>
							<!--Aqui el input de la imagen-->
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="imagen"><i class="fas fa-image"></i> Miniatura del Capitulo</label>
								<input type="file" name="imagen" />
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="oculto" style="color:#222;"><i class="fas fa-eye-slash"></i> Ocultar en Pagina Principal?</label>
								<select class="form-control" type="text" name="oculto">
									<option value="0">No</option>
									<option value="1">Si (No aparecera en la pagina principal)</option>
								</select>
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="enviar_notificacion"><i class="fas fa-bell"></i> Enviar notifiacion para el capitulo?</label>
								<input type="checkbox" aria-label="Enviar notificacion?" name="enviar_notificacion">
								<input type="text" class="form-control" placeholder="Texto para el subtitulo (Dejalo en blanco para mensaje por defecto)" name="texto_subtitulo">
							</div>
							<div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
								<label for="args" style="color:#222;"><i class="fas fa-eye"></i> Multiplicador de Visitas</label>
								<select class="form-control" type="text" name="args">
									<option value="0">x1</option>
									<option value="3">x3</option>
									<option value="5">x5</option>
									<option value="10">x10 (Ste men :v)</option>
									<option value="50">x50 (Hay gente que solo quiere ver el mundo arder)</option>
									<option value="100">x100 (Dios ha muerto)</option>
								</select>
							</div>
							<input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="fecha">


							<input type="submit" class="btn btn-success" value="Confirmar y Subir Capitulo">
						</form>

					</div>
				</div>
			</div>
			<div class="col-md-4" style="padding-left:50px;">
				<div class="row">
					<div class="jumbotron" style="background-color:#ebcc43 !important;color:#222 !important;">
						<h2 style="color:#222;">Acceso Rapido: Panel de Administracion<h2>
					</div>
					<a class="btn btn-primary btn-block" href="<?php echo $config['base_url']; ?>admin/subir-v2.php" role="button">Subir un ANIME</a>
					<a class="btn btn-success btn-block" href="<?php echo $config['base_url']; ?>admin/administracion.php" role="button">Volver al menu principal del Panel Admin</a>
					<!--<div style="margin-top:40px;" class="jumbotron">
					<form action="post">
						<input type="text" id="URL">
						<input type="button" value="Scrap" onclick="scrap_datos();">
					</form>

					<div id="resultados">
						
					</div>
				</div>-->
				</div>
			</div>
		</div>

		<div class="container">
			<div class="container-fluid justify-content-center">
				<div class="d-flex title justify-content-center">
					<h2>Ultimos Capitulos Agregados (Incluyendo Ocultos)</h2>
				</div>
			</div>
			<div class="row">
				<?php
				renderCap(true, 60, "recientes");
				?>
			</div>
		</div>

		<footer class="footer">
			<div class="container">
				<h5>Todos derechos reservados <span class="nm-footer">AnimeRE</span>.</h5>
			</div>
		</footer>

		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
		<script>
			var close = document.getElementsByClassName("closebtn");
			var i;

			for (i = 0; i < close.length; i++) {
				close[i].onclick = function() {
					var div = this.parentElement;
					div.style.opacity = "0";
					setTimeout(function() {
						div.style.display = "none";
					}, 600);
				}
			}
		</script>
</body>

</html>