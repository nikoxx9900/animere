<!DOCTYPE html>
<?php
include 'bin/core/conexion.php';
include 'bin/bin/funciones.php';
include 'config.php';
date_default_timezone_set('America/Bogota');
include 'comprobarCookies.php';
include 'visitas.php';
$fluid = "";
?>
<html lang="es">

<head>
	<?php include 'header.php' ?>
	<meta name="description" content="Mira tus Animes favoritos sin publicidad, online gratis en HD sub español, guarda tus series vistas y opina.">
	<meta name="keywords" content="Anime online en HD, Anime online sub español, Boku no Hero Academia 4 Temporada, Nanatsu no Taizai, One Punch Man, Hensuki, FullMetal Alchemist, SAO, Sword Art Online, Boruto, Naruto, Dragon Ball, Black Clover">
</head>

<body>
	<?php include 'bin/bin/static1.php'; ?>
	<?php include 'navbar.php'; ?>
	<!-- KEYWORDS INICIALES RENIMATION -->
	<div class="container d-none d-sm-none d-md-block d-lg-block mt-2">
		<div class="row justify-content-center">
			<h1 style="font-size:1rem;">Renimation tu fuente de Anime sub español y en HD</h1>
		</div>
		<div class="row justify-content-center">
			<h2 style="font-size:0.8rem;">Únete y disfruta de la web mejor diseñada y optimizada para ver Anime</h2>
		</div>
	</div>
	<!-- FIN KEYWORDS INICIALES RENIMATION -->
	<!-- DIV DE PUBLICIDAD BEBI -->
	<style>
		@media only screen and (max-width:575px) {
			.bebi-are {
				display: none !important
			}
		}
	</style>
	<div class="container d-flex justify-content-center" style="padding:15px;">
		<script type="text/javascript">
			if (!window.BB_a) {
				BB_a = [];
			}
			if (!window.BB_ind) {
				BB_ind = 0;
			}
			if (!window.BB_vrsa) {
				BB_vrsa = 'v3';
			}
			if (!window.BB_r) {
				BB_r = Math.floor(Math.random() * 1000000000)
			}
			BB_ind++;
			BB_a.push({
				"pl": 2006024,
				"index": BB_ind
			});
		</script>
		<script type="text/javascript">
			document.write('<scr' + 'ipt async data-cfasync="false" id="BB_SLOT_' + BB_r + '_' + BB_ind + '" src="//st.bebi.com/bebi_' + BB_vrsa + '.js"></scr' + 'ipt>');
		</script>
	</div>
	<!-- FIN DIV PUBLI BEBI -->
	<!-- SECCION DE NOTICIAS/ANUNCIOS -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php renderNoticias(); ?>
			</div>
		</div>
	</div>
	<!-- FIN SECCION DE NOTICIAS/ANUNCIOS -->
	<!-- Buscador Movil -->
	<div class="container d-md-none d-lg-none">
		<div class="row justify-content-center">
			<form style="color: #fff0 !important;" class="p-2 w-100">
				<form action="" method="post" class="navbar-form navbar-right" autocomplete="off" id="form-search2">
					<input name="buscar2" id="buscar2" method="post" type="text" class="form-control rounded-0" placeholder="Buscar Anime" style="height:2rem!important;">
					<div id="resultado-q2" style="z-index:1030;position:absolute;"></div>
				</form>

			</form>
		</div>
	</div>
	<!-- Capitulos Recientes  -->
	<div class="container">
		<div class="row justify-content-center">
			<div class="w-100 are-are m-1 d-flex justify-content-center"><span style="font-size:1.75rem!important;" class="bdr-title title" align="center"><i class="fab fa-youtube"></i> CAPÍTULOS RECIENTES</span></div>
		</div>
		<div class="row justify-content-center">
			<?php renderCap(false, 40, "recientes"); ?>
		</div>
	</div>
	<!-- Termina div caps recientes -->
	<!-- INICIO TOP SEMANAL -->
	<div class="container">
		<div class="row justify-content-center">
			<div class="w-100 are-are m-1 d-flex justify-content-center"><span style="font-size:1.75rem!important;" class="bdr-title title" align="center"><i style='color: #ff4500 !important;' class='fab fa-hotjar'></i> TOP SEMANAL</span></div>
		</div>
		<div class="m-1">
			<div class="row justify-content-center">
				<div class="owl-carousel owl-two">
					<?php
					renderCapPopulares(10);
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN TOP SEMAMAL -->
	<!-- BLOQUE DOS PUBLICIDAD ADSKEEPER -->
	<div class="container d-flex justify-content-center" style="padding:15px;">
		<style>
			#M439140ScriptRootC383475 {
				min-height: 300px;
			}
		</style>
		<div id="M439140ScriptRootC383475">
			<div id="M439140PreloadC383475">
				Loading... </div>
			<script>
				(function() {
					var D = new Date(),
						d = document,
						b = 'body',
						ce = 'createElement',
						ac = 'appendChild',
						st = 'style',
						ds = 'display',
						n = 'none',
						gi = 'getElementById',
						lp = d.location.protocol,
						wp = lp.indexOf('http') == 0 ? lp : 'https:';
					var i = d[ce]('iframe');
					i[st][ds] = n;
					d[gi]("M439140ScriptRootC383475")[ac](i);
					try {
						var iw = i.contentWindow.document;
						iw.open();
						iw.writeln("<ht" + "ml><bo" + "dy></bo" + "dy></ht" + "ml>");
						iw.close();
						var c = iw[b];
					} catch (e) {
						var iw = d;
						var c = d[gi]("M439140ScriptRootC383475");
					}
					var dv = iw[ce]('div');
					dv.id = "MG_ID";
					dv[st][ds] = n;
					dv.innerHTML = 383475;
					c[ac](dv);
					var s = iw[ce]('script');
					s.async = 'async';
					s.defer = 'defer';
					s.charset = 'utf-8';
					s.src = wp + "//jsc.adskeeper.co.uk/a/n/animere.net.383475.js?t=" + D.getYear() + D.getMonth() + D.getUTCDate() + D.getUTCHours();
					c[ac](s);
				})();
			</script>
		</div>
	</div>
	<!-- FIN BLOQUE DOS PUBLI ADSKEEPER -->
	<!-- ULTIMAS SERIES AGREGADAS -->
	<div class="container">
		<div class="row justify-content-center">
			<div class="w-100 are-are m-1 d-flex justify-content-center"><span style="font-size:1.75rem!important;" class="bdr-title title" align="center"><i class="far fa-play-circle"></i> ÚLTIMAS SERIES AGREGADAS</span></div>
		</div>
		<div class="row justify-content-center anime-grid">
			<div class="owl-carousel owl-three">
				<?php
				renderSeries(15, "recientes", TRUE);
				?>
			</div>
		</div>
	</div>
	<!-- TERMINA DIV ULTIMAS SERIES AGREGADAS -->
	<!-- TERCER BLOQUE PUBLI ADSKEEPER -->
	<div class="container d-flex justify-content-center" style="padding:15px;">
		<style>
			#M439140ScriptRootC384296 {
				min-height: 300px;
			}
		</style>
		<div id="M439140ScriptRootC384296">
			<div id="M439140PreloadC384296">
				Loading... </div>
			<script>
				(function() {
					var D = new Date(),
						d = document,
						b = 'body',
						ce = 'createElement',
						ac = 'appendChild',
						st = 'style',
						ds = 'display',
						n = 'none',
						gi = 'getElementById',
						lp = d.location.protocol,
						wp = lp.indexOf('http') == 0 ? lp : 'https:';
					var i = d[ce]('iframe');
					i[st][ds] = n;
					d[gi]("M439140ScriptRootC384296")[ac](i);
					try {
						var iw = i.contentWindow.document;
						iw.open();
						iw.writeln("<ht" + "ml><bo" + "dy></bo" + "dy></ht" + "ml>");
						iw.close();
						var c = iw[b];
					} catch (e) {
						var iw = d;
						var c = d[gi]("M439140ScriptRootC384296");
					}
					var dv = iw[ce]('div');
					dv.id = "MG_ID";
					dv[st][ds] = n;
					dv.innerHTML = 384296;
					c[ac](dv);
					var s = iw[ce]('script');
					s.async = 'async';
					s.defer = 'defer';
					s.charset = 'utf-8';
					s.src = wp + "//jsc.adskeeper.co.uk/a/n/animere.net.384296.js?t=" + D.getYear() + D.getMonth() + D.getUTCDate() + D.getUTCHours();
					c[ac](s);
				})();
			</script>
		</div>
	</div>
	<!-- FIN TERCER BLOQUES ADSKEEPER -->
	<!-- COMENTARIOS LANDING PAGE -->
	<div class="container">
		<style>
			.publisher-anchor-color a {
				color: #ebcc43 !important;
			}
		</style>
		<div id="disqus_thread" style="position:relative;z-index:99;"></div>
	</div>
	<script>
		var disqus_config = function() {
			this.page.url = 'https://renimation.com/index.php';
			this.page.identifier = 'https://renimation.com/index.php';
		};

		(function() {
			var d = document,
				s = d.createElement('script');
			s.src = 'https://animere-1.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
		})();
	</script>
	<noscript>Activa javascript para poder ver los comentarios</noscript>
	<!-- FIN COMENTARIOS LANDING PAGE -->

	<!-- FOOTER -->
	<?php include "footer.php"; ?>
	<!-- FIN INCLUDE FOOTER -->

	<!-- SCRIPTS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/animere.js"></script>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133390883-2"></script>
	<script>
		$("#modo-dia").click(function(e) {
			$("#main-theme").attr('href', $("#main-theme").attr('href') == 'css/main.css' ? 'css/main-light.css' : 'css/main.css');
		});
	</script>
	<script>
		$(document).ready(function() {
			var width = $(document).width();
			if ((width > 575) && (width < 1200)) {
				var showChar = 28;
			} else if (width < 574) {
				var showChar = 47;
			} else if (width > 1200) {
				var showChar = 29;
			}
			var ellipsestext = "...";


			$('.more2').each(function() {
				var content = $(this).html();

				if (content.length > showChar) {

					var c = content.substr(0, showChar);
					var h = content.substr(showChar, content.length - showChar);

					var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h;

					$(this).html(html);
				}

			});
			var width1 = $(document).width();
			if ((width1 > 575) && (width1 < 1200)) {
				var showChar1 = 28;
			} else if (width1 < 574) {
				var showChar1 = 47;
			} else if (width1 > 1200) {
				var showChar1 = 29;
			}
			var ellipsestext1 = "...";


			$('.more3').each(function() {
				var content1 = $(this).html();

				if (content1.length > showChar1) {

					var c1 = content1.substr(0, showChar1);
					var h1 = content1.substr(showChar1, content1.length - showChar1);

					var html1 = c1 + '<span class="moreellipses">' + ellipsestext1 + '&nbsp;</span><span class="morecontent"><span>' + h1;

					$(this).html(html1);
				}

			});
		});
	</script>

	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-133390883-2');
	</script>
	<link rel="manifest" href="/manifest.json" />
	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
</body>

</html>