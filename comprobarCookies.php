<?php
function is_session_started()
{
    if (php_sapi_name() !== 'cli') {
        if (version_compare(phpversion(), '5.4.0', '>=')) {
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            return session_id() === '' ? FALSE : TRUE;
        }
    }
    return FALSE;
}
if (is_session_started() === FALSE) session_start();
if ((isset($_COOKIE['id_pc']) and !isset($_COOKIE['PHPSESSID'])) || isset($_COOKIE['id_pc']) and !isset($_SESSION['usuario'])) { //Verifico si existen las coockie de auth y si la cookie de sesion ha expirado o no existe
    session_unset(); //Destruyo los remanentes de la sesion anterior
    session_destroy(); //
    //Variables de identificacion del usuario usando las cookies
    $id_pc = $_COOKIE['id_pc'];
    $passHash = $_COOKIE['auth_hash'];
    $us_theme = "main";

    try {
        $sql_cookie = "SELECT * FROM usuarios WHERE usuarios.Id = '" . $id_pc . "'";
        $resultado_cookie = $base->prepare($sql_cookie);
        $resultado_cookie->execute(array());
        $crow_cookie = $resultado_cookie->fetch(PDO::FETCH_ASSOC);

        $us_theme = $crow_cookie['us_theme'];

        if ($crow_cookie['passHash'] == $passHash) {
            session_start();                                    //llamo funcion crear sesion
            $_SESSION["usuario"] = $crow_cookie['Nombre'];                //le doy nombre al usuario de la sesion
            $_SESSION["idUser"] = $crow_cookie['Id'];                    //guardo el id del usuario en la base de datos
            $_SESSION["admin"] = $crow_cookie['admin'];                //Establece si el usuario es administrador o no
        }
    } catch (Exception $e) {
        echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
    }
}
