<meta charset="UTF-8">
<title><?php echo $config['descripcion'] . " | " . $config['nombre']; ?></title>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<meta property="fb:app_id" content="1929989280461762" />
<meta property="og:title" content="<?php echo $config['descripcion'] . " | " . $config['nombre']; ?>" />
<meta property="og:url" content="<?php echo $config['base_url']; ?>" />
<meta property="og:site_name" content="<?php echo $config['nombre']; ?>">
<meta property="og:image" content="<?php echo $config['base_url']; ?>img/r.svg" />
<meta property="og:description" content="<?php echo $config['long_descripcion']; ?>" />

<link rel="icon" type="image/x-icon" href="<?php echo $config['base_url']; ?><?php echo $config['dir_img']; ?>favicon.png">

<link id="main-theme" rel="stylesheet" type="text/css" href="<?php echo $config['base_url']; ?><?php echo $config['dir_css']; ?><?php echo $config['name_css']; ?>">

<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url']; ?><?php echo $config['dir_css']; ?>bootstrap.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<link rel="stylesheet" href="<?php echo $config['base_url']; ?><?php echo $config['dir_css']; ?>owl.carousel.css">

<link rel="stylesheet" href="<?php echo $config['base_url']; ?><?php echo $config['dir_css']; ?>owl.theme.default.css">

<!-- Alertify -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154940141-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-154940141-1');
</script>
<!-- Notificaciones OneSignal -->
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
        OneSignal.init({
            appId: "7b9f7426-7299-42b5-83ab-6ae2dd5e6e28",
        });
    });
</script>