<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru, Niko_  -->
<?php
include '../core/conexion.php';
include '../bin/funciones.php';
$buscar = '';
$buscar2 = '';
if (isset($_POST['buscar'])) {
	$buscar = $_POST['buscar'];
} else if (isset($_POST['buscar2'])) {
	$buscar = $_POST['buscar2'];
}
$sql = "SELECT * FROM series WHERE StrNombre LIKE '%" . $buscar . "%' LIMIT 5";
$resultado = $base->query($sql);
$fila = $resultado->fetch(PDO::FETCH_ASSOC);
$total = count($fila);
if ($fila['pageVersion'] == "2.0") {
	$pageVersion_fila = "serieV2";
} else {
	$pageVersion_fila = "serie";
}
?>
<?php
if ($total > 0 && $buscar != '') { ?>
	<div class="resultado">
		<h6>Se encontro:</h6>
		<?php
			do {
				?>
			<div class="resultado-q">

				<?php
						$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $fila['StrNombre']));
						echo "
	 <div class='container'>
	 <a href='../../" . $pageVersion_fila . "/" . url($fila['Id'], $nombreFiltrado) . "'>
		<div class='row'>
			<div class='col-3'>
				<figure class='q_img'><img class='img-fluid' src='" . $fila['StrImagen'] . "' alt=''></figure>
			</div>
			<div class='col-9 p-2'>
				<h5 style='border-bottom: 1px solid #ebcc43;'>
					<span>" . $fila['StrNombre'] . "</span>
				</h5>
				<p id='caps_scrollbar' style='max-height:50px;overflow:auto;'>" . $fila['StrSinopsis'] . "</p>
			</div>
		</div>
		</a>
	</div>
		
	 ";

						?>
			</div>
		<?php
			} while ($fila = $resultado->fetch(PDO::FETCH_ASSOC));
			?>
	<?php
	} elseif ($total > 0 && $buscar == '') echo "";
	else echo "<div class='resultado'><h6>No se encontraron resultados</h6></div>";
	?>