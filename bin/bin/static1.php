<?php
if (isset($_SESSION["usuario"]) && $_SESSION['admin'] == 1) {
    echo '<ul style="background-color:#ebcc43;position:relative;z-index: 100;" class="nav justify-content-center">
					<li class="nav-item">
						<a title="Panel Admin" href="' . $config['base_url'] . 'admin/administracion.php" class="nav-link badge badge-success">' . $_SESSION["usuario"] . ' Panel Admin</a>
					</li>
				</ul>';
} else {
    echo '';
}
?>
	<?php
    if (isset($_GET['registro'])) {
        $registro_usuario = $_GET["registro"];
        if ($registro_usuario == "correcto") {
            echo '
				<div class="alert">
					<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
					¡Listo! Ya estas registrado en AnimeRE. Ahora puedes <a title="Ingresa" href="login.php">Ingresar</a>
				</div>
			  ';
        }
    }
    ?>