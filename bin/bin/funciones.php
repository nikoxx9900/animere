
<?php
function url($id, $titulo)
{
	$titulo = $id . '/' . $titulo;
	$titulo = strtolower($titulo);
	$titulo = trim($titulo);
	$titulo = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$titulo
	);
	$titulo = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$titulo
	);
	$titulo = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$titulo
	);
	$titulo = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$titulo
	);
	$titulo = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$titulo
	);
	$titulo = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$titulo
	);
	$titulo = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$titulo
	);
	$titulo = str_replace("/", "/", $titulo);
	$titulo = str_replace(":", "", $titulo);
	$titulo = str_replace(" ", "-", $titulo);
	$titulo = str_replace("--", "-", $titulo);
	return $titulo;
}
function nombre($nombre)
{
	$nombre = str_replace("-", " ", $nombre);
	$nombre = str_replace("-", ":", $nombre);
	return $nombre;
}
function urln($id, $nombre)
{
	$titulo = $id . '/' . $nombre . '-cap-' . $id;
	$titulo = strtolower($titulo);
	$titulo = trim($titulo);
	$titulo = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$titulo
	);
	$titulo = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$titulo
	);
	$titulo = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$titulo
	);
	$titulo = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$titulo
	);
	$titulo = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$titulo
	);
	$titulo = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$titulo
	);
	$titulo = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$titulo
	);
	$titulo = str_replace("/", "/", $titulo);
	$titulo = str_replace(":", "", $titulo);
	$titulo = str_replace(" ", "-", $titulo);
	return $titulo;
}
function urll($id, $nombre)
{
	$titulo = $id . '/' . $nombre . '-cap-' . ($id);
	$titulo = strtolower($titulo);
	$titulo = trim($titulo);
	$titulo = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$titulo
	);
	$titulo = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$titulo
	);
	$titulo = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$titulo
	);
	$titulo = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$titulo
	);
	$titulo = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$titulo
	);
	$titulo = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$titulo
	);
	$titulo = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$titulo
	);
	$titulo = str_replace("/", "/", $titulo);
	$titulo = str_replace(":", "", $titulo);
	$titulo = str_replace(" ", "-", $titulo);
	return $titulo;
}
function urlCat($titulo)
{
	$titulo = $id . '/' . $titulo;
	$titulo = strtolower($titulo);
	$titulo = trim($titulo);
	$titulo = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$titulo
	);
	$titulo = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$titulo
	);
	$titulo = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$titulo
	);
	$titulo = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$titulo
	);
	$titulo = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$titulo
	);
	$titulo = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$titulo
	);
	$titulo = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$titulo
	);
	$titulo = str_replace("/", "/", $titulo);
	$titulo = str_replace(":", "", $titulo);
	$titulo = str_replace(" ", "-", $titulo);
	return $titulo;
}
function titulo($titulito)
{
	$titulito = ucwords($titulito) . " sub español | Anime online en HD";
	$titulito = str_replace(":", "", $titulito);
	$titulito = str_replace("-", " ", $titulito);

	echo $titulito;
}
function urlCap($url, $IdCap)
{
	$url = $url . '/' . $IdCap;
	$url = strtolower($url);
	$url = trim($url);
	$url = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$url
	);
	$url = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$url
	);
	$url = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$url
	);
	$url = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$url
	);
	$url = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$url
	);
	$url = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$url
	);
	$url = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$url
	);
	$url = str_replace("/", "/", $url);
	$url = str_replace(":", "", $url);
	$url = str_replace(" ", "-", $url);
	return $url;
}
function space($dato)
{
	$dato = $dato . '/';
	$dato = strtolower($dato);
	$dato = trim($dato);
	$dato = str_replace(
		array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		'a',
		$dato
	);
	$dato = str_replace(
		array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		'e',
		$dato
	);
	$dato = str_replace(
		array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		'i',
		$dato
	);
	$dato = str_replace(
		array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		'o',
		$dato
	);
	$dato = str_replace(
		array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		'u',
		$dato
	);
	$dato = str_replace(
		array('ñ', 'Ñ', 'ç', 'Ç'),
		array('n', 'N', 'c', 'C',),
		$dato
	);
	$dato = str_replace(
		array(
			"\\", "¨", "º", "-", "~",
			"#", "@", "|", "!", "\"",
			"·", "$", "%", "&",
			"(", ")", "?", "'", "¡",
			"¿", "[", "^", "`", "]",
			"+", "}", "{", "¨", "´",
			">", "< ", ";", ",", ":",
			"."
		),
		'-',
		$dato
	);
	$dato = str_replace("/", "/", $dato);
	$dato = str_replace(":", "", $dato);
	$dato = str_replace(" ", "-", $dato);
	return $dato;
}
function time_elapsed_string($datetime, $full = false)
{
	date_default_timezone_set('America/Bogota');
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'año',
		'm' => 'mes',
		'w' => 'semana',
		'd' => 'dia',
		'h' => 'hora',
		'i' => 'minuto',
		's' => 'segundo',
	);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? 'Hace ' . implode(', ', $string)  : 'justo ahora';
}

//FUNCIONES PARA GESTIÓN DE COTNRASEÑAS
//Pimienta para todas las contraseñas
$pepper = "ReAnime";

//Genera una cadena de 32 caracteres pseudo-aleatorios para usar como salt
function getSalt()
{
	return bin2hex(random_bytes(16));
}

//Devuelve una contraseña cifrada en MD5 usando salt&pepper
function saltPepper($pass, $salt, $pepper)
{
	return md5($pass . $salt . $pepper);
}
function players($id)
{
	include '../bin/core/conexion.php';
	$sql_p = "SELECT * FROM players";
	$resultado_p = $base->prepare($sql_p);
	$resultado_p->execute(array());
	echo "<div class='space'><select class='form-control' style='border:2px solid #fff;color:#fff;' name='s" . $id . "'><option value='0'>(Selecciona un Servidor)</option>";
	while ($crow_p = $resultado_p->fetch(PDO::FETCH_ASSOC)) {
		echo "<option  value='" . $crow_p['player'] . "'>" . $crow_p['player'] . "</option>";
	}
	echo "</select></div>";
}
function renderCap($mostrar_ocultos, $cantidad_a_mostrar, $orden)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	$orden_populares = "AND capitulos.fecha >= curdate() - INTERVAL DAYOFWEEK(curdate())+7 DAY AND capitulos.fecha < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY ";
	$orden_recientes = "";
	if ($mostrar_ocultos == false) {
		$mostrar_ocultos_def = "WHERE capitulos.oculto = 0";
	} else {
		$mostrar_ocultos_def = "";
	}
	if (!isset($cantidad_a_mostrar)) {
		$cantidad_a_mostrar_def = "40";
	} else {
		$cantidad_a_mostrar_def = $cantidad_a_mostrar;
	}
	if ($orden == "populares") {
		$orden_mostrar = $orden_populares;
	} else if ($orden == "recientes") {
		$orden_mostrar = $orden_recientes;
	}
	try {
		$sql = "SELECT capitulos.Id,capitulos.IdRel,capitulos.StrNombre,capitulos.StrImagen,capitulos.nCap,capitulos.visitas,series.tipo, capitulos.fecha FROM capitulos 
    INNER JOIN series on capitulos.IdRel=series.Id " . $mostrar_ocultos_def . " " . $orden_mostrar . "ORDER BY capitulos.Id DESC LIMIT " . $cantidad_a_mostrar_def . "";
		$resultado = $base->prepare($sql);
		$resultado->execute(array());
		while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
			$visitas = $crow['visitas'];
			if ($visitas > 0 && $visitas < 300) {
				$visita = "<span class='vistas-cap-badge badge-pills badge-are'>" . $visitas . " vistas  </span>";
			} else if ($visitas > 300) {
				$visita = "<span class='vistas-cap-badge badge-pills badge-are'><i style='color: #ff4500 !important;' class='fab fa-hotjar'></i> " . $visitas . " vistas  </span>";
			} else {
				$visita = "";
			}
			$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
			if ($crow['tipo'] == 0) {
				$infoCap = "Capitulo " . $crow['nCap'];
			} else if ($crow['tipo'] == 1) {
				$infoCap = "película";
			} else if ($crow['tipo'] == 2) {
				$infoCap = "OVA";
			} else if ($crow['tipo'] == 3) {
				$infoCap = "ONA";
			} else {
				$infoCap = "Especial";
			}
			echo "
            <div class='col col-lg col-md col-sm are_mobile m-1'>
            <div class='capitulo_are'>
                <a title='" . $crow['StrNombre'] . " sub español' class='post-vp' href='ver/" . url($crow['Id'], $nombreFiltrado) . "'>
                    <div style='display:flex;justify-content:center;align-items:center;'><i class='far fa-play-circle are_icon_c'></i></div>
                    <div class='img-fluid-are-div'><img style='min-width:230px;' class='img-fluid-are' src='" . $crow['StrImagen'] . "' alt='" . $crow['StrNombre'] . "'></div>
                    <div class='title_cap row m-0'>
                        <div class='col-12'><span class='more2 are-h5'>" . $crow['StrNombre'] . "</span></div>
                        <div class='col'>" . $visita . "<span class='cap-badge'></span> <span class='cap-badge badge-pills badge-are'>  •  " . $infoCap . "  •  sub español  •  HD</span></div>
                    </div>
                </a>
            </div>
        </div>				
    ";
		}
	} catch (Exception $e) {
		echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
	}
}
function renderCapPopulares($cantidad_mostrar)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	try {
		$sql_capRecomendado = "SELECT * FROM capitulos WHERE capitulos.oculto = 0 AND capitulos.fecha >= curdate() - INTERVAL DAYOFWEEK(curdate())+7 DAY
		AND capitulos.fecha < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY ORDER BY capitulos.visitas DESC LIMIT " . $cantidad_mostrar . "";
		$resultado_recomendado = $base->prepare($sql_capRecomendado);
		$resultado_recomendado->execute(array());
		while ($crow1 = $resultado_recomendado->fetch(PDO::FETCH_ASSOC)) {
			$visitas1 = $crow1['visitas'];
			if ($visitas1 > 0 && $visitas1 < 100) {
				$visita1 = "<span class='vistas-cap-badge badge-pills badge-are'>" . $visitas1 . " vistas  </span>";
			} else if ($visitas1 > 100) {
				$visita1 = "<span class='vistas-cap-badge badge-pills badge-are'><i style='color: #ff4500 !important;' class='fab fa-hotjar'></i> " . $visitas1 . " vistas  </span>";
			} else {
				$visita1 = "";
			}
			$nombreFiltrado1 = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow1['StrNombre']));
			$infoCap1 = "Capitulo " . $crow1['nCap'];
			echo "
				<div class='p-0'>
				<div class='capitulo_are'>
					<i class='far fa-play-circle are_icon_c'></i>
					<a title='" . $crow1['StrNombre'] . "' class='post-vp' href='../../ver/" . url($crow1['Id'], $nombreFiltrado1) . "'>
						<div class='img-fluid-are-div'><img class='img-fluid-are' src='" . $crow1['StrImagen'] . "' alt='" . $crow1['StrNombre'] . "'></div>
						<div class='title_cap row m-0'>
							<div class='col-12'><span class='more3 are-h5'>" . $crow1['StrNombre'] . "</span></div>
							<div class='col'>" . $visita1 . "<span class='cap-badge'></span> <span class='cap-badge badge-pills badge-are'>  •  " . $infoCap1 . "</span></div>
						</div>
					</a>
				</div>
			</div>				
		";
		}
	} catch (Exception $e) {
		echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
	}
}
function renderSeries($cantidad_series_mostrar, $series_orden, $activar_descripcion)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	if ($series_orden == "populares") {
		$series_orden_def = "WHERE series.enSlider = 1 ORDER BY RAND()";
	} else if ($series_orden == "recientes") {
		$series_orden_def = "ORDER BY series.Id";
	} else if ($series_orden == "random") {
		$series_orden_def = "ORDER BY RAND()";
	}

	$sql_ar = "SELECT * FROM series " . $series_orden_def . " DESC LIMIT " . $cantidad_series_mostrar . "";
	$resultado_ar = $base->query($sql_ar);
	while ($crow_ar = $resultado_ar->fetch(PDO::FETCH_ASSOC)) {
		$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow_ar['StrNombre']));
		if ($crow_ar['tipo'] == 0) {
			$tipoA = "TV";
		} else if ($crow_ar['tipo'] == 1) {
			$tipoA = "Película";
		} else if ($crow_ar['tipo'] == 2) {
			$tipoA = "OVA";
		} else if ($crow_ar['tipo'] == 3) {
			$tipoA = "ONA";
		} else {
			$tipoA = "Especial";
		}
		if ($crow_ar['pageVersion'] == "2.0") {
			$pageVersion_ar = "serieV2";
		} else {
			$pageVersion_ar = "serie";
		}
		if ($crow_ar['estado1'] == "Finalizado") {
			$color_info = "dc3545";
		} else {
			$color_info = "28a745";
		}
		if ($activar_descripcion == TRUE) {
			$desc_container = '<p class="a_description more">' . $crow_ar['StrSinopsis'] . '</p>';
		} else {
			$desc_container = '';
		}
		$fechaFormatInit_bottom = $crow_ar['StrFechaEstreno'];
		$fechaFormat_bottom = date("d/m/Y", strtotime($fechaFormatInit_bottom));
		setlocale(LC_ALL, "es_ES");
		$string = $fechaFormat_bottom;
		$date = DateTime::createFromFormat("d/m/Y", $string);
		$fechaFormat_bottom = strftime("%b %Y", $date->getTimestamp());
		echo '
					<div class="anime-card-rec m-1 anime_recomendados">
						<div class="card">
							<a title="' . $crow_ar['StrNombre'] . '" href="../../' . $pageVersion_ar . '/' . url($crow_ar["Id"], $nombreFiltrado) . '">
								' . $desc_container . '
								<div class="div_img_s"><img src="' . $crow_ar['StrImagen'] . '" class="card-img-top rounded-0" alt="' . $crow_ar['StrNombre'] . '"></div>
								<div class="are_info_s">
									<span class="are_s_title">' . $crow_ar['StrNombre'] . '</span><br>
									<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">' . ucfirst($fechaFormat_bottom) . '  • </span>
									<span style="color:#' . $color_info . ';font-size:0.8rem;left:5px;position:relative;">' . $crow_ar['estado1'] . '</span>
									<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">   •  ' . $tipoA . '</span>
								</div>
							</a>
						</div>
					</div>
					';
	}
}
function renderNoticias()
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	$sql_noticias = "SELECT * FROM noticias ORDER BY id DESC LIMIT 1";
	$resultado_noticias = $base->query($sql_noticias);
	while ($crow_noticias = $resultado_noticias->fetch(PDO::FETCH_ASSOC)) {
		if ($crow_noticias['link'] == NULL) {
			$linkNoticia = "";
		} else {
			$linkNoticia = '<span><a target="_blank" href="' . $crow_noticias['link'] . '" class="re-novedad btn btn-warning">Ir al Link <i class="fas fa-link"></i></a></span>';
		}
		// if ($crow_noticias['imagen'] == 0) {
		// 	$img = '<span>Hoy no hay imagen :v</span>';
		// } else {
		// 	$img = '<img src="' . $crow_noticias['imagen'] . '" class="img-fluid" />';
		// }
		echo '
		<div class="container">
			<div class="row justify-content-center pl-2 pr-2">
				<h4 class="re-novedad text-center pr-1 pl-1 text-light" style="background-color:#222;border:1px solid #ebcc43;">' . $crow_noticias['comentario'] . '</h4>
			</div>
			<div class="row justify-content-center pl-2 pr-2 mb-2">
				' . $linkNoticia . '
			</div>
		</div>
		';
	}
}
function renderVerLuego($idUser)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	$sql_verLuego = "SELECT * FROM series INNER JOIN verLuego ON series.Id = verLuego.idSerie WHERE verLuego.idUser = '" . $idUser . "' ORDER BY verLuego.id DESC";
	$resultado_verLuego = $base->query($sql_verLuego);
	$rc = $resultado_verLuego->rowCount();
	if ($rc < 1) {
		echo "No tienes pendiente ninguna serie. <strong><a style='color:#ebcc43;' href='https://renimation.com/animes'>¡Agrega una explorando nuestro catalogo!</a></strong>";
	} else {
		while ($crow_verLuego = $resultado_verLuego->fetch(PDO::FETCH_ASSOC)) {
			$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow_verLuego['StrNombre']));
			if ($crow_verLuego['tipo'] == 0) {
				$tipoA = "TV";
			} else if ($crow_verLuego['tipo'] == 1) {
				$tipoA = "Película";
			} else if ($crow_verLuego['tipo'] == 2) {
				$tipoA = "OVA";
			} else if ($crow_verLuego['tipo'] == 3) {
				$tipoA = "ONA";
			} else {
				$tipoA = "Especial";
			}
			if ($crow_verLuego['pageVersion'] == "2.0") {
				$pageVersion_verLuego = "serieV2";
			} else {
				$pageVersion_verLuego = "serie";
			}
			if ($crow_verLuego['estado1'] == "Finalizado") {
				$color_info = "dc3545";
			} else {
				$color_info = "28a745";
			}
			$fechaFormatInit_bottom = $crow_verLuego['StrFechaEstreno'];
			$fechaFormat_bottom = date("d/m/Y", strtotime($fechaFormatInit_bottom));
			setlocale(LC_ALL, "es_ES");
			$string = $fechaFormat_bottom;
			$date = DateTime::createFromFormat("d/m/Y", $string);
			$fechaFormat_bottom = strftime("%b %Y", $date->getTimestamp());
			echo '
					<div class="anime-card-rec m-1 anime_recomendados">
						<div class="card">
							<a title="' . $crow_verLuego['StrNombre'] . '" href="../../' . $pageVersion_verLuego . '/' . url($crow_verLuego["Id"], $nombreFiltrado) . '">
								<div class="div_img_s"><img src="' . $crow_verLuego['StrImagen'] . '" class="card-img-top rounded-0" alt="' . $crow_verLuego['StrNombre'] . '"></div>
								<div class="are_info_s">
									<span class="are_s_title">' . $crow_verLuego['StrNombre'] . '</span><br>
									<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">' . ucfirst($fechaFormat_bottom) . '  • </span>
									<span style="color:#' . $color_info . ';font-size:0.8rem;left:5px;position:relative;">' . $crow_verLuego['estado1'] . '</span>
									<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">   •  ' . $tipoA . '</span>
								</div>
							</a>
						</div>
					</div>
					';
		}
	}
}
function renderCapVistos($idUser)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	try {
		$sql_capVistos = "SELECT * FROM capitulos INNER JOIN vistos ON capitulos.Id = vistos.id_capitulo WHERE vistos.id_usuario = '" . $idUser . "' ORDER BY vistos.id DESC LIMIT 30";
		$resultado_capVistos = $base->prepare($sql_capVistos);
		$resultado_capVistos->execute(array());
		while ($crow1 = $resultado_capVistos->fetch(PDO::FETCH_ASSOC)) {
			$visitas1 = $crow1['visitas'];
			if ($visitas1 > 0 && $visitas1 < 100) {
				$visita1 = "<span class='vistas-cap-badge badge-pills badge-are'>" . $visitas1 . " vistas  </span>";
			} else if ($visitas1 > 100) {
				$visita1 = "<span class='vistas-cap-badge badge-pills badge-are'><i style='color: #ff4500 !important;' class='fab fa-hotjar'></i> " . $visitas1 . " vistas  </span>";
			} else {
				$visita1 = "";
			}
			$nombreFiltrado1 = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow1['StrNombre']));
			$infoCap1 = "Capitulo " . $crow1['nCap'];
			echo "
				<div class='p-0'>
					<div class='capitulo_are'>
						<i class='far fa-play-circle are_icon_c'></i>
						<a title='" . $crow1['StrNombre'] . "' class='post-vp' href='../../ver/" . url($crow1['Id'], $nombreFiltrado1) . "'>
							<div class='img-fluid-are-div'><img  class='img-fluid-are' src='" . $crow1['StrImagen'] . "' alt='" . $crow1['StrNombre'] . "'></div>
							<div class='title_cap row m-0'>
								<div class='col-12'><span class='more3 are-h5'>" . $crow1['StrNombre'] . "</span></div>
								<div class='col'>" . $visita1 . "<span class='cap-badge'></span> <span class='cap-badge badge-pills badge-are'>  •  " . $infoCap1 . "</span></div>
							</div>
						</a>
					</div>
				</div>				
		";
		}
	} catch (Exception $e) {
		echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
	}
}
function renderFavoritos($idUser)
{
	$path = $_SERVER['DOCUMENT_ROOT'];
	include $path . '/bin/core/conexion.php';
	$sql_favoritos = "SELECT * FROM series INNER JOIN favoritos ON series.Id = favoritos.id_serie WHERE favoritos.id_usuario = '" . $idUser . "' ORDER BY favoritos.id DESC";
	$resultado_favoritos = $base->query($sql_favoritos);
	$rc = $resultado_favoritos->rowCount();
	if ($rc < 1) {
		echo "Aqui aparecerán las series que hayas marcado como Favoritas.";
	} else {
		while ($crow_favoritos = $resultado_favoritos->fetch(PDO::FETCH_ASSOC)) {
			$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow_favoritos['StrNombre']));
			if ($crow_favoritos['tipo'] == 0) {
				$tipoA = "TV";
			} else if ($crow_favoritos['tipo'] == 1) {
				$tipoA = "Película";
			} else if ($crow_favoritos['tipo'] == 2) {
				$tipoA = "OVA";
			} else if ($crow_favoritos['tipo'] == 3) {
				$tipoA = "ONA";
			} else {
				$tipoA = "Especial";
			}
			if ($crow_favoritos['pageVersion'] == "2.0") {
				$pageVersion_favoritos = "serieV2";
			} else {
				$pageVersion_favoritos = "serie";
			}
			if ($crow_favoritos['estado1'] == "Finalizado") {
				$color_info = "dc3545";
			} else {
				$color_info = "28a745";
			}
			$fechaFormatInit_bottom = $crow_favoritos['StrFechaEstreno'];
			$fechaFormat_bottom = date("d/m/Y", strtotime($fechaFormatInit_bottom));
			setlocale(LC_ALL, "es_ES");
			$string = $fechaFormat_bottom;
			$date = DateTime::createFromFormat("d/m/Y", $string);
			$fechaFormat_bottom = strftime("%b %Y", $date->getTimestamp());
			echo '
						<div class="anime-card-rec m-1 anime_recomendados hola" style="display:block;">
							<div class="card">
								<a title="' . $crow_favoritos['StrNombre'] . '" href="../../' . $pageVersion_favoritos . '/' . url($crow_favoritos["Id"], $nombreFiltrado) . '">
									<div class="div_img_s"><img src="' . $crow_favoritos['StrImagen'] . '" class="card-img-top rounded-0" alt="' . $crow_favoritos['StrNombre'] . '"></div>
									<div class="are_info_s">
										<span class="are_s_title">' . $crow_favoritos['StrNombre'] . '</span><br>
										<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">' . ucfirst($fechaFormat_bottom) . '  • </span>
										<span style="color:#' . $color_info . ';font-size:0.8rem;left:5px;position:relative;">' . $crow_favoritos['estado1'] . '</span>
										<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">   •  ' . $tipoA . '</span>
									</div>
								</a>
							</div>
						</div>
						';
		}
	}
}
?>
