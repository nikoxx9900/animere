<?php
$config = array(
    "base_url" => "https://renimation.com/",
    "dir_css" => "css/",
    "name_css" => "main1.12.2.css",
    "dir_js" => "js/",
    "dir_img" => "img/",
    "nombre" => "Renimation",
    "descripcion" => "Anime online sub español en HD",
    "long_descripcion" => "Mira tus Animes favoritos sin publicidad, online gratis en HD sub español, guarda tus series vistas y opina.",

);
