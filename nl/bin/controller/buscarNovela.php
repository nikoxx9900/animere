<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru, Niko_  -->
<?php
include '../core/conexion.php';
include '../bin/funciones.php';
$buscar = '';
if (isset($_POST['buscar'])) {
	$buscar = $_POST['buscar'];
}
$sql = "SELECT * FROM novelas WHERE Nombre LIKE '%" . $buscar . "%' LIMIT 5";
$resultado = $base->query($sql);
$fila = $resultado->fetch(PDO::FETCH_ASSOC);
$total = count($fila);
?>
<?php
if ($total > 0 && $buscar != '') { ?>
	<div class="resultado">
		<h6>Se encontro:</h6>
		<?php
			do {
				?>
			<div class="resultado-q">

				<?php
						$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $fila['Nombre']));
						echo "
	 <div class='container'>
		<div class='row'>
			<div class='col-3'>
				<figure class='q_img'><img class='img-fluid' src='" . $fila['imagen'] . "' alt=''></figure>
			</div>
			<div class='col-9 p-2'>
				<h5 style='border-bottom: 1px solid #ebcc43;'>
					<a href='../../novela/" . url($nombreFiltrado, $fila['Id']) . "'>" . $fila['Nombre'] . "</a>
				</h5>
				<p id='caps_scrollbar' style='max-height:50px;overflow:auto;'>" . $fila['sinopsis'] . "</p>
			</div>
		</div>
	</div>
		
	 ";

						?>
			</div>
		<?php
			} while ($fila = $resultado->fetch(PDO::FETCH_ASSOC));
			?>
	<?php
	} elseif ($total > 0 && $buscar == '') echo "";
	else echo "<div class='resultado'><h6>No se encontraron resultados</h6></div>";
	?>