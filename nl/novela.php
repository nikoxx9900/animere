<!DOCTYPE html>
<?php
include "bin/bin/funciones.php";
include "bin/core/conexion.php";
include 'comprobarCookies.php';
?>
<?php
try {
    $id = $_GET['Id'];
    $sql = "SELECT * FROM novelas WHERE Id='" . $id . "'";
    $resultado = $base->query($sql);
    $resultado->execute(array());
    $crow = $resultado->fetch(PDO::FETCH_ASSOC);
    $url1 = str_replace('../../', 'https://nl.animere.net/', $crow['imagenFondo']);
} catch (Exception $e) {
    echo "Error en linea: " . $e->getMessage();
}
?>

<html lang="es">

<head>
    <meta charset="UTF-8">
    <title><?php echo $crow['Nombre']; ?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta property="fb:app_id" content="1929989280461762" />
    <meta property="og:title" content="<?php titulo($_GET['Nombre']); ?>">
    <meta property="og:image" content="<?php echo $url1; ?>">
    <meta property="og:description" content="<?php echo json_encode($crow['StrSinopsis']); ?>">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <meta content="AnimeRE <?php echo $crow['Nombre']; ?> online gratis en hd, ver <?php echo $crow['Nombre']; ?> gratis online en AnimeRE" name="description" />
    <meta content="<?php echo $crow['Nombre']; ?> online gratis en hd completo en español, ver <?php echo $crow['Nombre']; ?> gratis online" name="keywords" />
    <meta content="all, index, follow" name="robots" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="https://nl.animere.net/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
    <style>
        .modal-content {
            background-color: #222 !important;
        }
    </style>
</head>

<body>
    <?php include 'navbar.php'; ?>
    <div style='position:fixed;height:100%;top:0;left:0;width:100%;z-index:-100;opacity:0.2;background-image: url("<?php echo $crow['imagenFondo']; ?>");background-size:cover;background-position:top;'></div>

    <?php
    if (isset($_SESSION["usuario"]) && $_SESSION["admin"] == 1) {
        echo '<div class="d-flex justify-content-center p-3" style="background-color:#ffffffc4;">
					<a href="https://animere.net/admin/administracion.php" class="nav-link btn btn-success">' . $_SESSION["usuario"] . ' Panel Admin</a>
					<button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#are-admin">
					Opciones Administrativas
					</button>
					
					</div>

					<!-- Modal -->
					<div class="modal fade" id="are-admin" tabindex="-1" role="dialog" aria-labelledby="are-adminLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="are-adminLabel">Modificar ' . $crow['Nombre'] . '</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<ul style="background-color:#111;padding:10px;position:relative;z-index: 100;" class="nav justify-content-center">
							<form method="post" action="../../form/refrescar.php" class="m-2">
								<div class="space">
									<select class="space form-control" type="text" name="estado1">
										<option value="Finalizado">Finalizado</option>
										<option value="En Emision">En Emision</option>
									</select>
								</div>
								<input  type="hidden" name="ids" value ="' . $id . '" readonly>
								<input name="estado" type="submit" class="btn btn-success" value="Actualizar">

							</form>
					
					
							<form method="post" action="../../form/img_p.php" enctype="multipart/form-data" class="m-2">
							<div class="box">
								<input type="file" name="imagen" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple/>
								<input  type="hidden" name="id_p" value ="' . $id . '" readonly>
								<input  type="hidden" name="nombre" value ="' . $crow['Nombre'] . '" readonly>
								<input name="imagen_p" type="submit" class="btn btn-success" value="Imagen Portada">
							</div>
								

							</form>
					
					
							<form method="post" action="../../form/img_f.php" enctype="multipart/form-data" class="m-2">
								<div class="box">
									<input type="file" name="imagen-fondo" id="file-2" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple/>
									<input  type="hidden" name="id_f" value ="' . $id . '" readonly>
									<input  type="hidden" name="nombre1" value ="' . $crow['Nombre'] . '" readonly>
									<input name="imagen_f" type="submit" class="btn btn-success" value="Imagen Fondo">
								</div>
								

							</form>
					
					</ul>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
						</div>
						</div>
					</div>
					</div>';
    } else {
        echo '';
    }
    ?>
    <div id="fb-root"></div>
    <script async defer src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>
    <?php include 'navbar-ver.php'; ?>
    <div class="container mt-2">
        <?php
        try {
            $id = $_GET['Id'];
            $nombre = $_GET['Nombre'];
            $sql = "SELECT * FROM novelas WHERE Id='" . $id . "'";
            $resultado = $base->query($sql);
            $resultado->execute(array());
            if ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                if ($crow['estado1'] == "En Publicacion") {
                    $colorE = "text-success";
                    $titE = "En Publicacion";
                } else {
                    $colorE = "text-danger";
                    $titE = "Finalizado";
                }
                $hiddenCat1 = "style='display:block;'";
                $hiddenCat2 = "style='display:block;'";
                $hiddenCat3 = "style='display:block;'";
                $hiddenCat4 = "style='display:block;'";
                $hiddenCat5 = "style='display:block;'";
                if ($crow['A5'] === "0") {
                    $hiddenCat5 = "style='display:none;'";
                }
                if ($crow['A4'] === "0") {
                    $hiddenCat4 = "style='display:none;'";
                }
                if ($crow['A3'] === "0") {
                    $hiddenCat3 = "style='display:none;'";
                }
                if ($crow['A2'] === "0") {
                    $hiddenCat2 = "style='display:none;'";
                }
                if ($crow['A1'] === "0") {
                    $hiddenCat1 = "style='display:none;'";
                }
            }
        } catch (Exception $e) {
            echo "Error en linea: " . $e->getMessage();
        }

        ?>
        <div class='row'>
            <div class='col-lg-3 col-md-4 col-sm-12 col-12'>
                <div class='anime_imagen2'><img class='are-in mx-auto d-block' src='<?php echo $crow['imagen']; ?>'></div>
                <div class="fb-page mx-1 d-none d-sm-none d-md-none d-lg-block" data-href="https://www.facebook.com/isAnimeRevolution" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/isAnimeRevolution" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/isAnimeRevolution">Anime.RE</a></blockquote>
                </div>
            </div>
            <div class='are-in anime-info col-lg-9 col-md-8 col-sm-12 col-12'>
                <div class='container-fluid'>
                    <div class='row d-flex titulo_anime justify-content-lg-left justify-content-sm-left justify-content-md-left justify-content-lg-center'>
                        <div style='border-bottom:solid 1px; border-color:#999;max-width:80%;padding-bottom:15px;' class='col-lg-6 col-12 col-sm-12 col-md-12'>
                            <h1 style='color:#fff;font-size:1.6rem;'><span style="color:#ff00ff;">Titulo:</span> <?php echo $crow['Nombre']; ?></h1>
                            <h2 style='color:#fff;font-size:1.2rem;'><span style="color:#ff00ff;">English:</span> <?php echo $crow['NombreIngles']; ?></h2>
                            <h3 style='color:#fff;font-size:1.1rem;'><span style="color:#ff00ff;">Japonés:</span> <?php echo $crow['NombreJapones']; ?></h3>
                        </div>
                        <div style='border-bottom:solid 1px; border-color:#999;max-width:80%;padding-bottom:15px;' class='col-lg-6 col-12 col-sm-12 col-md-12'>
                            <div id='estado' class='are-in boton_estado p-2 bd-highlight'>
                                <span style='margin-bottom:0.5rem !important; margin:auto;' class='<?php echo $colorE; ?>' href='#' disabled><span style="color:#ff00ff;">Estado:</span> <?php echo $titE; ?></span>
                            </div>
                            <div class='are-in boton_estado p-2 bd-highlight'>
                                <span style='margin-bottom:0.5rem !important; margin:auto;color:#fff;' class='' disabled><span style="color:#ff00ff;">Estreno:</span> <?php $fechaFormatInit = $crow['fechaEstreno'];
                                                                                                                                                                        $fechaFormat = date("d/m/Y", strtotime($fechaFormatInit));
                                                                                                                                                                        setlocale(LC_ALL, "es_ES");
                                                                                                                                                                        $string = $fechaFormat;
                                                                                                                                                                        $date = DateTime::createFromFormat("d/m/Y", $string);
                                                                                                                                                                        $fechaFormat = strftime("%e %b %Y", $date->getTimestamp());
                                                                                                                                                                        echo ucfirst($fechaFormat); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class='row d-flex descripcion_anime'>
                        <div class='d-flex flex-lg-row flex-md-row flex-sm-row bd-highlight mb-3 info_anime'>
                            <div <?php echo $hiddenCat1; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://animere.net/animes.php?cat=<?php echo $crow['A1']; ?>'><?php echo $crow['A1']; ?></a></div>
                            <div <?php echo $hiddenCat2; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://animere.net/animes.php?cat=<?php echo $crow['A2']; ?>'><?php echo $crow['A2']; ?></a></div>
                            <div <?php echo $hiddenCat3; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://animere.net/animes.php?cat=<?php echo $crow['A3']; ?>'><?php echo $crow['A3']; ?></a></div>
                            <div <?php echo $hiddenCat4; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://animere.net/animes.php?cat=<?php echo $crow['A4']; ?>'><?php echo $crow['A4']; ?></a></div>
                            <div <?php echo $hiddenCat5; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://animere.net/animes.php?cat=<?php echo $crow['A5']; ?>'><?php echo $crow['A5']; ?></a></div>
                        </div>
                        <div class='col-lg-12 caps_scrollbar' style='height:10vh;overflow:auto;'>
                            <p style='color:#fff; padding-top:15px;'><?php echo $crow['sinopsis']; ?></p>
                        </div>
                    </div>
                    <div class='row'>
                        <?php include "novelaCaps.php"; ?>
                    </div>
                </div>
            </div>
        </div>




    </div>
    <?php include "footer.php" ?>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../../js/bootstrap.js"></script>
    <script type="text/javascript" src="https://animere.net/js/ajax.js"></script>
</body>

</html>