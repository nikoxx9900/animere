    <div class="container-fluid ml-0 mr-0 pl-sm-3 pr-sm-3 pl-md-5 pr-md-3 pl-lg-5 pr-lg-5">
    	<div class="">
    		<nav class="navbar navbar-expand-lg navbar-dark bg-bark p-lg-0">
    			<a class="navbar-brand" href="https://animere.net/"><img src="https://nl.animere.net/img/nl.png" width="140" height=""></a>
    			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    				<span class="navbar-toggler-icon"></span>
    			</button>
    			<div class="collapse navbar-collapse justify-content-center" id="navbarTogglerDemo02">
    				<ul class="navbar-nav ml-auto mr-auto mt-2 mt-lg-0">
    					<li class="nav-item active">
    						<a class="text-are nav-link" href="https://nl.animere.net/"><b style="font-size:1.3rem !important;" class="are_nav_t"><i class="fas fa-home"></i> INICIO</b></a>
    					</li>
    					<li class="nav-item">
    						<a class="text-are nav-link" href="https://nl.animere.net/novelas.php"><b style="font-size:1.3rem !important;" class="are_nav_t"><i class="fas fa-list"></i> NOVELAS</b></a>
    					</li>
    					<li class="nav-item">
    						<a class="text-are nav-link" href="https://www.facebook.com/isAnimeRevolution" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class="are_nav_t"><i class="fab fa-facebook-square"></i></b></a>
    					</li>
    					<li class="nav-item">
    						<a class="text-are nav-link" href="https://twitter.com/isREnimatioN" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class="are_nav_t"><i class="fab fa-twitter"></i></b></a>
    					</li>
    					<?php
						if (isset($_SESSION["usuario"])) {
							echo '
								<li class="nav-item">
									<div class="dropdown">
										<button class="btn btn-are dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											' . $_SESSION["usuario"] . '
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="https://nl.animere.net/logout.php">Cerrar Sesión</a>
										</div>
									</div>
								</li>
								';
						} else {
							echo '
								<li class="nav-item">
									<div class="dropdown" style="padding-top:0.3rem;">
										<button class="btn btn-are dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<b style="font-size:0.8rem !important;" class="are_nav_t"><i class="fas fa-user"></i></b>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a href="https://nl.animere.net/login.php" class="dropdown-item are_nav_t"><i class="fas fa-user"></i> LOGIN</a>
											<a href="https://nl.animere.net/register.php" class="dropdown-item are_nav_t"><i class="fas fa-user"></i> REGISTRATE</a>
										</div>
									</div>
								</li>';
						}
						?>
    				</ul>
    				<form class="form-inline" id="buscar1" style="color: #fff0 !important;">
    					<form action="" method="post" class="navbar-form navbar-right" autocomplete="off" id="form-search">
    						<input name="buscar" id="buscar" method="post" type="text" class="form-control" placeholder="Buscar Novela">
    					</form>
    					<div id="resultado-q"></div>
    				</form>
    			</div>
    		</nav>
    	</div>
    </div>