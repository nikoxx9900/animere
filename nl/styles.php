<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/bootstrap.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/main1.11.css" media="screen,projection" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="icon" type="image/x-icon" href="https://nl.animere.net/img/faviconl.png">
<link rel="stylesheet" href="css/owl.theme.default.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />