<!DOCTYPE html>
<?php
include 'bin/core/conexion.php';
?>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AnimeRE</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="../../js/visible.js"></script>
    <!-- <script src="../../js/epub.js"></script>
    <script src="https://futurepress.github.io/epubjs-reader/js/reader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script> -->
    <script src="../../js/pdf.js"></script>
    <?php
    if (!isset($_SESSION['usuario'])) {
        echo '
        <script type="text/javascript" src="https://nl.animere.net/js/swfobject.js"></script>
        <script type="text/javascript" src="http://nl.animere.net/js/dtjava.js"></script>
        <script src="https://nl.animere.net/js/are-cookie.js"></script>
        <script>var ec1 = new evercookie();</script>
      ';
    }
    ?>
</head>

<body>
    <?php
    if (isset($_GET['novela']) && isset($_GET['capitulo'])) {
        $id = $_GET['capitulo'];
        $nombre = $_GET['novela'];
        try {
            $sql = "SELECT * FROM capitulos INNER JOIN novelas ON capitulos.idRel = novelas.id WHERE capitulos.id='" . $id . "'";
            $resultado = $base->prepare($sql);
            $resultado->execute(array());
            if ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                $novela = $crow['epub'];
            }
        } catch (Exception $e) {
            echo "Fallo en la base datos" . $e->getMessage();
        }
    } else {
        echo "Capitulo no encontrado, evita modificar la URL. Si el error persiste contactanos en Facebook @IsAnimeRevolution";
        die();
    }
    ?>
    <style>
        .re-visor {
            margin-right: auto;
        }
    </style>
    <div id="areNovela"></div>
    <input id="Y" type="hidden" name="Y" runat="server" />;

    <script>
        url = "<?php echo $novela; ?>";
        var thePdf = null;
        var scale = 2;

        pdfjsLib.getDocument(url).promise.then(function(pdf) {
            thePdf = pdf;
            viewer = document.getElementById('areNovela');

            for (page = 1; page <= pdf.numPages; page++) {
                canvas = document.createElement("canvas");
                canvas.className = 're-visor';
                canvas.id = page;
                viewer.appendChild(canvas);
                renderPage(page, canvas);
            }
        });

        function renderPage(pageNumber, canvas) {
            thePdf.getPage(pageNumber).then(function(page) {
                const viewport = page.getViewport(scale);
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                page.render({
                    canvasContext: canvas.getContext('2d'),
                    viewport: viewport
                });
            });
        }

        $(window).scroll(function() {
            $('#areNovela').find('.re-visor:visible').each(function(e) {
                if ($(this).visible(true) == true) {
                    var name = "ScrollPostion";
                    ec1.set(name, $(this).attr('id'));
                }

            });
        });
    </script>


    <!-- <script>
        var book = ePub(");
        var rendition = book.renderTo("areNovela", {
            manager: "continuous",
            flow: "scrolled",
            width: "100%"
        });
        var displayed = rendition.display();



        displayed.then(function(renderer) {
            // -- do stuff
        });

        // Navigation loaded
        book.loaded.navigation.then(function(toc) {
            // console.log(toc);
        });

        function applyTheme() {
            let theme = {
                bg: "#222222",
                fg: "#ffffff",
                l: "#0B4085",
                ff: "'Open Sans', sans-serif",
                fs: "11pt",
                lh: "1.4",
                ta: "justify",
                m: "0"
            };

            rendition.getContents().forEach(c => c.addStylesheetRules({
                "body": {
                    "background": theme.bg,
                    "color": theme.fg,
                    "font-family": `${theme.ff} !important`,
                    "font-size": `${theme.fs} !important`,
                    "line-height": `${theme.lh} !important`,
                    "text-align": `${theme.ta} !important`,
                    "padding-top": theme.m,
                    "padding-bottom": theme.m
                },
                "a": {
                    "color": "inherit !important",
                    "text-decoration": "none !important",
                    "-webkit-text-fill-color": "inherit !important"
                },
                "a:link": {
                    "color": `${theme.l} !important`,
                    "text-decoration": "none !important",
                    "-webkit-text-fill-color": `${theme.l} !important`
                },
                "a:link:hover": {
                    "background": "rgba(0, 0, 0, 0.1) !important"
                },
                "img": {
                    "max-width": "100% !important"
                },
            }));
        }

        rendition.hooks.content.register(applyTheme);
    </script> -->
</body>

</html>