<!DOCTYPE html>
<html lang="es">
<?php
include 'bin/core/conexion.php';
include 'bin/bin/funciones.php';
include 'comprobarCookies.php'
?>
<html>

<head>
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'navbar.php'; ?>

    <!-- Aqui el div de ANIMES POPULARES -->
    <div class="container">
        <div class="row justify-content-center are-are">
            <span style="font-size:1.75rem!important;" class="bdr-title title"><i class="far fa-play-circle"></i> NOVELAS LIGERAS</span>
        </div>
        <div class="row justify-content-center">
            <div class="owl-carousel owl-one">
                <?php
                $sql = "SELECT * FROM novelas ORDER BY Id DESC LIMIT 10";
                $resultado = $base->query($sql);
                while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['Nombre']));
                    if ($crow['estado'] == "Finalizado") {
                        $color_info = "dc3545";
                    } else {
                        $color_info = "28a745";
                    }
                    $fechaFormatInit_top = $crow['fechaEstreno'];
                    $fechaFormat = date("d/m/Y", strtotime($fechaFormatInit_top));
                    setlocale(LC_ALL, "es_ES");
                    $string = $fechaFormat;
                    $date = DateTime::createFromFormat("d/m/Y", $string);
                    $fechaFormat = strftime("%b %Y", $date->getTimestamp());
                    echo '
					<div class="anime-card-rec m-1 anime_recomendados">
						<div class="card">
							<a title="' . $crow['Nombre'] . '" href="../../novela/' . url($nombreFiltrado, $crow["Id"]) . '">
								
								<p class="a_description more">' . $crow['sinopsis'] . '</p>
								<div class="div_img_s"><img src="' . $crow['imagen'] . '" class="card-img-top rounded-0" alt="' . $crow['Nombre'] . '"></div>
								<div class="are_info_s">
									<span class="are_s_title">' . $crow['Nombre'] . '</span><br>
									<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;">' . ucfirst($fechaFormat) . '  • </span>
									<span style="color:#' . $color_info . ';font-size:0.8rem;left:5px;position:relative;">' . $crow['estado'] . '</span>
								</div>
							</a>
						</div>
					</div>
					';
                }
                ?>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.owl-one').owlCarousel({
                loop: true,
                margin: 3,
                nav: false,
                autoWidth: false,
                autoplay: false,
                autoPlaySpeed: 5000,
                autoPlayTimeout: 5000,
                autoplayHoverPause: true,
                items: 5,
                responsive: {
                    0: {
                        items: 2
                    },
                    500: {
                        items: 3,
                        margin: 3
                    },
                    800: {
                        items: 4,
                        margin: 3
                    },
                    1000: {
                        items: 5,
                        margin: 5
                    }
                }
            });
        });
    </script>
</body>

</html>