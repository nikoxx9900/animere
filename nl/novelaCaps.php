<div class="are-in container pl-3">
    <div class="panel-heading">
        <span style="padding:3px;color:#fff;font-size:1.6rem;"><i class="fas fa-th-list"></i> Lista de Capitulos</span>
    </div>
    <div>
        <div id="caps_scrollbar" class="col-lg-12 col-md-12 col-sm-12 col-12 tabla_episodios">
            <div class="panel panel-default">
                <table class="table">
                    <tr style="display:flex;flex-direction:column;" class="td-cap">
                        <?php
                        try {
                            $id = $_GET['Id'];
                            $nombre = $_GET['Nombre'];
                            $sql = "SELECT novelas.Id,novelas.sinopsis,capitulos.Nombre,capitulos.idRel,capitulos.Id,capitulos.visitas FROM novelas INNER JOIN capitulos ON novelas.Id = capitulos.IdRel WHERE idRel = " . $id . " ORDER BY capitulos.nCap DESC";
                            $resultado = $base->query($sql);
                            $hayEpisodios = 0; //Establece el valor inicial de 0 capitulos
                            while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                                $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['Nombre']));
                                //Comprobamos capitulos vistos
                                if (isset($_SESSION["usuario"])) { //Comprobamos si el usuario esta logeado
                                    $idUser = $_SESSION["idUser"];         //Id del usuario en la bd						
                                    $sqlVisto = "SELECT * FROM vistos WHERE id_usuario = :usuario AND id_capitulo = :capitulo";
                                    $usuario = htmlentities(addslashes($idUser));
                                    $capitulo = htmlentities(addslashes($crow['Id']));
                                    $resultadoVisto = $base->prepare($sqlVisto);
                                    $resultadoVisto->bindValue(':usuario', $usuario);
                                    $resultadoVisto->bindValue(':capitulo', $capitulo);
                                    $resultadoVisto->execute();
                                    $visto = "";
                                    if ($resultadoVisto->rowCount() > 0) {
                                        $visto .= "• <div class='badge badge-pills badge-are-c'>Visto <i class='fas fa-check'></i></div>";
                                    }
                                    ++$hayEpisodios;
                                } else {
                                    $cookie_name = $crow['Id'];
                                    $visto = "";
                                    if (!isset($_COOKIE[$cookie_name])) {
                                        $visto = "";
                                    } else {
                                        $visto .= "<div class='badge badge-pills badge-are-c'><i class='fas fa-check'></i></div>";
                                    }
                                    ++$hayEpisodios; //Si hay capitulos aumenta la variable en la cantidad de episodios
                                }
                                echo  "
											<td style='border-bottom: 1px solid #ff00ff;border-top:0;'><a href='../../leer/" . url($nombreFiltrado, $crow['Id']) . "'><i class='fas fa-book'></i> " . $crow['Nombre'] . " " . $visto . " • <span style='color:#ffffffc4;font-size:0.8rem;'>" . $crow['visitas'] . " Visitas</span></a></td>
											";
                            }
                            if ($hayEpisodios == 0) { //si no hay episodios devuelve un mensaje
                                echo "No hay episodios agregados aun.";
                            }
                        } catch (Exception $e) {
                            echo "Error en linea: " . $e->getMessage();
                        }
                        ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>