<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../bin/core/conexion.php';
include '../comprobarCookies.php';
include 'adminProtect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Agregar Novela</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="../js/dpdw.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<style>
		.morecontent span {
			display: none;
		}

		.morelink {
			display: block;
		}
	</style>
	<style>
		.add {
			padding: 20px;
			background-color: #007bff;
			/* Azul */
			color: white;
			margin-bottom: 15px;
		}

		.edit {
			padding: 20px;
			background-color: #ffc107;
			/* Amarillo */
			color: #222;
			margin-bottom: 15px;
		}

		.closebtn {
			margin-left: 15px;
			color: white;
			font-weight: bold;
			float: right;
			font-size: 22px;
			line-height: 20px;
			cursor: pointer;
			transition: 0.3s;
		}

		.closebtn:hover {
			color: black;
		}
	</style>
</head>

<body>
	<?php
	// include '../navbar-ver.php';
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php
				$envionoti = $_GET['novela'];
				if ($envionoti == "correcto") {
					$msg_toast =
						'<div class="add">
						<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span>
						Novela subida correctamente.
					</div>';
				}
				echo $msg_toast; ?>
				<div class="content">
					<h3 class="title mb-2"><i class="fas fa-plus-square"></i> Agregar Novela <span style="color:#5deb43;background-color:#222;">Verde = Manual</span></h3>
					<div class="form-group">
						<form method="post" action="form/up-v2.php" class="subida" enctype="multipart/form-data">
							<span id="error" style="color:#ff4500;"></span>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="name"><i class="fas fa-fingerprint"></i> ID de AniList</label>
								<input id="animeID" type="text" placeholder="ID de AniList" onKeyUp="getAnimeInfo()">
							</div>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="name"><i class="fas fa-heading"></i> Nombre de la Novela</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Boku no HeroAcademia...">
								<label for="name"><i class="fas fa-heading"></i> Nombre Ingles de la Novela</label>
								<input type="text" class="form-control" id="name-english" name="nombreIngles" placeholder="My Heroe Academia...">
								<label for="name"><i class="fas fa-heading"></i> Nombre Japones de la Novela</label>
								<input type="text" class="form-control" id="name-native" name="nombreJapones" placeholder="ソードアート">
							</div>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="name"><i class="fas fa-file-alt"></i> Sinopsis en ingles:</label>
								<span><a style="color:#ff4500;" id="sinopsis-ingles" href="" target="_blank">TRADUCCION CLICK AQUI</a></span><br>
								<label for="name"><i class="fas fa-file-alt"></i> Sinopsis</label>
								<textarea type="text" class="form-control" id="sinopsis" name="sinopsis" placeholder="Erase una vez un norteño..."></textarea>
							</div>
							<div for="imagen" class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label><i class="fas fa-image"></i> Imagen de Portada</label>
								<input type="file" name="imagen" />
								<label><i class="fas fa-images"></i> La imagen se agrega manual, puedes descargarla de el siguiente link:</label>
								<a style="font-size:1rem;color:#ff4500;" target="_blank" name="linkPortada" id="linkPortada" style="width:100%;" rows="1">Imagen Sugerida</a>
							</div>
							<div for="imagen-fondo" class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label><i class="fas fa-images"></i> Imagen de Fondo</label>
								<input type="file" name="imagen-fondo" />
							</div>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="fecha-estreno"><i class="fas fa-calendar-alt"></i> Fecha de Publicacion</label>
								<input id="fecha-estrenoAnim" class="form-control" type="date" name="fecha-estreno" placeholder="Fecha de estreno">
							</div>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="estado1"><i class="fas fa-satellite-dish"></i> Estado de Publicacion</label>
								<select class="space form-control" type="text" name="estado1" id="estado">
									<option value="Finalizado">Finalizado</option>
									<option value="En Publicacion">En Publicacion</option>
								</select>
							</div>
							<div class="form-group" style="background-color:#333;border: 1px solid #FF00FF;color:#fff;padding:0.4rem;">
								<label for="enSlider"><i class="fas fa-fire-alt" style="color:#ff4500;"></i> Añadir a Novelas Populares?</p>
									<select class="form-control" type="text" name="destacada">
										<option value="0">No (Por Defecto)</option>
										<option value="1">Si</option>
									</select>
							</div>
							<div class="form-group" style="background-color:#5deb43;color:#222;padding:0.4rem;">
								<label for=""><i class="fas fa-sticky-note"></i> Categorias en ingles para referencia</label>
								<p id="categorias" style="width:100%;"></p>
								<label for=""><i class="fas fa-sticky-note"></i> Categorias para la Novela</label>
								<?php
								try {
									include "../bin/core/conexion.php";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero1'><option value='0'>No Especificada</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "</select></div>";
									echo "<div class=''><select class='space form-control' type='text' name='gnero2'><option value='0'>No Especificada</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "</select></div>";
									echo "<div class=''><select class='space form-control' type='text' name='gnero3'><option value='0'>No Especificada</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero4'><option value='0'>No Especificada</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
									$sql = "SELECT * FROM categorias";
									$resultado = $base->prepare($sql);
									$resultado->execute(array());
									echo "<div class=''><select class='space form-control' type='text' name='gnero5'><option value='0'>No Especificada</option>";
									while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='" . $crow['Nombre'] . "'>" . $crow['Nombre'] . "</option>";
									}
									echo "</select></div>";
								} catch (Exception $e) {
									echo "Fallo en la base de datos " . $e->getLine();
								}
								?>
							</div>
							<br>
							<input type="submit" class="btn btn-success" value="Confirmar y Subir">
						</form>
						<br><br>

					</div>
				</div>
			</div>
			<div class="col-md-4 p-4">
				<div class="row">
					<div class="jumbotron" style="background-color:#333;border: 1px solid #FF00FF !important;color:#fff !important;">
						<h2>Acceso Rapido: Panel de Administracion<h2>
					</div>
					<a class="btn btn-primary btn-block" href="https://nl.animere.net/admin/subir-cap.php" role="button">Subir Capitulos</a>
					<a class="btn btn-danger btn-block" href="https://nl.animere.net/admin/add_category.php" role="button">Agregar Categorias</a>
					<a class="btn btn-success btn-block" href="https://nl.animere.net/admin/administracion.php" role="button">Volver al menu principal del Panel Admin</a>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<h5>Todos derechos reservados <span class="nm-footer">AnimeRE</span>.</h5>
		</div>
	</footer>

	<script>
		function getAnimeInfo() {
			var valueInput = document.getElementById("animeID").value;
			// Here we define our query as a multi-line string
			// Storing it in a separate .graphql/.gql file is also possible
			var query = `
		query ($id: Int) { # Define which variables will be used in the query (id)
		Media (id: $id, type: MANGA
		) { # Insert our variables into the query arguments (id) (type: ANIME is hard-coded in the query)
			id
			status
			genres
			coverImage {
				extraLarge
				large
				medium
				color
			}
			description
			title {
			romaji
			english
			native
			}
			startDate {
				year
				month
				day
			}
			endDate {
				year
				month
				day
			}
		}
		}
		`;

			// Define our query variables and values that will be used in the query request
			var variables = {
				id: valueInput
			};

			// Define the config we'll need for our Api request
			var url = 'https://graphql.anilist.co',
				options = {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Accept': 'application/json',
					},
					body: JSON.stringify({
						query: query,
						variables: variables
					})
				};

			// Make the HTTP Api request
			fetch(url, options).then(handleResponse)
				.then(handleData)
				.catch(handleError);

			function handleResponse(response) {
				return response.json().then(function(json) {
					return response.ok ? json : Promise.reject(json);
				});
			}

			function handleData(data) {
				console.log(data);
				document.getElementById("error").innerHTML = "";
				document.getElementById("name").value = data.data.Media.title.romaji;
				document.getElementById("name-english").value = data.data.Media.title.english;
				document.getElementById("name-native").value = data.data.Media.title.native;
				document.getElementById("sinopsis-ingles").href = "https://translate.google.com/?hl=es#view=home&op=translate&sl=en&tl=es&text=" + encodeURI(data.data.Media.description);
				document.getElementById("linkPortada").href = data.data.Media.coverImage.extraLarge;

				var monthFix = data.data.Media.startDate.month;
				var dayFix = data.data.Media.startDate.day
				if (monthFix < 10) {
					var monthFix = "0" + monthFix;
				}
				if (dayFix < 10) {
					var dayFix = "0" + dayFix;
				}
				var dateAnime = data.data.Media.startDate.year + "-" + monthFix + "-" + dayFix;
				document.getElementById("fecha-estrenoAnim").value = dateAnime;
				if (data.data.Media.status == "RELEASING") {
					document.getElementById("estado").value = "En Publicacion";
				} else {
					document.getElementById("estado").value = "Finalizado";
				}
				document.getElementById("categorias").innerHTML = data.data.Media.genres;
			}

			function handleError(error) {
				document.getElementById("error").innerHTML = 'No se encuentra la Novela';
				console.error(error);
			}
		}
	</script>
	<script>
		var close = document.getElementsByClassName("closebtn");
		var i;

		for (i = 0; i < close.length; i++) {
			close[i].onclick = function() {
				var div = this.parentElement;
				div.style.opacity = "0";
				setTimeout(function() {
					div.style.display = "none";
				}, 600);
			}
		}
	</script>
</body>

</html>