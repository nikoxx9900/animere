<?php
define("ROW_PER_PAGE",20);
?>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<style>
#keyword{color:#ebcc43;border: #CCC 1px solid; border-radius: 4px; padding: 7px;background:url("demo-search-icon.png") no-repeat center right 7px;}
</style>
</head>
<body>
<?php	
	$search_keyword = '';
	if(!empty($_POST['search']['keyword'])) {
		$search_keyword = $_POST['search']['keyword'];
	}
	$sql = 'SELECT * FROM capitulos WHERE capitulos.StrNombre LIKE :keyword ORDER BY id DESC';
	
	/* Pagination Code starts */
	$per_page_html = '';
	$page = 1;
	$start=0;
	if(!empty($_POST["page"])) {
		$page = $_POST["page"];
		$start=($page-1) * ROW_PER_PAGE;
	}
	$limit=" limit " . $start . "," . ROW_PER_PAGE;
	$pagination_statement = $base->prepare($sql);
	$pagination_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
	$pagination_statement->execute();

	$row_count = $pagination_statement->rowCount();
	if(!empty($row_count)){
		$per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
		$page_count=ceil($row_count/ROW_PER_PAGE);
		if($page_count>1) {
			for($i=1;$i<=$page_count;$i++){
				if($i==$page){
					$per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page current" />';
				} else {
					$per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page" />';
				}
			}
		}
		$per_page_html .= "</div>";
	}
	
	$query = $sql.$limit;
	$pdo_statement = $base->prepare($query);
	$pdo_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
?>
<form name='frmSearch' action='' method='post'>
<div style='text-align:left;color:#ebcc43;margin:20px 0px;'><input type='text' placeholder="Buscar" name='search[keyword]' value="<?php echo $search_keyword; ?>" id='keyword' maxlength='25'></div>
</form>
<table class='tbl-qa'>
  <thead>
	<tr>
	  <th class='table-header' width='80%'>Capitulo</th>
	  <th class='table-header' width='20%'>Accion</th>
	</tr>
  </thead>
  <tbody id='table-body'>
	<?php
	if(!empty($result)) { 
		foreach($result as $row) {
	?>
	  <tr class='table-row'>
	  <td><a style="color:#ebcc43 !important;" href="../../ver/<?php echo url($row["Id"],$row["StrNombre"]);?>"><?php echo $row['StrNombre']; ?></a></td>
		<td>
        <button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#modalBorrar<?php echo $row['Id'];?>'>
              <i class='far fa-trash-alt'></i>
              </button>
        <div class='modal fade' id='modalBorrar<?php echo $row['Id'];?>' tabindex='-1' role='dialog' aria-labelledby='modalBorrarTitle' aria-hidden='true'>
            <form method='post' role='form' action="delCap.php">
              <div class='modal-dialog modal-dialog-centered' role='document'>
              <div class='modal-content'>
                <div class='modal-header'>
                  <h5 class='modal-title' id='modalBorrar<?php echo $row['Id'];?>Title'>Borrar <?php echo $row['StrNombre'];?></h5>
                  
                  <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </div>
                <div class='modal-body'>
                    <h3>Seguro que quieres eliminar <?php echo $row['StrNombre'];?> ?</h3>
                </div>
                <div class='modal-footer'>
                    <input name="s_id" type="hidden" value="<?php echo $row['Id'];?>" >
                  <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>
                  <button name='borrar_s' type='submit' class='btn btn-danger'>Borrar (Irreversible)</button>
                </div>
              </div>
            </div>
            </form>
          </div>
        </td>
	  </tr>
    <?php
		}
	}
	?>
  </tbody>
</table>
<?php echo $per_page_html; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="bootstrap.js"></script>
</body>
</html>



        