<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
$nombre1 = $_POST['name'];
$nombreIngles = $_POST['nombreIngles'];
$nombreJapones = $_POST['nombreJapones'];
$sinopsis = $_POST['sinopsis'];
$sinopsis = str_replace('<br>', '', $sinopsis);
$sinopsis = str_replace('</br>', '', $sinopsis);
$fechaestreno = $_POST['fecha-estreno'];
$estado = $_POST['estado1'];
$a1 = $_POST['gnero1'];
$a2 = $_POST['gnero2'];
$a3 = $_POST['gnero3'];
$a4 = $_POST['gnero4'];
$a5 = $_POST['gnero5'];
$destacada = $_POST['destacada'];
$nombre = strtolower(preg_replace('/[\W\s\/]+/', '-', $nombre1));


/*PROCESADOR DE IMAGENES*/
if (is_array($_FILES)) {
	/*IMAGEN PORTADA*/

	$file = $_FILES['imagen']['tmp_name'];
	$sourceProperties = getimagesize($file);
	$fileNewName = $nombre . "_" . date('Y-m-d');
	$folderPath = "../../img/novelas/";
	$ext = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
	$ext_webpI = "jpg";
	$imageType = $sourceProperties[2];
	switch ($imageType) {
		case IMAGETYPE_PNG:
			$imageResourceId = imagecreatefrompng($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webpI, 95);
			break;
		case IMAGETYPE_GIF:
			$imageResourceId = imagecreatefromgif($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webpI, 95);
			break;
		case IMAGETYPE_JPEG:
			$imageResourceId = imagecreatefromjpeg($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webpI, 95);
			break;
		default:
			echo "Las imagenes solo puedes ser: .JPG .PNG o .GIF";
			exit;
			break;
	}
	/*IMAGEN FONDO*/
	$file2 = $_FILES['imagen-fondo']['tmp_name'];
	$sourceProperties2 = getimagesize($file2);
	$fileNewName2 = $nombre . "_f" . date('Y-m-d');
	$folderPath2 = "../../img/novelas/";
	$ext2 = pathinfo($_FILES['imagen-fondo']['name'], PATHINFO_EXTENSION);
	$ext_webp = "jpg";
	$imageType2 = $sourceProperties2[2];
	switch ($imageType2) {
		case IMAGETYPE_PNG:
			$imageResourceId2 = imagecreatefrompng($file2);
			$targetLayer2 = imageResizeFondo($imageResourceId2, $sourceProperties2[0], $sourceProperties2[1]);
			imagejpeg($targetLayer2, $folderPath2 . $fileNewName2 . "_thump." . $ext_webp, 90);
			break;
		case IMAGETYPE_GIF:
			$imageResourceId2 = imagecreatefromgif($file2);
			$targetLayer2 = imageResizeFondo($imageResourceId2, $sourceProperties2[0], $sourceProperties2[1]);
			imagejpeg($targetLayer2, $folderPath2 . $fileNewName2 . "_thump." . $ext_webp, 90);
			break;
		case IMAGETYPE_JPEG:
			$imageResourceId2 = imagecreatefromjpeg($file2);
			$targetLayer2 = imageResizeFondo($imageResourceId2, $sourceProperties2[0], $sourceProperties2[1]);
			imagejpeg($targetLayer2, $folderPath2 . $fileNewName2 . "_thump." . $ext_webp, 90);
			break;
		default:
			echo "Las imagenes solo puedes ser: .JPG .PNG o .GIF";
			exit;
			break;
	}
	/*FUNCIONES*/
	// move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);

	$imagen = "../" . $folderPath . $fileNewName . "_thump." . $ext_webpI;
	$imagenbg = "../" . $folderPath2 . $fileNewName2 . "_thump." . $ext_webp;
}
function imageResize($imageResourceId, $width, $height)
{
	$targetWidth = 225;
	$targetHeight = 320;
	$targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
	imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
	return $targetLayer;
}
function imageResizeFondo($imageResourceId2, $width2, $height2)
{
	$targetWidth2 = 1920;
	$targetHeight2 = 1080;
	$targetLayer2 = imagecreatetruecolor($targetWidth2, $targetHeight2);
	imagecopyresampled($targetLayer2, $imageResourceId2, 0, 0, 0, 0, $targetWidth2, $targetHeight2, $width2, $height2);
	return $targetLayer2;
}



try {
	include '../../bin/core/conexion.php';
	$sql = "INSERT INTO novelas (Nombre, NombreJapones, NombreIngles, imagen, imagenFondo, fechaEstreno, estado, sinopsis, 
	A1,A2,A3,A4,A5,destacada)
	VALUES (:Nombre, :NombreJapones, :NombreIngles, :imagen, :imagenFondo, :fechaEstreno, :estado, :sinopsis,:A1,:A2,:A3,:A4,:A5,:destacada)";

	$resultado = $base->prepare($sql);
	$resultado->execute(array(
		":Nombre" => $nombre1, ":NombreJapones" => $nombreJapones, ":NombreIngles" => $nombreIngles, ":imagen" => $imagen, ":imagenFondo" => $imagenbg, ":fechaEstreno" => $fechaestreno,
		":estado" => $estado, ":sinopsis" => $sinopsis, ":A1" => $a1, ":A2" => $a2, ":A3" => $a3, ":A4" => $a4, ":A5" => $a5, ":destacada" => $destacada
	));
	$resultado->closeCursor();
	header('location: ../subir-v2.php?novela=correcto');
} catch (Exception $e) {
	echo "Fallo en la base datos" . $e->getMessage();
}
?>