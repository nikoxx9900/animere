<!--AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
$nombre1 = $_POST['name'];
$nombre_link = $nombre1;
$novela = $_POST['idRel'];
$novela_e = explode(";", $novela);
$idRel = $novela_e[0];
$volumen = $_POST['volumen'];
$Nombre = $novela_e[1];
$oculto = $_POST['oculto'];
$nCap = $_POST['nCap'];
$fecha = $_POST['fecha'];
$args = $_POST['args'];
$visitas = "0";
$nombre1 = strtolower(preg_replace('/[\W\s\/]+/', '-', $Nombre));

if (isset($_FILES)) {
	if (is_array($_FILES) && $_FILES['epub']['size'] > 0) {
		$file_epub = $_FILES['epub']['tmp_name'];
		$fileNewName_epub = $nombre1 . "_" . $nCap;
		$folderPath_epub = "../../cdn/novelas/";
		$folderPath_epub_move = "../../cdn/novelas/";

		move_uploaded_file($file_epub, $folderPath_epub_move . $fileNewName_epub . ".epub");
		$epub = $folderPath_epub . $fileNewName_epub . ".epub";
	} else {
		die("No has agregado el epub del capitulo");
	}
}

/*PROCESADOR DE IMAGENES*/
if (is_array($_FILES)) {
	$file = $_FILES['imagen']['tmp_name'];
	$sourceProperties = getimagesize($file);
	$fileNewName = $nombre1 . "_" . date('Y-m-d');
	$folderPath = "../../img/capitulos/";
	$ext = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
	$ext_webp = "jpg";
	$imageType = $sourceProperties[2];
	switch ($imageType) {
		case IMAGETYPE_PNG:
			$imageResourceId = imagecreatefrompng($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
			break;
		case IMAGETYPE_JPEG:
			$imageResourceId = imagecreatefromjpeg($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
			break;

		default:
			echo "Las imagenes solo puedes ser: .JPG o .PNG";
			exit;
			break;
	}


	// move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
	$imagen = $folderPath . $fileNewName . "_thump." . $ext_webp;
}
function imageResize($imageResourceId, $width, $height)
{
	$targetWidth = 720;
	$targetHeight = 405;
	$targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
	imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
	return $targetLayer;
}



if ($idRel != 0) {
	try {
		include '../../bin/core/conexion.php';
		$sql = "INSERT INTO capitulos (idRel, Nombre, imagen, epub, volumen, nCap, oculto, visitas, fecha) 
		VALUES (:idRel, :Nombre, :imagen, :epub, :volumen, :nCap, :oculto, :visitas, :fecha)";

		$resultado = $base->prepare($sql);
		$resultado->execute(array(
			":idRel" => $idRel, ":Nombre" => $Nombre, ":imagen" => $imagen, ":epub" => $epub, ":volumen" => $volumen, ":nCap" => $nCap, ":oculto" => $oculto, ":visitas" => $visitas,
			":fecha" => $fecha
		));
		$resultado->closeCursor();
		echo "<script>window.location.replace('https://nl.animere.net/admin/subir-cap.php?cap=correcto');</script>";
	} catch (Exception $e) {
		echo "Fallo en la base datos" . $e->getMessage();
	}
} else {
	echo "Tienes que seleccionar una serie a la cual agregar el episodio.";
}

?>