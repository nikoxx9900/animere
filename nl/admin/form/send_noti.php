<?PHP
try{
    include '../bin/bin/funciones.php';						
    $sql2="SELECT * FROM capitulos WHERE capitulos.StrNombre = '".$nombre_link."'";
    $resultado2 = $base->prepare($sql2);
    $resultado2->execute(array());
    $crow2=$resultado2->fetch(PDO::FETCH_ASSOC);
    
}catch(Exception $e){
    echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
}

    $url_link = "https://animere.net/ver/". url($crow2['Id'],$nombre_url);
    $img_badge = "https://animere.net/img/favicon.png";
    if(!empty($texto_subtitulo)){
        $texto_sub = $texto_subtitulo;
    }else{
        $texto_sub = "Nuevo capitulo de ".$nombre_link;
    }

function sendMessage($nombre_link1,$texto_sub,$url,$img,$badge) {
    $titulo_noti = "Ver ".$nombre_link1;
    $content      = array(
        'en' => $texto_sub
	);
	$headings      = array(
        "en" => $titulo_noti
    );
    $fields = array(
        'app_id' => "744aebc2-8509-4ed9-8e2e-025fd206f36e",
        'included_segments' => array(
            'All'
        ),
		'contents' => $content,
		'headings' => $headings,
        'url' => $url,
        'chrome_web_image' => $img,
        'chrome_web_badge' => $badge
    );
    
    $fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic MTc3MTE0YTEtMzBlOC00NDczLWI2ZDMtODc5MDA5NjI4NzNk'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    return $response;
}

$response = sendMessage($nombre_link,$texto_sub,$url_link,$imagen_noti,$img_badge);
$return["allresponses"] = $response;
$return = json_encode($return);

$data = json_decode($response, true);
print_r($data);
$id = $data['id'];
print_r($id);

print("\n\nJSON received:\n");
print($return);
print("\n");

?>
