<?php
$id_capitulo = $_POST['id_capitulo'];
$idrel_capitulo = $_POST['idrel_capitulo'];
$url_cap = $_POST['url_cap'];
$mensaje = $_POST['mensaje'];
$tipo_reporte = $_POST['tipo_reporte'];
$fecha_reporte = $_POST['fecha_reporte'];
$email_usuario = $_POST['email_usuario'];
$ip_user = $_POST['ip_user'];


if ($email_usuario != NULL && $tipo_reporte > 0) {
    try {
        include '../bin/core/conexion.php';
        $sql = "INSERT INTO reportes (ip_user, email_usuario, id_capitulo, idrel_capitulo, url_cap, fecha_reporte, tipo_reporte, mensaje) 
        VALUES (:ip_user, :email_usuario, :id_capitulo,:idrel_capitulo,:url_cap, :fecha_reporte, :tipo_reporte, :mensaje)";

        $resultado = $base->prepare($sql);
        $resultado->execute(array(
            ":ip_user" => $ip_user, ":email_usuario" => $email_usuario, ":id_capitulo" => $id_capitulo, ":idrel_capitulo" => $idrel_capitulo, ":url_cap" => $url_cap,
            ":fecha_reporte" => $fecha_reporte, ":tipo_reporte" => $tipo_reporte, ":mensaje" => $mensaje
        ));
        $resultado->closeCursor();
        header('Location: ' . $_SERVER['HTTP_REFERER'] . "?sub=correcto");
    } catch (Exception $e) {
        echo "Fallo en la base datos" . $e->getMessage();
    }
} else {
    echo "Debes proporcionar un Email y seleccionar un tipo de problema para enviar tu reporte. (El email será usado solo con fines informativos para tenerte al tanto del reporte)";
}
