<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../bin/core/conexion.php';
include '../comprobarCookies.php';
include 'adminProtect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Administracion | Novelas | AnimeRE</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<script type="text/javascript" src="../js/dpdw.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<script type="text/javascript" src="../js/bootstrap.js"></script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="d-flex flex-column bd-highlight mb-3">
					<div class="p-2 bd-highlight"><a href="subir-v2.php" type="button" class="btn btn-info btn-lg btn-block">Agregar Novela</a></div>
					<div class="p-2 bd-highlight"><a href="subir-cap.php" type="button" class="btn btn-primary btn-lg btn-block">Agregar Capitulo</a></div>
					<div class="p-2 bd-highlight"><a href="add_category#.php" type="button" class="btn btn-info btn-lg btn-block">Agregar Categoria</a></div>
					<div class="p-2 bd-highlight"><a href="modificar#-cap.php" type="button" class="btn btn-danger btn-lg btn-block">Eliminar Capitulos</a></div>
					<div class="p-2 bd-highlight"><a href="modificar#.php" type="button" class="btn btn-danger btn-lg btn-block">Eliminar Novela</a></div>

				</div>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<h5>Este es un script realizado para <span class="nm-footer">AnimeRE 2019 (Novelas Ligeras)</span>.</h5>
		</div>
		<div class="konata" style="position:fixed;bottom:0;left:0;"><img data-toggle="tooltip" data-placement="top" title="Deja de holgazanear y ponte a subir animes '-.- la gente lo espera" src="konata.png" alt=""></div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
</body>

</html>