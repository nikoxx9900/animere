<!-- AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include 'adminProtect.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Test Anime</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script type="text/javascript" src="../js/dpdw.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <style>
        .morecontent span {
            display: none;
        }

        .morelink {
            display: block;
        }
    </style>
</head>

<body>
    <?php
    include '../navbar-ver.php';
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <input id="animeID" type="text" placeholder="ID de AniList" onKeyUp="getAnimeInfo()">
                <p id="resultado"></p>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <h5>Todos derechos reservados <span class="nm-footer">AnimeRE</span>.</h5>
            </div>
        </footer>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
        <script>
            function getAnimeInfo() {
                var valueInput = document.getElementById("animeID").value;
                // Here we define our query as a multi-line string
                // Storing it in a separate .graphql/.gql file is also possible
                var query = `
        query ($id: Int) { # Define which variables will be used in the query (id)
        Media (id: $id, type: ANIME
        ) { # Insert our variables into the query arguments (id) (type: ANIME is hard-coded in the query)
            id
            status
            format
    				description
    				meanScore
            episodes
            title {
            romaji
            native
            }
    				source
    				startDate {
    				  year
    				  month
    				  day
    				}
    				endDate {
    				  year
    				  month
    				  day
    				}
    				season
    				studios {
    				  edges {
    				    id
    				  }
    				}
        }
        }
        `;

                // Define our query variables and values that will be used in the query request
                var variables = {
                    id: valueInput
                };

                // Define the config we'll need for our Api request
                var url = 'https://graphql.anilist.co',
                    options = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                        body: JSON.stringify({
                            query: query,
                            variables: variables
                        })
                    };

                // Make the HTTP Api request
                fetch(url, options).then(handleResponse)
                    .then(handleData)
                    .catch(handleError);

                function handleResponse(response) {
                    return response.json().then(function(json) {
                        return response.ok ? json : Promise.reject(json);
                    });
                }

                function handleData(data) {
                    document.getElementById("resultado").innerHTML = data.data.Media.title.native;
                }

                function handleError(error) {
                    alert('Error, check console');
                    console.error(error);
                }
            }
        </script>
</body>

</html>