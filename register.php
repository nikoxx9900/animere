<?php

// initializing variables
$username = "";
$email    = "";
$errors = array();
$_SESSION['success'] = "";

// connect to the database
$db = mysqli_connect('localhost', 'root', 'weare991010rootnortetv', 'renimation');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) {
    array_push($errors, "¡Tu nombre de usuario es requerido!");
  }
  if (empty($email)) {
    array_push($errors, "¡Debes ingresar un email!");
  }
  if (empty($password_1)) {
    array_push($errors, "¡No olvides la contraseña papu!");
  }
  if ($password_1 != $password_2) {
    array_push($errors, "¡Las contraseñas no coinciden!");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM usuarios WHERE Nombre='$username' OR Email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['Nombre'] === $username) {
      array_push($errors, "¡Parece que ya estas registrado! Intenta ingresar como usuario.");
    }

    if ($user['Email'] === $email) {
      array_push($errors, "¡Este Email ya esta registrado en AnimeRE! <a href='https://renimation.com/login'><strong>Intenta Ingresar</strong></a>");
    }
  }
  function getSalt()
  {
    return bin2hex(random_bytes(16));
  }

  $pepper = "ReAnime";
  $passSalt = getSalt(); //string aleatoria de 16 caracteres que sirve de salt
  $pass = $password_1;

  $passHash = $pass . $passSalt . $pepper; //conbinamos todo

  $final = md5($passHash); //lo pasamos a MD5



  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {

    $query = "INSERT INTO usuarios (Nombre, Email, passHash, passSalt) 
  			  VALUES('$username', '$email', '$final','$passSalt')";
    mysqli_query($db, $query);
    $user_first_session = "SELECT * FROM usuarios WHERE Nombre='$username' OR Email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_first_session);
    $user = mysqli_fetch_assoc($result);
    session_start();
    $_SESSION['username'] = $username;
    $_SESSION['idUser'] = $user['id'];
    $_SESSION['success'] = "Ahora estas logeado!";
    header('location: index.php?registro=correcto');
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <title>Haz parte de nuestra comunidad | AnimeRE</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="shourtcut icon" type="image/x-icon" href="https://renimation.com/img/favicon.png">
  <style>
    * {
      margin: 0px;
      padding: 0px;
    }

    body {
      font-size: 120%;
      background: #222;
    }

    form {
      background-color: #2222228c;
      border: 1px solid #ebcc438c;
    }

    p {
      width: 92%;
      margin: 0 auto;
      margin-bottom: 10px;
    }

    .header {
      width: 30%;
      margin: 50px auto 0px;
      color: white;
      text-align: center;
      border-bottom: none;
    }

    .input-group {
      margin: 10px 0px 10px 0px;
    }

    .input-group label {
      display: block;
      text-align: left;
      margin: 3px;
      width: 92%;
      margin: 0 auto;
    }

    .input-group input {
      height: 30px;
      width: 92%;
      margin: 0 auto;
      padding: 5px 10px;
      font-size: 16px;
      border-radius: 5px;
      border: 1px solid gray;
    }

    .input-group button {
      width: 92%;
      margin: 0 auto;
    }

    .btn {
      padding: 10px;
      font-size: 15px;
      color: white;
      background: #ebcc43;
      border: none;
      border-radius: 5px;
    }

    .error {
      width: 92%;
      margin: 0px auto;
      margin-top: 10px;
      padding: 10px;
      border: 1px solid #a94442;
      color: #a94442;
      background: #f2dede;
      border-radius: 5px;
      text-align: left;
    }

    .success {
      color: #3c763d;
      background: #dff0d8;
      border: 1px solid #3c763d;
      margin-bottom: 20px;
    }

    a {
      color: #ebcc43;
      font-weight: 900;
    }

    #are_bg::after {
      background: linear-gradient(180deg, rgba(8, 14, 18, 0) 0, #222 91%);
      content: "";
      display: block;
      height: 80px;
      width: 100%;
      position: absolute;
      bottom: -5px;
      left: 0;
    }
  </style>
</head>

<body>
  <div id="are_bg" style='position:fixed;top:0;left:0;width:100%;z-index:-100;opacity:0.4;'>
    <video src="cdn/are_bg.webm" loop autoplay muted></video>
  </div>
  <?php
  include 'config.php';
  include 'navbar.php'; ?>

  <div class="container mt-3">
    <div class="row justify-content-center">
      <div class="col-10 col-sm-10 col-md-8 col-lg-6">
        <div class="">
          <video style="position:relative;top:0;left:0;width:100%;height:100%" src="cdn/are_bg.webm" autoplay loop muted></video>
        </div>
        <form method="post" action="register.php">
          <?php if (count($errors) > 0) : ?>
            <div class="error">
              <?php foreach ($errors as $error) : ?>
                <p><?php echo $error ?></p>
              <?php endforeach ?>
            </div>
          <?php endif ?>
          <div class="input-group">
            <label>Crea un usuario</label>
            <input type="text" name="username" value="<?php echo $username; ?>">
          </div>
          <div class="input-group">
            <label>Ingresa tu email</label>
            <input type="email" name="email" value="<?php echo $email; ?>">
          </div>
          <div class="input-group">
            <label>Crea una contraseña</label>
            <input type="password" name="password_1">
          </div>
          <div class="input-group">
            <label>Confirma tu contraseña</label>
            <input type="password" name="password_2">
          </div>
          <div class="input-group">
            <button type="submit" class="btn" name="reg_user">¡Registrame!</button>
          </div>
          <p>
            ¿Ya eres parte de AnimeRE? <a href="login.php">Ingresa</a>
          </p>
        </form>
      </div>
    </div>
  </div>
</body>

</html>