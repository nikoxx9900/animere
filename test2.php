<!DOCTYPE html>
<html lang="es">
<head>
  <meta name="keywords" content="AdBlock, Plus, detectar" />
  <meta name="description" content="Detecta si el usuario tiene instalado AdBlock Plus en su navegador." />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Detectar AdBlock Plus en navegador</title>
  <style>
    #ad_banner{width: 100%;text-align:center;}
    .yes {width: 689px;margin: 20px auto;padding: 20px;background: #CF9;border: 1px solid #9C3;}
    .no {width: 689px;margin: 20px auto;padding: 20px;background: #FCC;border: 1px solid #F66;}
  </style>
</head>
<body>
<script type="text/javascript" src="ad_banner.js"></script>
<script type="text/javascript">
      if (document.getElementById("ad_banner") != undefined) {
        document.write('<p class="yes">No se ha detectado AdBlock Plus. Actívalo para ver como se bloquea el anuncio</p>');
      } else {
        document.write('<p class="no">Se ha detectado AdBlock Plus y podemos explicarle brevemente al usuario que la única forma de manetener el sitio es mediante la publicidad y pedirle que desactive AdBlock Plus para esta página. Desactívalo para ver como se muestra el anuncio.</p>');
      }
</script>
</body>
</html>