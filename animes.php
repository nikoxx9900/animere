<?php
define("ROW_PER_PAGE", 20);
require_once('bin/bin/funciones.php');
require_once('bin/core/conexion.php');
require_once('config.php');
require_once('comprobarCookies.php');
$fluid = "";
?>
<html>

<head>
	<meta charset="UTF-8">
	<title>Series | AnimeRE</title>
	<link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<script type="text/javascript" src="js/dpdw.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<style>
		.morecontent span {
			display: none;
			color: #fff;
		}

		.morelink {
			display: block;
			color: #fff;
		}
	</style>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154940141-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-154940141-1');
	</script>

	<script>
		$(document).ready(function() {
			// Configure/customize these variables.
			var showChar = 270; // How many characters are shown by default
			var ellipsestext = "...";
			var moretext = "Ver Mas...>";
			var lesstext = "Ver Menos...";


			$('.more').each(function() {
				var content = $(this).html();

				if (content.length > showChar) {

					var c = content.substr(0, showChar);
					var h = content.substr(showChar, content.length - showChar);

					var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

					$(this).html(html);
				}

			});

			$(".morelink").click(function() {
				if ($(this).hasClass("less")) {
					$(this).removeClass("less");
					$(this).html(moretext);
				} else {
					$(this).addClass("less");
					$(this).html(lesstext);
				}
				$(this).parent().prev().toggle();
				$(this).prev().toggle();
				return false;
			});
		});
	</script>
	<style>
		.tbl-qa {
			width: 100%;
			font-size: 0.9em;
			background-color: #f5f5f5;
		}

		.tbl-qa th.table-header {
			padding: 5px;
			text-align: left;
			padding: 10px;
		}

		.tbl-qa .table-row td {
			padding: 10px;
			background-color: #FDFDFD;
			vertical-align: top;
		}

		.button_link {
			color: #FFF;
			text-decoration: none;
			background-color: #428a8e;
			padding: 10px;
		}

		#keyword {
			border: #CCC 1px solid;
			border-radius: 4px;
			padding: 7px;
			background: url("demo-search-icon.png") no-repeat center right 7px;
		}

		.btn-page {
			margin-right: 10px;
			padding: 5px 10px;
			border: #ebcc43 1px solid;
			background: #222;
			color: #ebcc43;
			border-radius: 4px;
			cursor: pointer;
		}

		.btn-page:hover {
			background: #444;
		}

		.btn-page.current {
			background: #ebcc43;
			color: #222;
		}
	</style>
</head>

<body>
	<?php include 'navbar-ver.php'; ?>
	<?php
	if (isset($_GET['cat'])) {
		$cat = $_GET['cat'];
		if (!empty($_GET['catS'])) {
			$cat = $_GET['cat'];
		}
	}
	$search_keyword = '';
	if (!empty($_POST['search']['keyword'])) {
		$search_keyword = $_POST['search']['keyword'];
	}
	if (!isset($cat)) {
		$sql = 'SELECT * FROM series ORDER BY StrNombre ASC ';
	} else {
		$sql = 'SELECT * FROM series WHERE series.A1="' . $cat . '" OR series.A2="' . $cat . '" OR series.A3="' . $cat . '" OR series.A4="' . $cat . '" OR series.A5="' . $cat . '" ORDER BY StrNombre ASC ';
	}


	/* Pagination Code starts */
	$per_page_html = '';
	$page = 1;
	$start = 0;
	if (!empty($_POST["page"])) {
		$page = $_POST["page"];
		$start = ($page - 1) * ROW_PER_PAGE;
	}
	$limit = " limit " . $start . "," . ROW_PER_PAGE;
	$pagination_statement = $base->prepare($sql);
	$pagination_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
	$pagination_statement->execute();

	$row_count = $pagination_statement->rowCount();
	if (!empty($row_count)) {
		$per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
		$page_count = ceil($row_count / ROW_PER_PAGE);
		if ($page_count > 1) {
			for ($i = 1; $i <= $page_count; $i++) {
				if ($i == $page) {
					$per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page current" />';
				} else {
					$per_page_html .= '<input type="submit" name="page" value="' . $i . '" class="btn-page" />';
				}
			}
		}
		$per_page_html .= "</div>";
	}

	$query = $sql . $limit;
	$pdo_statement = $base->prepare($query);
	$pdo_statement->bindValue(':keyword', '%' . $search_keyword . '%', PDO::PARAM_STR);
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
	?>
	<div class="container">
		<form role="form" method="GET" action="">
			<?php
			$catInf = "Todos";
			if (isset($_GET['cat'])) {
				if ($_GET['cat'] == '') {
					$catInf = "Todos";
				} else {
					$catInf = $_GET['cat'];
				}
			}
			try {
				$sql1 = "SELECT * FROM categorias";
				$resultado1 = $base->prepare($sql1);
				$resultado1->execute(array());
				echo "<div class='space'><select class='space form-control' type='text' name='cat'><option value='0'>Género: " . $catInf . "</option>";
				while ($crow1 = $resultado1->fetch(PDO::FETCH_ASSOC)) {
					echo "<option value='" . $crow1['Nombre'] . "'>" . $crow1['Nombre'] . "</option>";
				}
				echo "</select></div>";
			} catch (Exception $e) {
				echo "Fallo en la base de datos " . $e->getLine();
			}
			if ($crow1['pageVersion'] == "2.0") {
				$pageVersion1 = "serieV2";
			} else {
				$pageVersion1 = "serie";
			}
			?>
			<button name="catS" type="submit" class="btn btn-success">Filtrar</button>
		</form>
	</div>
	<div class="container d-md-none d-lg-none">
		<div class="row justify-content-center">
			<form style="color: #fff0 !important;" class="p-2 w-100">
				<form action="" method="post" class="navbar-form navbar-right" autocomplete="off" id="form-search2">
					<input name="buscar2" id="buscar2" method="post" type="text" class="form-control rounded-0" placeholder="Buscar Anime" style="height:2rem!important;">
					<div id="resultado-q2" style="z-index:1030;position:absolute;"></div>
				</form>

			</form>
		</div>
	</div>
	<form name='frmSearch' action='' method='post'>

		<div class="container">
			<div class="row title justify-content-center">
				<h3 class="mt-2"><i class="fas fa-search"></i> Explorar Series</h3>
			</div>
			<div class="anime-grid row justify-content-sm-center">
				<?php
				if (!empty($result)) {
					foreach ($result as $crow) {
						$nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
						if ($crow['estado1'] == "Finalizado") {
							$color_info = "dc3545";
						} else {
							$color_info = "28a745";
						}
						$fechaFormatInit_aPage1 = $crow['StrFechaEstreno'];
						$fechaFormat_aPage1 = date("d/m/Y", strtotime($fechaFormatInit_aPage1));
						setlocale(LC_ALL, "es_ES");
						$string = $fechaFormat_aPage1;
						$date = DateTime::createFromFormat("d/m/Y", $string);
						$fechaFormat_aPage = strftime("%b %Y", $date->getTimestamp());
						if ($crow['tipo'] == 0) {
							$tipoA = "TV";
						} else if ($crow['tipo'] == 1) {
							$tipoA = "película";
						} else if ($crow['tipo'] == 2) {
							$tipoA = "OVA";
						} else if ($crow['tipo'] == 3) {
							$tipoA = "ONA";
						} else {
							$tipoA = "Especial";
						}

				?>
						<div class="col-are-3 col-5 col-sm col-md col-lg anime-card m-1">
							<div class="card">
								<a href="../../<?php echo $pageVersion1; ?>/<?php echo url($crow["Id"], $nombreFiltrado); ?>">
									<p class="a_description more"><?php echo $crow['StrSinopsis']; ?></p>
									<div class="div_img_s"><img src="<?php echo $crow['StrImagen']; ?>" style="min-width:112px;width:100%;height:auto;" class="card-img-top rounded-0 lazyload" alt="<?php echo $crow['StrNombre']; ?>"></div>
									<div class="are_info_s">
										<h1 style="font-size:1rem;" class="are_s_title pt-1"><?php echo $crow['StrNombre']; ?></h1>
										<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;"><?php echo ucfirst($fechaFormat_aPage); ?> • </span>
										<span style="color:#<?php echo $color_info; ?>;font-size:0.8rem;left:5px;position:relative;"><?php echo $crow['estado1']; ?></span>
										<span style="color:#ebcc43c4;font-size:0.8rem;left:5px;position:relative;"> • <?php echo $tipoA; ?></span>
									</div>
								</a>
							</div>
						</div>
				<?php
					}
				}
				?>
				<?php echo $per_page_html; ?>
			</div>
		</div>
		</div>
	</form>
	<div class="container-fluid" style="background-color:#ebcc43">
		<div class="container d-flex justify-content-center" style="padding:15px;">
			<div id="M439140ScriptRootC383461">
				<div id="M439140PreloadC383461">
					Loading... </div>
				<script>
					(function() {
						var D = new Date(),
							d = document,
							b = 'body',
							ce = 'createElement',
							ac = 'appendChild',
							st = 'style',
							ds = 'display',
							n = 'none',
							gi = 'getElementById',
							lp = d.location.protocol,
							wp = lp.indexOf('http') == 0 ? lp : 'https:';
						var i = d[ce]('iframe');
						i[st][ds] = n;
						d[gi]("M439140ScriptRootC383461")[ac](i);
						try {
							var iw = i.contentWindow.document;
							iw.open();
							iw.writeln("<ht" + "ml><bo" + "dy></bo" + "dy></ht" + "ml>");
							iw.close();
							var c = iw[b];
						} catch (e) {
							var iw = d;
							var c = d[gi]("M439140ScriptRootC383461");
						}
						var dv = iw[ce]('div');
						dv.id = "MG_ID";
						dv[st][ds] = n;
						dv.innerHTML = 383461;
						c[ac](dv);
						var s = iw[ce]('script');
						s.async = 'async';
						s.defer = 'defer';
						s.charset = 'utf-8';
						s.src = wp + "//jsc.adskeeper.co.uk/a/n/animere.net.383461.js?t=" + D.getYear() + D.getMonth() + D.getUTCDate() + D.getUTCHours();
						c[ac](s);
					})();
				</script>
			</div>
			<div id="M439140ScriptRootC383483">
				<script>
					(function() {
						var D = new Date(),
							d = document,
							b = 'body',
							ce = 'createElement',
							ac = 'appendChild',
							st = 'style',
							ds = 'display',
							n = 'none',
							gi = 'getElementById',
							lp = d.location.protocol,
							wp = lp.indexOf('http') == 0 ? lp : 'https:';
						var i = d[ce]('iframe');
						i[st][ds] = n;
						d[gi]("M439140ScriptRootC383483")[ac](i);
						try {
							var iw = i.contentWindow.document;
							iw.open();
							iw.writeln("<ht" + "ml><bo" + "dy></bo" + "dy></ht" + "ml>");
							iw.close();
							var c = iw[b];
						} catch (e) {
							var iw = d;
							var c = d[gi]("M439140ScriptRootC383483");
						}
						var dv = iw[ce]('div');
						dv.id = "MG_ID";
						dv[st][ds] = n;
						dv.innerHTML = 383483;
						c[ac](dv);
						var s = iw[ce]('script');
						s.async = 'async';
						s.defer = 'defer';
						s.charset = 'utf-8';
						s.src = wp + "//jsc.adskeeper.co.uk/a/n/animere.net.383483.js?t=" + D.getYear() + D.getMonth() + D.getUTCDate() + D.getUTCHours();
						c[ac](s);
					})();
				</script>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<h5>Todos derechos reservados <span class="nm-footer">AnimeRE 2019</span>.</h5>
		</div>
	</footer>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
</body>

</html>