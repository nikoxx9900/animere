$(document).ready(function () {

    $("#likes").click(function () {
        $("#likes").removeClass("unclick");
        $("#likes").addClass("clicked", function () {
            $("#dislikes").removeClass("clicked");
            $("#dislikes").addClass("unclick");
        });
    });

    $("#dislikes").click(function () {
        $("#dislikes").removeClass("unclick");
        $("#dislikes").addClass("clicked", function () {
            $("#likes").removeClass("clicked");
            $("#likes").addClass("unclick");
        });

    });

    $('.owl-one').owlCarousel({
        loop: true,
        margin: 3,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });



    $('.owl-two').owlCarousel({
        loop: true,
        margin: 5,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 4
            },
            800: {
                items: 4,
                margin: 4
            },
            1000: {
                items: 5,
                margin: 4
            }
        }
    });

    $('.owl-three').owlCarousel({
        loop: true,
        margin: 3,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });
    $('.owl-user1').owlCarousel({
        loop: false,
        margin: 3,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });
    $('.owl-user2').owlCarousel({
        loop: false,
        margin: 3,
        nav: false,
        dots: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });
    $('.owl-user3').owlCarousel({
        loop: false,
        margin: 3,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2,
                margin: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });
    $('.owl-user4').owlCarousel({
        loop: true,
        margin: 3,
        nav: false,
        autoWidth: false,
        autoplay: false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true,
        items: 5,
        responsive: {
            0: {
                items: 2
            },
            500: {
                items: 3,
                margin: 3
            },
            800: {
                items: 4,
                margin: 3
            },
            1000: {
                items: 5,
                margin: 5
            }
        }
    });
    var showChar1 = 270;
    var ellipsestext1 = "...";
    $('.more').each(function () {
        var content = $(this).html();
        if (content.length > showChar1) {
            var c = content.substr(0, showChar1);
            var h = content.substr(showChar1, content.length - showChar1);
            var html = c + '<span class="moreellipses">' + ellipsestext1 + '&nbsp;</span><span class="morecontent"><span>' + h;
            $(this).html(html);
        }
    });

    $(".carousel-item:first").addClass("active")

    $(".are_anim").hide();
    setTimeout(function () {
        $('.are_anim').slideDown();
    }, 500);


    // CERRAR NOTIFICACION
    var close = document.getElementsByClassName("closebtn");
    var i;
    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function () { div.style.display = "none"; }, 600);
            window.location.href = "http://animere.net/index.php";
        }
    }
});