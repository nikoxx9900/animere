
$(function () {
    $("#form-search").submit(function (e) {
        e.preventDefault();
    });
    $("#buscar").keyup(function () {
        var envio = $("#buscar").val();
        $("#resultado-q").html(
            '<h4><img src="https://renimation.com/img/loading.gif" width="20" height="20" /> Cargando</h4>'
        );
        $.ajax({
            type: "post",
            url: "https://renimation.com/bin/controller/buscarAnime.php",
            data: "buscar=" + envio,
            success: function (respuesta) {
                if (respuesta != "") {
                    $("#resultado-q").html(respuesta);
                }
            }
        });
    });
});
$(function () {
    $("#form-search2").submit(function (e) {
        e.preventDefault();
    });
    $("#buscar2").keyup(function () {
        var envio = $("#buscar2").val();
        $("#resultado-q2").html(
            '<h4><img src="https://renimation.com/img/loading.gif" width="20" height="20" /> Cargando</h4>'
        );
        $.ajax({
            type: "post",
            url: "https://renimation.com/bin/controller/buscarAnime.php",
            data: "buscar2=" + envio,
            success: function (respuesta) {
                if (respuesta != "") {
                    $("#resultado-q2").html(respuesta);
                }
            }
        });
    });
});