<!DOCTYPE html>
<?php
date_default_timezone_set('America/Bogota');
include 'bin/core/conexion.php';
include 'bin/bin/funciones.php';
include 'config.php';
include 'comprobarCookies.php';
include 'visitas.php';
if (isset($_GET['idUser']) && isset($_GET['nombreUser'])) {
    $idUser = $_GET['idUser'];
    $nombreUser = $_GET['nombreUser'];
    try {
        $sql_user = "SELECT * FROM usuarios WHERE usuarios.id = '" . $idUser . "' LIMIT 1";
        $resultado_user = $base->prepare($sql_user);
        $resultado_user->execute(array());
        $crow_user = $resultado_user->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
    }
} else {
    header("Location: https://renimation.com");
    die("No tienes permiso para visitar esta URL sin especificar una identificacion de usuario. Si el problema persiste porfavor contactanos a nuestro email renimationlatam@gmail.com");
}
if (!isset($_SESSION['idUser'])) {
    header("Location: https://renimation.com/login");
}
?>
<html lang="es">

<head>
    <?php include 'header.php'; ?>
</head>

<body>
    <?php include 'navbar.php'; ?>

    <div class="container-fluid p-5" style="background:#ebcc43;">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-3 col-lg-2">
                        <div class="d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start">
                            <img src="<?php echo $crow_user['foto_perfil'] ?>" alt="Perfil de <?php echo $crow_user['Nombre'] ?>" class="img-fluid rounded-circle shadow" style="max-width:150px;object-fit:cover;height:150px;">
                        </div>
                    </div>
                    <div class="col-12 col-sm-8 col-md-9 col-lg-10 mt-3">
                        <div class="d-flex w-100 justify-content-center justify-content-sm-start justify-content-md-start justify-content-lg-start">
                            <h1 class="text-dark mb-0"><strong><?php echo $crow_user['Nombre']; ?></strong></h1>
                        </div>
                        <div class="d-flex w-100 justify-content-center justify-content-sm-start justify-content-md-start justify-content-lg-start">
                            <span class="text-dark"><strong><?php echo $crow_user['bio']; ?></strong></span>
                        </div>
                        <?php
                        if (isset($_SESSION['usuario'])) {
                            if ($_SESSION['idUser'] == $_GET['idUser']) {
                                echo '
                                <div class="d-flex w-100 justify-content-center justify-content-sm-start justify-content-md-start justify-content-lg-start mt-1">
                                    <button href="" class="btn btn-warning rounded-pill mr-2" style="border:3px solid #222;" data-toggle="modal" data-target="#editarUsuario">Editar Perfil</button>
                                </div>

                                <div class="modal fade" id="editarUsuario" tabindex="-1" role="dialog" aria-labelledby="editarUsuarioTitulo" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content" style="background-color:#222;">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editarUsuarioTitulo">Editar Perfil - <strong>' . $nombreUser . '</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="d-flex justify-content-center mb-2">
                                                    <img src="' . $crow_user['foto_perfil'] . '" alt="Perfil de ' . $crow_user['Nombre'] . '" class="img-fluid rounded-circle shadow" style="max-width:150px;object-fit:cover;height:150px;">
                                                </div>
                                                <form method="POST" action="../../form/editUser.php" enctype="multipart/form-data">
                                                    <div class="input-group mb-3">
                                                        <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="foto_perfil">
                                                        <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Subir Foto de Perfil</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="re_descripcion_user">Edita tu Bio</label>
                                                        <textarea name="bio" class="form-control" id="re_descripcion_user" rows="3"></textarea>
                                                    </div>
                                                    <input type="hidden" name="idUser" value="' . $idUser . '"/>
                                                    <input type="hidden" name="nombreUser" value="' . $nombreUser . '"/>
                                                    <button type="submit" class="btn btn-warning mb-2">Guardar Cambios</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                ';
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-2" style="border-bottom:1px solid #ebbc43;">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h4 class="mt-2 text-light pl-2">Últimos Capitulos Vistos</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-user2 mt-2">
                <?php renderCapVistos($idUser); ?>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-2" style="border-bottom:1px solid #ebbc43;">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h4 class="mt-2 text-light pl-2">Ver Luego</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-user1">
                <?php renderVerLuego($idUser); ?>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-2" style="border-bottom:1px solid #ebbc43;">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h4 class="mt-2 text-light pl-2">Favoritos</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-user3 mt-2">
                <?php renderFavoritos($idUser); ?>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/ajax.js"></script>
    <script type="text/javascript" src="../../js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="../../js/animere.js"></script>
</body>

</html>