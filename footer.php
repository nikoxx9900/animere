<footer class="footer">
	<div class="container">
		<div class="row ">
			<div class="d-flex justify-content-center p-2 col-lg-4 col-md-4 col-sm-12">
				<h5>Todos derechos reservados <strong>Renimation <?php echo date("Y"); ?></strong>.</h5>
			</div>
			<div class="d-flex justify-content-center p-2 col-lg-4 col-md-4 col-sm-12">
				<a title="Condiciones de uso" href="condiciones.php" class="btn btn-primary btn-lg rounded-0" role="button" aria-pressed="true" style="background-color:#ebcc4300!important;color:#ebcc43!important;border:1px solid #ebcc43!important;">Condiciones de Uso</a>
			</div>
			<div class="d-flex justify-content-center p-2 col-lg-4 col-md-4 col-sm-12">
				<p style="text-align: center;">Anime online gratis en HD sub español | Renimation no aloja absolutamente ningún video en sus servidores.</p>
			</div>
		</div>
	</div>
</footer>