<!DOCTYPE html>
<?php
include "bin/bin/funciones.php";
include "bin/core/conexion.php";
include 'config.php';
include 'comprobarCookies.php';
$fluid = "";
$id = $_GET['Id'];
$nombre = $_GET['StrNombre'];
$sql = "SELECT * FROM series WHERE Id='" . $id . "'";
$resultado = $base->prepare($sql);
$resultado->execute(array());
$crow = $resultado->fetch(PDO::FETCH_ASSOC);
$url1 = str_replace('../../', 'https://animere.net/', $crow['StrImagenFondo']);
if (isset($_SESSION['usuario'])) {
	$idUserRe = $_SESSION['idUser'];
} else {
	$idUserRe = "0";
}
if ($id == 240) {
	$shigatsu = "shigatsu";
} else {
	$shigatsu = "";
}
if ($crow['pageVersion'] == "2.0") {
	$referer = url($id, $nombre);
	header("Location: https://animere.net/serieV2/$referer");
}
?>
<?php
try {
	$id_bg = $_GET['Id'];
	$nombre_bg = $_GET['StrNombre'];
	$sql_bg = "SELECT * FROM series WHERE Id='" . $id_bg . "'";
	$resultado_bg = $base->query($sql_bg);
	$resultado_bg->execute(array());
	$crow_bg = $resultado_bg->fetch(PDO::FETCH_ASSOC);
} catch (Exception $e) {
	echo "Error en linea: " . $e->getMessage();
}
?>

<html lang="es">

<head>
	<meta charset="UTF-8">
	<title><?php titulo($_GET['StrNombre']); ?></title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta property="fb:app_id" content="1929989280461762" />
	<meta property="og:title" content="<?php titulo($_GET['StrNombre']); ?>">
	<meta property="og:image" content="<?php echo $url1; ?>">
	<meta property="og:description" content="<?php echo json_encode($crow['StrSinopsis']); ?>">
	<meta name="rating" content="General" />
	<meta name="distribution" content="Global" />
	<meta name="classification" content="All" />
	<meta name="googlebot" content="archive">
	<meta name="resource-type" content="document" />
	<link rel="canonical" href="https://animere.net/serie/<?php echo url($crow['Id'], $crow['StrNombre']); ?>" />
	<meta content="AnimeRE <?php echo $crow['StrNombre']; ?> online gratis en hd, ver <?php echo $crow['StrNombre']; ?> gratis online en AnimeRE" name="description" />
	<meta content="<?php echo $crow['StrNombre']; ?> online gratis en hd completo en español, ver <?php echo $crow['StrNombre']; ?> gratis online" name="keywords" />
	<meta content="all, index, follow" name="robots" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="shourtcut icon" type="image/x-icon" href="https://animere.net/img/favicon.png">
	<link rel="stylesheet" type="text/css" href="https://animere.net/css/estilos.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!-- Alertify -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<style>
		.modal-content {
			background-color: #222 !important;
		}

		.favActivo {
			color: indianred !important;
		}

		.re-btn {
			color: #ebcc43;
		}

		.vlActivo {
			color: #ebcc43 !important;
		}

		.re-btn2 {
			color: #fff;
		}
	</style>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154940141-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-154940141-1');
	</script>

</head>

<body class="<?php echo $shigatsu; ?>">
	<div style='position:fixed;height:100%;top:0;left:0;width:100%;z-index:-100;opacity:0.1;background-image: url("<?php echo $crow_bg['StrImagenFondo']; ?>");background-size:cover;background-position:top;'></div>

	<?php
	if (isset($_SESSION["usuario"]) && $_SESSION["admin"] == 1) {
		echo '<div class="d-flex justify-content-center p-3" style="background-color:#b39c2c;">
					<a href="' . $config['base_url'] . 'admin/administracion.php" class="nav-link btn btn-success">' . $_SESSION["usuario"] . ' Panel Admin</a>
					<button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#are-admin">
					Opciones Administrativas
					</button>
					
					</div>

					<!-- Modal -->
					<div class="modal fade" id="are-admin" tabindex="-1" role="dialog" aria-labelledby="are-adminLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="are-adminLabel">Modificar ' . $crow['StrNombre'] . '</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<ul style="background-color:#111;padding:10px;position:relative;z-index: 100;" class="nav justify-content-center">
							<form method="post" action="../../form/refrescar.php" class="m-2">
								<div class="space">
									<select class="space form-control" type="text" name="estado1">
										<option value="Finalizado">Finalizado</option>
										<option value="En Emision">En Emision</option>
									</select>
								</div>
								<input  type="hidden" name="ids" value ="' . $id . '" readonly>
								<input name="estado" type="submit" class="btn btn-success" value="Actualizar">

							</form>
					
					
							<form method="post" action="../../form/img_p.php" enctype="multipart/form-data" class="m-2">
							<div class="box">
								<input type="file" name="imagen" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple/>
								<input  type="hidden" name="id_p" value ="' . $id . '" readonly>
								<input  type="hidden" name="nombre" value ="' . $crow['StrNombre'] . '" readonly>
								<input name="imagen_p" type="submit" class="btn btn-success" value="Imagen Portada">
							</div>
								

							</form>
					
					
							<form method="post" action="../../form/img_f.php" enctype="multipart/form-data" class="m-2">
								<div class="box">
									<input type="file" name="imagen-fondo" id="file-2" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple/>
									<input  type="hidden" name="id_f" value ="' . $id . '" readonly>
									<input  type="hidden" name="nombre1" value ="' . $crow['StrNombre'] . '" readonly>
									<input name="imagen_f" type="submit" class="btn btn-success" value="Imagen Fondo">
								</div>
								

							</form>
					
					</ul>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
						</div>
						</div>
					</div>
					</div>';
	} else {
		echo '';
	}
	?>
	<?php include 'navbar-ver.php'; ?>
	<div class="container mt-2">
		<div id="resultado-q2" style="POSITION: ABSOLUTE;left: 1rem;top:8rem;z-index:1030;"></div>
		<?php
		try {
			$id = $_GET['Id'];
			$nombre = $_GET['StrNombre'];
			$sql = "SELECT * FROM series WHERE Id='" . $id . "'";
			$resultado = $base->query($sql);
			$resultado->execute(array());
			if ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
				if ($crow['estado1'] == "En Emision") {
					$colorE = "text-success";
					$titE = "En Emision";
				} else {
					$colorE = "text-danger";
					$titE = "Finalizado";
				}
				if ($crow['tipo'] == 0) {
					$tipoA = "Serie";
				} else if ($crow['tipo'] == 1) {
					$tipoA = "Pelicula";
				} else if ($crow['tipo'] == 2) {
					$tipoA = "OVA";
				} else if ($crow['tipo'] == 3) {
					$tipoA = "ONA";
				} else {
					$tipoA = "Especial";
				}
				$hiddenCat1 = "style='display:block;'";
				$hiddenCat2 = "style='display:block;'";
				$hiddenCat3 = "style='display:block;'";
				$hiddenCat4 = "style='display:block;'";
				$hiddenCat5 = "style='display:block;'";
				if ($crow['A5'] == 0) {
					$hiddenCat5 = "style='display:none;'";
				} else if ($crow['A4'] == 0) {
					$hiddenCat4 = "style='display:none;'";
				} else if ($crow['A3'] == 0) {
					$hiddenCat3 = "style='display:none;'";
				} else if ($crow['A2'] == 0) {
					$hiddenCat2 = "style='display:none;'";
				} else if ($crow['A1'] == 0) {
					$hiddenCat1 = "style='display:none;'";
				}
			}
		} catch (Exception $e) {
			echo "Error en linea: " . $e->getMessage();
		}
		?>
		<div class='row' itemscope itemtype="http://schema.org/Movie">
			<div class='col-lg-3 col-md-4 col-sm-12 col-12'>
				<div class='anime_imagen2'><img itemprop="image" class='mx-auto d-block' src='<?php echo $crow['StrImagen']; ?>'></div>
				<div>
					<div class="container">
						<?php
						$colorFav = "#ebcc43";
						$colorVerLuego = "#fff";
						$vlIsSet = "";
						$favIsSet = "";
						if (isset($_SESSION['usuario'])) {
							try {
								$sql_fav = "SELECT * FROM favoritos WHERE id_serie='" . $id . "' AND id_usuario = '" . $idUserRe . "'";
								$resultado_fav = $base->query($sql_fav);
								$resultado_fav->execute(array());
								if ($crow_fav = $resultado_fav->fetch(PDO::FETCH_ASSOC)) {
									$colorFav = "favActivo";
									$favIsSet = "1";
								} else {
									$colorFav = "";
									$favIsSet = "0";
								}
							} catch (Exception $e) {
								echo "Error en linea: " . $e->getMessage();
							}
							try {
								$sql_verLuego = "SELECT * FROM verLuego WHERE idSerie='" . $id . "' AND idUser = '" . $idUserRe . "'";
								$resultado_verLuego = $base->query($sql_verLuego);
								$resultado_verLuego->execute(array());
								if ($crow_verLuego = $resultado_verLuego->fetch(PDO::FETCH_ASSOC)) {
									$colorVerLuego = "vlActivo";
									$vlIsSet = "1";
								} else {
									$colorVerLuego = "";
									$vlIsSet = "0";
								}
							} catch (Exception $e) {
								echo "Error en linea: " . $e->getMessage();
							}
						}

						?>
						<div class="row">
							<div id="containerAddFav" class="col-6 d-flex justify-content-center">
								<button id="toggleFav" class="<?php echo $colorFav; ?> like re-btn" style="background:none;border:0;"><i style="font-size:2rem;" class="fas fa-heart"></i></button>
							</div>
							<div class="col-6 d-flex justify-content-center">
								<button id="toggleVl" class="<?php echo $colorVerLuego; ?>  re-btn2" style="background:none;border:0;"><i style="font-size:2rem;" class="fas fa-history"></i></button>
							</div>
						</div>
					</div>
				</div>
				<div class="d-none d-sm-none d-md-none d-lg-block" style="background-color:#222222ad;">
					<div class="d-100 title d-flex justify-content-center are-are">Series Recomendadas</div>
					<div class="container" style="font-size:0.8rem!important;">
						<div class="row">
							<div class="col-6"><?php renderSeries(1, "random", false); ?></div>
							<div class="col-6"><?php renderSeries(1, "random", false); ?></div>
						</div>
						<div class="row">
							<div class="col-6"><?php renderSeries(1, "random", false); ?></div>
							<div class="col-6"><?php renderSeries(1, "random", false); ?></div>
						</div>
					</div>
				</div>
			</div>
			<div class='anime-info col-lg-9 col-md-8 col-sm-12 col-12'>
				<div class='container-fluid'>
					<div class='row d-flex titulo_anime justify-content-lg-left justify-content-sm-left justify-content-md-left justify-content-lg-center'>
						<div style='border-bottom:solid 1px; border-color:#999;max-width:80%;padding-bottom:15px;' class='col-lg-6 col-12 col-sm-12 col-md-12'>
							<h1 itemprop="name" style='color:#ebcc34;font-size:1.8rem;'><strong><?php echo $crow['StrNombre']; ?></strong></h1>
						</div>
						<div style='border-bottom:solid 1px; border-color:#999;max-width:80%;padding-bottom:15px;' class='col-lg-6 col-12 col-sm-12 col-md-12'>
							<div id='estado' class='boton_estado p-2 bd-highlight'>
								<span style='margin-bottom:0.5rem !important; margin:auto;' class='<?php echo $colorE; ?>' href='#' disabled><span style="color:#ffe575;">Estado:</span> <?php echo $titE; ?></span>
							</div>
							<div class='boton_estado p-2 bd-highlight'>
								<span style='margin-bottom:0.5rem !important; margin:auto;' class='' disabled><span style="color:#ffe575;">Estreno:</span><span itemprop="dateCreated"> <?php $fechaFormatInit = $crow['StrFechaEstreno'];
																																														$fechaFormat = date("d M Y", strtotime($fechaFormatInit));
																																														echo $fechaFormat; ?></span>
							</div>
							<div class='boton_estado p-2 bd-highlight'>
								<span style='margin-bottom:0.5rem !important; margin:auto;' class='' disabled><span style="color:#ffe575;">Tipo:</span> <?php echo $tipoA; ?></span> </div>
						</div>
					</div>
					<div class='row d-flex descripcion_anime'>
						<div class='d-flex flex-lg-row flex-md-row flex-sm-row bd-highlight mb-3 info_anime'>
							<div <?php echo $hiddenCat1; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=<?php echo $crow['A1']; ?>'><span itemprop="genre"><?php echo $crow['A1']; ?></span></a></div>
							<div <?php echo $hiddenCat2; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=<?php echo $crow['A2']; ?>'><span itemprop="genre"><?php echo $crow['A2']; ?></span></a></div>
							<div <?php echo $hiddenCat3; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=<?php echo $crow['A3']; ?>'><span itemprop="genre"><?php echo $crow['A3']; ?></span></a></div>
							<div <?php echo $hiddenCat4; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=<?php echo $crow['A4']; ?>'><span itemprop="genre"><?php echo $crow['A4']; ?></span></a></div>
							<div <?php echo $hiddenCat5; ?> class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=<?php echo $crow['A5']; ?>'><span itemprop="genre"><?php echo $crow['A5']; ?></span></a></div>
						</div>
						<div class='col-lg-12 caps_scrollbar'>
							<strong itemprop="about" style='color:#ebcc34; padding-top:15px;'><?php echo $crow['StrSinopsis']; ?></strong>
						</div>
					</div>
					<div class='row caps_anime'>
						<?php include "serie/serieCaps.php"; ?>
					</div>
				</div>
			</div>
		</div>




	</div>
	<?php include "footer.php" ?>
	<script type="text/javascript" src="../../js/jquery.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="../../js/ajax.js"></script>
	<script type="text/javascript" src="../../js/votacion.js"></script>
	<script type="text/javascript">
		var idUserRe = "<?php echo $idUserRe; ?>";
		if (idUserRe == "0") {
			idUserRe = "0";
		} else {
			idUserRe = idUserRe;
		}
		if (idUserRe != "0") {
			var estadoActual = "<?php echo $favIsSet; ?>";
			$("#toggleFav").click(function() {
				if (estadoActual == "0") {
					var favSend = "1";
					$.post("../../bin/controller/addFav.php", {
							fav: favSend,
							id_usuario: idUserRe,
							id_serie: <?php echo $_GET['Id']; ?>
						})
						.done(function(data) {
							estadoActual = "1";
							$("#toggleFav").addClass("favActivo");
							alertify.success('Se agrego a Favoritos');
						});
				} else if (estadoActual == "1") {
					var favSend = "-1";
					$.post("../../bin/controller/addFav.php", {
							fav: favSend,
							id_usuario: idUserRe,
							id_serie: <?php echo $_GET['Id']; ?>
						})
						.done(function(data) {
							estadoActual = "0";
							$("#toggleFav").removeClass("favActivo");
							alertify.error('Se quitó de Favoritos');
						});
				}
			});
			var estadoActualVl = "<?php echo $vlIsSet; ?>";
			$("#toggleVl").click(function() {
				if (estadoActualVl == "0") {
					var vlSend = "1";
					$.post("../../bin/controller/addVl.php", {
							vl: vlSend,
							idUser: idUserRe,
							idSerie: <?php echo $_GET['Id']; ?>
						})
						.done(function(data) {
							estadoActualVl = "1";
							$("#toggleVl").addClass("vlActivo");
							alertify.success('Se agrego a tu lista de Ver Luego');
						});
				} else if (estadoActualVl == "1") {
					var vlSend = "-1";
					$.post("../../bin/controller/addVl.php", {
							vl: vlSend,
							idUser: idUserRe,
							idSerie: <?php echo $_GET['Id']; ?>
						})
						.done(function(data) {
							estadoActualVl = "0";
							$("#toggleVl").removeClass("vlActivo");
							alertify.error('Se quitó de tu lista de Ver Luego');
						});
				}
			});
		} else {
			if (!alertify.registrate) {
				//define a new dialog
				alertify.dialog('registrate', function factory() {
					return {
						main: function(message) {
							this.message = message;
						},
						setup: function() {
							return {
								buttons: [{
									text: "Registrarme",
									key: 27 /*Esc*/
								}],
								focus: {
									element: 0
								},
								options: {
									title: "Renimation",

								}
							};
						},
						prepare: function() {
							this.setContent(this.message);
						}
					}
				});
			}
			$("#toggleFav").click(function() {
				alertify.registrate("Esta funcion solo esta disponible para usuarios registrados. ¿Deseas Registarte ahora?",
					function() {
						alertify.success('Registrarme');
						window.location.href = "http://renimation.com/register";
					},
					function() {
						alertify.error('Te lo recordaremos luego.');
					});
			});
			$("#toggleVl").click(function() {
				alertify.confirm("Renimation", "Esta funcion solo esta disponible para usuarios registrados. ¿Deseas Registarte ahora?",
					function() {
						alertify.success('Registrarme');
						window.location.href = "http://renimation.com/register";
					},
					function() {
						alertify.error('Te lo recordaremos luego.');
					});
			});
		}
	</script>
</body>

</html>