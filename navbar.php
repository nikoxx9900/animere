	<?php
	if (isset($_SESSION['idUser']) && isset($_SESSION['usuario'])) {
		$idUser = $_SESSION['idUser'];
		$nombreUser = $_SESSION['usuario'];
		$rutaUsuario = "usuario/" . $idUser . "/" . $nombreUser;
	} else {
		$rutaUsuario = "login.php?msg=usuarioNoLogeado";
	}
	?>
	<div class="navbar-fixed-are container-fluid ml-0 mr-0 pt-2 pb-2" style="background-color:#222">
		<div class="container<?php if (isset($fluid)) {
									echo $fluid;
								} else {
									echo "";
								} ?>">
			<nav class="navbar navbar-expand navbar-dark bg-bark p-md-0 p-lg-0 row justify-content-center pt-0 pt-sm-0 pt-md-0 pt-lg-0">
				<a class="navbar-brand d-none d-sm-none d-md-block d-lg-block" href="<?php echo $config['base_url']; ?>"><img src="<?php echo $config['base_url']; ?><?php echo $config['dir_img']; ?>r.png" width="140" height=""></a>
				<div class="collapse navbar-collapse justify-content-center" id="animere-navbar">
					<ul class="navbar-nav mr-md-auto mr-lg-auto mt-md-0 mt-lg-0">
						<li class="nav-item mr-0 mr-sm-0 mr-md-2 mr-lg-2 active">
							<a class="text-warning nav-link p-1" href="<?php echo $config['base_url']; ?>">
								<div class="container mt-2">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center"><b class="text-navbar-are"><i class="fas fa-home icon-navbar-are"></i></b></div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center"><b>INICIO</b></div>
									</div>
								</div>
							</a>
						</li>
						<li class="nav-item mr-0 mr-sm-0 mr-md-2 mr-lg-2">
							<a class="text-warning nav-link p-1" href="<?php echo $config['base_url']; ?>animes">
								<div class="container mt-2">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center"><b class="text-navbar-are"><i class="fas fa-list icon-navbar-are"></i></b></div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center"><b>ANIMES</b></div>
									</div>
								</div>
							</a>
						</li>
						<li class="nav-item mr-0 mr-sm-0 mr-md-2 mr-lg-2">
							<a class="text-warning nav-link p-1" href="<?php echo $config['base_url']; ?>emision">
								<div class="container mt-2">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center"><b class="text-navbar-are"><i class="fas fa-tv icon-navbar-are"></i></b></div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center"><b>EMISION</b></div>
									</div>
								</div>
							</a>
						</li>
						<li class="nav-item mr-0 mr-sm-0 mr-md-2 mr-lg-2">
							<a class="text-warning nav-link p-1 d-block d-sm-block d-md-none d-lg-none" href="<?php echo $config['base_url'] . $rutaUsuario; ?>">
								<div class="container mt-2">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center"><b class="text-navbar-are"><i class="fas fa-home icon-navbar-are"></i></b></div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center"><b>BIBLIOTECA</b></div>
									</div>
								</div>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a class="text-warning nav-link" href="https://noticias.animere.net/" tabindex="-1" aria-disabled="true"><b style="font-size:1.3rem !important;" class=""><i class="fas fa-newspaper"></i> NOTICIAS</b></a>
						</li> -->
						<li class="nav-item d-none d-sm-none d-md-none d-lg-block">
							<a class="text-warning nav-link pt-0 pb-0 mt-2" href="https://www.facebook.com/isAnimeRevolution" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class=""><i class=" fab fa-facebook" style="color: rgb(24, 119, 242);"></i></b></a>
						</li>
						<li class="nav-item d-none d-sm-none d-md-none d-lg-block">
							<a class="text-warning nav-link pt-0 pb-0 mt-2" href="https://twitter.com/isREnimatioN" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class=""><i class=" fab fa-twitter" style="color:#00acee;"></i></b></a>
						</li>
					</ul>
					<form class="form-inline d-none d-sm-none d-md-block d-lg-block mr-lg-2" id="buscar1" style="color: #fff0 !important;margin-bottom:0;">
						<form action="" method="post" class="navbar-form navbar-right" autocomplete="off" id="form-search">
							<input name="buscar" id="buscar" method="post" type="text" class="form-control rounded-pill" placeholder="Buscar Anime">
						</form>

					</form>
					<div class="d-none d-sm-none d-md-flex d-lg-flex">
						<?php
						if (isset($_SESSION["usuario"])) {
							echo '
									<div class="dropdown d-none d-sm-none d-md-none d-lg-inline">
										<button class="btn btn-warning dropdown-toggle text-navbar-are p-1 rounded-pill" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span style="font-size: 0.9rem;padding: 10px;"><i class="fas fa-user"></i> ' . $_SESSION["usuario"] . '</span>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="' . $config['base_url'] . 'usuario/' . $_SESSION["idUser"] . '/' . $_SESSION["usuario"] . '">Mi Perfil</a>
											<a class="dropdown-item text-danger" href="' . $config['base_url'] . 'logout.php">Cerrar Sesión</a>
										</div>
									</div>
								';
						} else {
							echo '
									<div class="dropdown">
										<button class="btn btn-warning dropdown-toggle rounded-pill" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<b style="font-size:0.8rem !important;" class="are_nav_t"><i class="fas fa-user"></i></b>
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a href="' . $config['base_url'] . 'login.php" class="dropdown-item are_nav_t"><i class="fas fa-user"></i> Ingresa</a>
											<a href="' . $config['base_url'] . 'register.php" class="dropdown-item are_nav_t"><i class="fas fa-user"></i> Registrate</a>
										</div>
									</div>';
						}
						?>

					</div>
					<div id="resultado-q"></div>
				</div>
			</nav>
		</div>
	</div>
	<?php
	if (isset($_SESSION['usuario'])) {
		$user_link = $config['base_url'] . "usuario/" . $_SESSION['idUser'] . "/" . $_SESSION['usuario'];
	} else {
		$user_link = $config['base_url'] . "login.php";
	}
	?>
	<nav class="navbar navbar-expand navbar-dark d-md-none d-lg-none p-1" style="background-color:#222;">
		<a class="navbar-brand pl-2" href="<?php echo $config['base_url']; ?>">
			<img src="<?php echo $config['base_url']; ?><?php echo $config['dir_img']; ?>r.png" width="100" height="" alt="Mobile Logo">
		</a>
		<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
			<li class="nav-item d-md-none d-lg-none">
				<a class="text-warning nav-link pt-0" href="https://www.facebook.com/isAnimeRevolution" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class=""><i class=" fab fa-facebook" style="color: rgb(24, 119, 242);"></i></b></a>
			</li>
			<li class="nav-item d-md-none d-lg-none">
				<a class="text-warning nav-link pt-0" href="https://twitter.com/isREnimatioN" tabindex="-1" aria-disabled="true"><b style="font-size:1.4rem !important;" class=""><i class=" fab fa-twitter" style="color:#00acee;"></i></b></a>
			</li>
			<li class="nav-item mr-0 mr-sm-0 mr-md-2 mr-lg-2 d-flex">
				<a class="text-warning nav-link p-1" href="<?php echo $user_link; ?>">
					<div class="container">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 d-flex justify-content-center"><b class="text-navbar-are"><i class="fas fa-user icon-navbar-are"></i></b></div>
						</div>
					</div>
				</a>
			</li>
		</ul>
	</nav>