<!--AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../config.php';

$capitulo_init = $_POST['idrel'];
$capitulo = explode(";", $capitulo_init);
$nCap = $_POST['nCap'];
$idrel = $capitulo[0];
$nombre1 = $capitulo[1] . " " . $nCap;
$nombre_link = $nombre1;
$url1 = $_POST['url1'];
$s1 = $_POST['s1'];
$url2 = $_POST['url2'];
$s2 = $_POST['s2'];
$url3 = $_POST['url3'];
$s3 = $_POST['s3'];
$url4 = $_POST['url4'];
$s4 = $_POST['s4'];
$url5 = $_POST['url5'];
$s5 = $_POST['s5'];
$url6 = $_POST['url6'];
$s6 = $_POST['s6'];
$url7 = $_POST['url7'];
$s7 = $_POST['s7'];
$urld = $_POST['urld'];
$s8 = $_POST['s8'];
$urld2 = $_POST['urld2'];
$s9 = $_POST['s9'];
$urld3 = $_POST['urld3'];
$s10 = $_POST['s10'];
$oculto = $_POST['oculto'];
$fecha = $_POST['fecha'];
$args = $_POST['args'];
$enviar_notificacion = $_POST['enviar_notificacion'];
$texto_subtitulo = $_POST['texto_subtitulo'];
$nombre = strtolower(preg_replace('/[\W\s\/]+/', '-', $nombre1));
$nombre_url = $nombre;

if (isset($_FILES)) {
	if (is_array($_FILES) && $_FILES['hls']['size'] > 0) {
		$file_hls = $_FILES['hls']['tmp_name'];
		$fileNewName_hls = $nombre . "_" . $nCap;
		$folderPath_hls = "../../cdn/caps/";
		$folderPath_hls_move = "../cdn/caps/";

		move_uploaded_file($file_hls, $folderPath_hls_move . $fileNewName_hls . ".m3u8");
		$hls = $folderPath_hls . $fileNewName_hls . ".m3u8";
	} else {
		$hls = "";
	}
}

/*PROCESADOR DE IMAGENES*/
if (is_array($_FILES)) {
	$file = $_FILES['imagen']['tmp_name'];
	$sourceProperties = getimagesize($file);
	$fileNewName = $nombre . "_" . date('Y-m-d');
	$folderPath = "../img/capitulos/";
	$ext = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
	$ext_webp = "jpg";
	$imageType = $sourceProperties[2];
	switch ($imageType) {
		case IMAGETYPE_PNG:
			$imageResourceId = imagecreatefrompng($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
			break;
		case IMAGETYPE_JPEG:
			$imageResourceId = imagecreatefromjpeg($file);
			$targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
			imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
			break;

		default:
			echo "Las imagenes solo puedes ser: .JPG o .PNG";
			exit;
			break;
	}


	// move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
	$imagen = "https://renimation.com/img/capitulos/" . $fileNewName . "_thump." . $ext_webp;
	$imagen_noti = "https://renimation.com/img/capitulos/" . $fileNewName . "_thump." . $ext_webp;
}
function imageResize($imageResourceId, $width, $height)
{
	$targetWidth = 720;
	$targetHeight = 405;
	$targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
	imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
	return $targetLayer;
}



if ($idrel != 0) {
	try {
		include '../bin/core/conexion.php';
		$sql = "INSERT INTO capitulos (StrNombre, StrImagen, StrImagenFb, HLS, StrOpcion1, StrOpcion2, StrOpcion3, StrOpcion4, StrOpcion5, StrOpcion6, StrOpcion7,
		 StrOpcionD, StrOpcionD2, StrOpcionD3, IdRel, nCap, oculto, fecha, args,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10) 
		VALUES (:nombre, :image_b64, :imagen, :hls, :url1, :url2, :url3, :url4, :url5, :url6, :url7, :urld, :urld2, :urld3, :idrel, :nCap, :oculto, :fecha, :args, 
		:s1,:s2,:s3,:s4,:s5,:s6,:s7,:s8,:s9,:s10)";

		$resultado = $base->prepare($sql);
		$resultado->execute(array(
			":nombre" => $nombre1, ":image_b64" => $imagen, ":imagen" => $imagen, ":hls" => $hls, ":url1" => $url1, ":url2" => $url2, ":url3" => $url3, ":url4" => $url4, ":url5" => $url5, ":url6" => $url6, ":url7" => $url7,
			":urld" => $urld, ":urld2" => $urld2, ":urld3" => $urld3, ":idrel" => $idrel, ":nCap" => $nCap, ":oculto" => $oculto, ":fecha" => $fecha, ":args" => $args,
			":s1" => $s1, ":s2" => $s2, ":s3" => $s3, ":s4" => $s4, ":s5" => $s5, ":s6" => $s6, ":s7" => $s7, ":s8" => $s8, ":s9" => $s9, ":s10" => $s10
		));
		$resultado->closeCursor();
	} catch (Exception $e) {
		echo "Fallo en la base datos" . $e->getMessage();
	}
} else {
	echo "Tienes que seleccionar una serie a la cual agregar el episodio.";
}
if (isset($enviar_notificacion)) {
	include 'send_noti.php';
	echo "<script>window.location.replace('https://renimation.com/admin/subir-cap.php?envionoti=correcto');</script>";
} else {
	echo "<script>window.location.replace('https://renimation.com/admin/subir-cap.php?envionoti=no');</script>";
}
?>