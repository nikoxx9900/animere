<?PHP
include '../config.php';		
try{
    include '../bin/bin/funciones.php';						
    $sql2="SELECT * FROM capitulos WHERE capitulos.StrNombre = '".$nombre_link."'";
    $resultado2 = $base->prepare($sql2);
    $resultado2->execute(array());
    $crow2=$resultado2->fetch(PDO::FETCH_ASSOC);
    
}catch(Exception $e){
    echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
}

    $url_link = $config['base_url']."ver/". url($crow2['Id'],$nombre_url);
    $img_badge = $config['base_url'].$config['dir_img']."favicon.png";
    if(!empty($texto_subtitulo)){
        $texto_sub = $texto_subtitulo;
    }else{
        $texto_sub = $nombre_link;
    }

function sendMessage($nombre_link1,$texto_sub,$url,$img,$badge) {
    $titulo_noti = $nombre_link1." | Renimation";
    $content      = array(
        'en' => $texto_sub
	);
	$headings      = array(
        "en" => $titulo_noti
    );
    $fields = array(
        'app_id' => "7b9f7426-7299-42b5-83ab-6ae2dd5e6e28",
        'included_segments' => array(
            'All'
        ),
		'contents' => $content,
		'headings' => $headings,
        'url' => $url,
        'chrome_web_image' => $img,
        'chrome_web_badge' => $badge
    );
    
    $fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic OGU2MWQ4YzAtMzhhMy00MTQzLWIwMzUtY2RkZTM5ZWJhMGJj'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    return $response;
}

$response = sendMessage($nombre_link,$texto_sub,$url_link,$imagen_noti,$img_badge);
$return["allresponses"] = $response;
$return = json_encode($return);

$data = json_decode($response, true);
print_r($data);
$id = $data['id'];
print_r($id);

print("\n\nJSON received:\n");
print($return);
print("\n");
