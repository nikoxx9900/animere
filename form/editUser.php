<!--AnimeRE Todos los Derechos reservados -->
<!-- By Subaru -->
<?php
include '../config.php';
include '../bin/core/conexion.php';
$idUser = $_POST['idUser'];
$nombreUser = $_POST['nombreUser'];
$bio = $_POST['bio'];

if ($bio == NULL) {
    try {
        $sql_bio = "SELECT usuarios.bio FROM usuarios WHERE usuarios.id = '" . $idUser . "' LIMIT 1";
        $resultado_bio = $base->prepare($sql_bio);
        $resultado_bio->execute(array());
        if ($crow_bio = $resultado_bio->fetch(PDO::FETCH_ASSOC)) {
            $bioActual = $crow_bio['bio'];
        }
    } catch (Exception $e) {
        echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
    }
    $bio = $bioActual;
} else {
    $bio = $bio;
}
if (!isset($_POST['foto_perfil']) and !isset($_POST['bio'])) {
    header("Location: https://renimation.com/usuario/$idUser/$nombreUser");
} else {
    /*PROCESADOR DE IMAGENES*/
    function imageResize($imageResourceId, $width, $height)
    {
        $targetWidth = $width;
        $targetHeight = $height;
        $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }
    if (is_array($_FILES) && $_FILES['foto_perfil']['size'] > 0) {
        $file = $_FILES['foto_perfil']['tmp_name'];
        $sourceProperties = getimagesize($file);
        $fileNewName = $nombreUser . "_" . date('Y-m-d');
        $folderPath = "../img/usuarios/";
        $ext_webp = pathinfo($_FILES['foto_perfil']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];
        switch ($imageType) {
            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
                break;
            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file);
                $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
                break;

            default:
                echo "Las imagenes solo puedes ser: .JPG o .PNG";
                exit;
                break;
        }
        // move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
        $imagen = "https://renimation.com/img/usuarios/" . $fileNewName . "_thump." . $ext_webp;
    } else {
        try {
            $sql = "SELECT * FROM usuarios WHERE usuarios.id = '" . $idUser . "' LIMIT 1";
            $resultado = $base->prepare($sql);
            $resultado->execute(array());
            if ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
                $imagenActual = $crow['foto_perfil'];
            }
        } catch (Exception $e) {
            echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
        }
        $imagen = $imagenActual;
    }




    try {

        $sql = "UPDATE usuarios SET foto_perfil='" . $imagen . "',bio='" . $bio . "' WHERE id='" . $idUser . "'";

        // Prepare statement
        $stmt = $base->prepare($sql);

        // execute the query
        $stmt->execute();
        header("Location: https://renimation.com/usuario/$idUser/$nombreUser");
    } catch (Exception $e) {
        echo "Fallo en la base datos" . $e->getMessage();
    }
}
?>