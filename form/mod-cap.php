<?php
$id_init = $_POST['id_init'];
$hls_init = $_POST['hls_init'];
$imagen_init = $_POST['imagen_init'];
$nombre1 = $_POST['name'];
$nombre_link = $nombre1;
$url1 = $_POST['url1'];
$s1 = $_POST['ss1'];
$url2 = $_POST['url2'];
$s2 = $_POST['ss2'];
$url3 = $_POST['url3'];
$s3 = $_POST['ss3'];
$url4 = $_POST['url4'];
$s4 = $_POST['ss4'];
$url5 = $_POST['url5'];
$s5 = $_POST['ss5'];
$url6 = $_POST['url6'];
$s6 = $_POST['ss6'];
$url7 = $_POST['url7'];
$s7 = $_POST['ss7'];
$urld = $_POST['urld'];
$s8 = $_POST['ss8'];
$urld2 = $_POST['urld2'];
$s9 = $_POST['ss9'];
$urld3 = $_POST['urld3'];
$s10 = $_POST['ss10'];
$oculto = $_POST['oculto'];
$nCap = $_POST['nCap'];
$args = $_POST['args'];
$nombre = strtolower(preg_replace('/[\W\s\/]+/', '-', $nombre1));
$nombre_url = $nombre;

if (isset($_FILES)) {
    if (is_array($_FILES) && $_FILES['hls']['size'] > 0) {
        $file_hls = $_FILES['hls']['tmp_name'];
        $fileNewName_hls = $nombre . "_" . $nCap;
        $folderPath_hls = "../../cdn/caps/";
        $folderPath_hls_move = "../cdn/caps/";

        move_uploaded_file($file_hls, $folderPath_hls_move . $fileNewName_hls . ".m3u8");
        $hls = $folderPath_hls . $fileNewName_hls . ".m3u8";
    } else {
        $hls = $hls_init;
    }
}

/*PROCESADOR DE IMAGENES*/
if (is_array($_FILES) && $_FILES['imagen']['size'] > 0) {
    $file = $_FILES['imagen']['tmp_name'];
    $sourceProperties = getimagesize($file);
    $fileNewName = $nombre . "_" . date('Y-m-d');
    $folderPath = "../img/capitulos/";
    $ext_webp = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
    $imageType = $sourceProperties[2];
    switch ($imageType) {
        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($file);
            $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
            imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
            break;
        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($file);
            $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
            imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
            break;

        default:
            echo "Las imagenes deben ser JPG o PNG";
            exit;
            break;
    }


    // move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
    $imagen = "https://renimation.com/img/capitulos/" . $fileNewName . "_thump." . $ext_webp;
    $imagen_noti = "https://animere.net/img/capitulos/" . $fileNewName . "_thump." . $ext_webp;
} else {
    $imagen = $imagen_init;
}
function imageResize($imageResourceId, $width, $height)
{
    $targetWidth = 720;
    $targetHeight = 405;
    $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}




try {
    include '../bin/core/conexion.php';
    $sql = "UPDATE capitulos SET StrNombre='" . $nombre1 . "', StrImagen='" . $imagen . "', StrImagenFb='" . $imagen . "', HLS='" . $hls . "', StrOpcion1='" . $url1 . "', StrOpcion2='" . $url2 . "', 
    StrOpcion3='" . $url3 . "', StrOpcion4='" . $url4 . "', StrOpcion5='" . $url5 . "', StrOpcion6='" . $url6 . "', StrOpcion7='" . $url7 . "',
        StrOpcionD='" . $urld . "', StrOpcionD2='" . $urld2 . "', StrOpcionD3='" . $urld3 . "', nCap='" . $nCap . "', oculto='" . $oculto . "', args='" . $args . "'
        ,s1='" . $s1 . "',s2='" . $s2 . "',s3='" . $s3 . "',s4='" . $s4 . "',s5='" . $s5 . "',s6='" . $s6 . "',s7='" . $s7 . "',s8='" . $s8 . "',s9='" . $s9 . "',s10='" . $s10 . "'
        WHERE Id = '" . $id_init . "'";
    $stmt = $base->prepare($sql);
    $stmt->execute();
} catch (Exception $e) {
    echo "Fallo en la base datos" . $e->getMessage();
}
header("Location:" . $_SERVER['HTTP_REFERER']);
