<?php
$comentario = $_POST['comentario'];
$link = $_POST['link'];
$fecha = date("d-m-Y");
/*PROCESADOR DE IMAGENES*/
if (is_array($_FILES) && $_FILES['imagen']['size'] > 0) {
    $file = $_FILES['imagen']['tmp_name'];
    $sourceProperties = getimagesize($file);
    $fileNewName = date('Y-m-d-H-m-s');
    $folderPath = "../img/noticias/";
    $ext = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
    $ext_webp = "jpg";
    $imageType = $sourceProperties[2];
    switch ($imageType) {
        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($file);
            $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
            imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
            break;
        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($file);
            $targetLayer = imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
            imagejpeg($targetLayer, $folderPath . $fileNewName . "_thump." . $ext_webp, 70);
            break;

        default:
            echo "Las imagenes deben ser JPG o PNG";
            exit;
            break;
    }


    // move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
    $imagen = $folderPath . $fileNewName . "_thump." . $ext_webp;
} else {
    $imagen = "0";
}
function imageResize($imageResourceId, $width, $height)
{
    $targetWidth = $width;
    $targetHeight = $height;
    $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}

try {
    include '../bin/core/conexion.php';
    $sql = "INSERT INTO noticias (comentario, link, imagen,fecha) VALUES (:comentario, :link, :imagen,:fecha)";

    $resultado = $base->prepare($sql);
    $resultado->execute(array(
        ":comentario" => $comentario, ":link" => $link, ":imagen" => $imagen, ":fecha" => $fecha
    ));
    $resultado->closeCursor();
} catch (Exception $e) {
    echo "Fallo en la base datos" . $e->getMessage();
}
echo "<script>window.location.replace('https://animere.net/admin/noticias.php?noticia=correcto');</script>";
