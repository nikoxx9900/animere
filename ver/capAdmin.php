 <?php
    $id = $_GET['Id'];
    $sql_adm = "SELECT * FROM capitulos WHERE Id = '" . $id . "'";
    $resultado_adm = $base->prepare($sql_adm);
    $resultado_adm->execute(array());
    $crow_adm = $resultado_adm->fetch(PDO::FETCH_ASSOC);
    ?>

 <div class="d-flex justify-content-center p-3" style="background-color:#b39c2c;">
     <a href="<?php echo $config['base_url'] ?>admin/administracion.php" class="nav-link btn btn-success"><?php $_SESSION["usuario"]; ?> Panel Admin</a>
     <button type="button" class="btn btn-primary ml-2" data-toggle="modal" data-target="#are-admin">
         Modificar Capitulo
     </button>

 </div>

 <!-- Modal -->
 <div class="modal fade" id="are-admin" tabindex="-1" role="dialog" aria-labelledby="are-adminLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content" style="background-color:#222;">
             <div class="modal-header">
                 <h5 class="modal-title" id="are-adminLabel">Modificar <?php echo $crow_adm['StrNombre']; ?></h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="form-group">
                     <form method="post" action="../../form/mod-cap.php" class="subida" enctype="multipart/form-data">
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="name"><i class="fas fa-heading"></i> Nombre del Capitulo</label>
                             <input type="text" class="form-control" id="name" name="name" placeholder="(EJ: One Piece 880 :v)" value="<?php echo $crow_adm['StrNombre']; ?>">
                         </div>
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="nCap"><i class="fas fa-list-ol"></i> Numero del Capitulo</label>
                             <input value="<?php echo $crow_adm['nCap']; ?>" type="text" class="form-control" id="nCap" name="nCap" placeholder="(Ejemplo: 1, 5, 24, 12, etc. solo numeros)">
                         </div>

                         <!--Aqui el input del HLS-->
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="hls"><i class="fab fa-youtube"></i> Opcion AnimeRE (HLS)</label>
                             <input type="file" name="hls" />
                             <input value="<?php echo $crow_adm['HLS']; ?>" type="hidden" name="hls_init" />
                         </div>
                         <!--Aqui las opciones normales con embed -->
                         <b>A partir de aqui, los links deben ser iframes</b>
                         <div class="form-group">

                             <?php
                                $selected = $crow_adm['s1'];
                                players1(1, $selected); ?>
                             <textarea type="text" class="form-control" id="url1" name="url1" placeholder="Escriba url del Video 1"><?php echo htmlspecialchars($crow_adm['StrOpcion1']) ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected2 = $crow_adm['s2'];
                                players1(2, $selected2); ?>
                             <textarea type="text" class="form-control" id="url2" name="url2" placeholder="Escriba url del Video 2"><?php echo $crow_adm['StrOpcion2'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected3 = $crow_adm['s3'];
                                players1(3, $selected3); ?>
                             <textarea type="text" class="form-control" id="url3" name="url3" placeholder="Escriba url del Video 3"><?php echo $crow_adm['StrOpcion3'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected4 = $crow_adm['s4'];
                                players1(4, $selected4); ?>
                             <textarea type="text" class="form-control" id="url4" name="url4" placeholder="Escriba url del Video 4"><?php echo $crow_adm['StrOpcion4'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected5 = $crow_adm['s5'];
                                players1(5, $selected5); ?>
                             <textarea type="text" class="form-control" id="url5" name="url5" placeholder="Escriba url del Video 5"><?php echo $crow_adm['StrOpcion5'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected6 = $crow_adm['s6'];
                                players1(6, $selected6); ?>
                             <textarea type="text" class="form-control" id="url6" name="url6" placeholder="Escriba url del Video 6"><?php echo $crow_adm['StrOpcion6'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected7 = $crow_adm['s7'];
                                players1(7, $selected7); ?>
                             <textarea type="text" class="form-control" id="url7" name="url7" placeholder="Escriba url del Video 7"><?php echo $crow_adm['StrOpcion7'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected8 = $crow_adm['s8'];
                                players1(8, $selected8); ?>
                             <textarea type="text" class="form-control" id="urld" name="urld" placeholder="Escriba url de descarga (Mediafire, Mega, Openload, etc)"><?php echo $crow_adm['StrOpcionD'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected9 = $crow_adm['s9'];
                                players1(9, $selected9); ?>
                             <textarea type="text" class="form-control" id="urld2" name="urld2" placeholder="Escriba url de descarga 2 (Mediafire, Mega, Openload, etc)"><?php echo $crow_adm['StrOpcionD2'] ?></textarea>
                         </div>
                         <div class="form-group">
                             <?php
                                $selected10 = $crow_adm['s10'];
                                players1(10, $selected10); ?>
                             <textarea type="text" class="form-control" id="urld3" name="urld3" placeholder="Escriba url de descarga 3 (Mediafire, Mega, Openload, etc)"><?php echo $crow_adm['StrOpcionD3'] ?></textarea>
                         </div>
                         <!--Aqui el input de la imagen-->
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="imagen"><i class="fas fa-image"></i> Miniatura del Capitulo</label>
                             <input type="file" name="imagen" />
                             <input type="hidden" name="imagen_init" value="<?php echo $crow_adm['StrImagen'] ?>" />
                         </div>
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="oculto" style="color:#222;"><i class="fas fa-eye-slash"></i> Ocultar en Pagina Principal?</label>
                             <?php
                                $oculto1 = "";
                                $oculto2 = "";
                                if ($crow_adm['oculto'] == "0") {
                                    $oculto1 = "selected";
                                } else if ($crow_adm['oculto'] == "1") {
                                    $oculto2 = "selected";
                                }
                                ?>
                             <select class="form-control" type="text" name="oculto">
                                 <option <?php echo $oculto1; ?> value="0">No</option>
                                 <option <?php echo $oculto2; ?> value="1">Si (No aparecera en la pagina principal)</option>
                             </select>
                         </div>
                         <div class="form-group" style="background-color:#ebcc43;color:#222;padding:0.4rem;">
                             <label for="args" style="color:#222;"><i class="fas fa-eye"></i> Multiplicador de Visitas</label>
                             <?php
                                $select1 = "";
                                $select2 = "";
                                $select3 = "";
                                $select4 = "";
                                $select5 = "";
                                $select6 = "";
                                if ($crow_adm['args'] == "0") {
                                    $select1 = "selected";
                                } else if ($crow_adm['args'] == "3") {
                                    $select2 = "selected";
                                } else if ($crow_adm['args'] == "5") {
                                    $select3 = "selected";
                                } else if ($crow_adm['args'] == "10") {
                                    $select4 = "selected";
                                } else if ($crow_adm['args'] == "50") {
                                    $select5 = "selected";
                                } else if ($crow_adm['args'] == "10") {
                                    $select6 = "selected";
                                }
                                ?>
                             <select class="form-control" type="text" name="args">
                                 <option <?php echo $select1; ?> value="0">x1</option>
                                 <option <?php echo $select2; ?> value="3">x3</option>
                                 <option <?php echo $select3; ?> value="5">x5</option>
                                 <option <?php echo $select4; ?> value="10">x10 (Ste men :v)</option>
                                 <option <?php echo $select5; ?> value="50">x50 (Hay gente que solo quiere ver el mundo arder)</option>
                                 <option <?php echo $select6; ?> value="100">x100 (Dios ha muerto)</option>
                             </select>
                         </div>
                         <input type="hidden" name="s1" value="<?php echo $crow_adm['s1']; ?>">
                         <input type="hidden" name="s2" value="<?php echo $crow_adm['s2']; ?>">
                         <input type="hidden" name="s3" value="<?php echo $crow_adm['s2']; ?>">
                         <input type="hidden" name="s4" value="<?php echo $crow_adm['s3']; ?>">
                         <input type="hidden" name="s5" value="<?php echo $crow_adm['s4']; ?>">
                         <input type="hidden" name="s6" value="<?php echo $crow_adm['s5']; ?>">
                         <input type="hidden" name="s7" value="<?php echo $crow_adm['s6']; ?>">
                         <input type="hidden" name="s8" value="<?php echo $crow_adm['s7']; ?>">
                         <input type="hidden" name="s9" value="<?php echo $crow_adm['s8']; ?>">
                         <input type="hidden" name="s0" value="<?php echo $crow_adm['s9']; ?>">
                         <input type="hidden" name="s10" value="<?php echo $crow_adm['s10']; ?>">
                         <input type="hidden" name="id_init" value="<?php echo $_GET['Id']; ?>">

                         <input type="submit" class="btn btn-success" value="Aplicar Cambios">
                     </form>

                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
             </div>
         </div>
     </div>
 </div>