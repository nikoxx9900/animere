<?php
$id = $_GET['Id'];
$nombre = $_GET['StrNombre'];
try {
    $sql = "SELECT capitulos.Id,capitulos.StrNombre,capitulos.StrOpcion1,capitulos.StrImagen,capitulos.IdRel,capitulos.visitas,capitulos.fecha,capitulos.args FROM capitulos INNER JOIN series ON capitulos.Idrel = series.Id WHERE capitulos.id='" . $id . "'";
    $resultado = $base->prepare($sql);
    $resultado->execute(array());
    if ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
        $visita = $crow['visitas'];
        $visitas = $crow['visitas'];
        if ($crow['fecha'] !== "0000-00-00 00:00:00") {
            $fechaFormatInit = $crow['fecha'];
            $fechaFormat = date("d/m/Y", strtotime($fechaFormatInit));
            setlocale(LC_ALL, "es_ES");
            $string = $fechaFormat;
            $date = DateTime::createFromFormat("d/m/Y", $string);
            $fechaFormat = strftime("%e %b %Y", $date->getTimestamp());
            $fecha_subida = "<p style='color:#ffe575 !important;margin-bottom:0rem !important;font-size:0.9rem;display:inline;'>  •  Publicado el " . ucfirst($fechaFormat) . "</p>";
        } else {
            $fecha_subida = "";
        }


        if ($visitas > 0 && $visitas < 500) {
            $visita_c = "<p style='color:#ffe575 !important;margin-bottom:0rem !important;font-size:0.9rem;' class='badge badge-pills badge-are p-0'>" . $visitas . " Visualizaciones</p>";
        } else if ($visitas >= 501) {
            $visita_c = "<p style='color:#ffe575 !important;margin-bottom:0rem !important;font-size:0.9rem;' class='badge badge-pills badge-are p-0'>" . $visitas . " Visualizaciones <i style='color: #ff4500 !important;' class='fab fa-hotjar'></i></p>";
        } else {
            $visita_c = "";
        }
        $cookie_name = $crow['Id'];
        $visto = "";
        if (!isset($_COOKIE[$cookie_name])) {
            $visto = "";
        } else {
            $visto .= "<div class='badge badge-pills badge-are-c ml-2' style='font-size:0.8rem;'><i class='fas fa-check'></i></div>";
        }
    }
} catch (Exception $e) {
    echo "Fallo en la base datos" . $e->getMessage();
}
// AQUI VA EL CONTADOR DE VISITAS
$multiplicador = 1;
if (isset($crow['args'])) {
    $multiplicador = $crow['args'];
}
try {
    $sql = "UPDATE capitulos SET visitas = '" . $visita . "' + '" . $multiplicador . "' WHERE capitulos.Id='" . $id . "'";
    $stmt = $base->prepare($sql);
    $stmt->execute();
} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}
