<?php

$imagenPortada = str_replace(' ', '%20', $crow_cap['StrImagen']);
$cdnHLS = $crow_cap["HLS"];
echo "
    
    <div class='tab-content' id='pills-tabContent'>
        <div class='tab-pane fade active show' id='pills-hls' role='tabpanel' aria-labelledby='pills-hls-tab'>
            <video itemprop='video' id='are_ren' style='width:100%'></video>
        </div>
        <div class='embed-responsive embed-responsive-16by9 tab-pane fade' id='pills-o1' role='tabpanel' aria-labelledby='pills-o1-tab'>

        </div>

        <div class='modal fade' id='descargarModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
            <div class='modal-dialog modal-dialog-centered' role='document'>
                <div class='modal-content' style='background-color:#222!important;'>
                    <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalCenterTitle'><span><i class='fas fa-download'></i> Descargar " . $crow_cap['StrNombre'] . "</span></h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div class='modal-body'>
                        <div>
                            <a target='_blank' style='" . $displayD . "' class='rounded-0 btn btn-primary btn-lg btn-block align-self-center' href='" . $crow_cap['StrOpcionD'] . "'<span class='glyphicon glyphicon-th-list'></span><i class='fas fa-download'></i> " . $crow_cap['s8'] . "</a>
                            <a target='_blank' style='" . $displayD2 . "' class='rounded-0 btn btn-primary btn-lg btn-block align-self-center' href='" . $crow_cap['StrOpcionD2'] . "'<span class='glyphicon glyphicon-th-list'></span><i class='fas fa-download'></i> " . $crow_cap['s9'] . "</a>
                            <a target='_blank' style='" . $displayD3 . "' class='rounded-0 btn btn-primary btn-lg btn-block align-self-center mb-2' href='" . $crow_cap['StrOpcionD3'] . "'<span class='glyphicon glyphicon-th-list'></span><i class='fas fa-download'></i> " . $crow_cap['s10'] . "</a>
                        </div>
                    </div>
                    <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class='nav nav-pills nav-fill' id='pills-tab' role='tablist' style='border-bottom: 1px solid #222;'>
        <li class='nav-item''>
            <a id='pills-hls-tab' data-toggle='pill' href='#pills-hls' role='tab' aria-controls='pills-hls' aria-selected='true' class='nav-are active show rounded-0' style='color:#ebcc34;'><i class='fab fa-youtube'></i> AnimeRE</a>
        </li>
        <li class='nav-item' style='" . $display1 . "'>
            <a id='pills-o1-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o1' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s1']) . "</a>
        </li>
        <li class='nav-item' style='" . $display2 . "'>
            <a id='pills-o2-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o2' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s2']) . "</a>
        </li>
        <li class='nav-item' style='" . $display3 . "'>
            <a id='pills-o3-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o3' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s3']) . "</a>
        </li>
        <li class='nav-item' style='" . $display4 . "'>
            <a id='pills-o4-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o4' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s4']) . "</a>
        </li>
        <li class='nav-item' style='" . $display5 . "'>
            <a id='pills-o6-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o6' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s6']) . "</a>
        </li>
        <li class='nav-item' style='" . $display6 . "'>
            <a id='pills-o7-tab' data-toggle='pill' href='#pills-o1' role='tab' aria-controls='pills-o7' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'>" . ucfirst($crow_cap['s7']) . "</a>
        </li>
        <li class='nav-item'>
            <a id='pills-o5-tab' data-toggle='modal' data-target='#descargarModal' href='#pills-o5' role='tab' aria-controls='pills-o5' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'><i class='fas fa-download'></i> Descargar</a>
        </li>
        <li class='nav-item'>
            <a data-toggle='modal' data-target='#reportarCap' id='pills-rep-tab' data-toggle='pill' href='#rep-o5' role='tab' aria-controls='pills-rep' aria-selected='false' class='nav-are rounded-0' style='color:#ebcc34;'><i class='fas fa-flag'></i> Reportar</a>
        </li>
</ul>

";
