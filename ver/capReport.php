<?php
if (isset($_GET['sub'])) {
    $rep = $_GET['sub'];
    if ($rep == "correcto") {
        $report_true = "<p class='text-success'>Tu Reporte ha sido enviado.</p>";
    } else {
        $report_true = "";
    }
}
?>
<div id="info_capitulo" class="container border-bottom mt-2">
    <div class='row'>
        <div class="col-12">
            <h1 style='color:#ebcc43;margin-bottom:0rem;font-size:1.5rem!important;' class="pl-2 btn_infoSerie"><strong itemprop="name"><?php echo $crow_cap['StrNombre'];
                                                                                                                                        echo " sub español " . $visto; ?></strong></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-7 col-lg-7 pl-2"><?php echo $visita_c;
                                                                echo $fecha_subida; ?></div>
        <style>
            #arefb {
                opacity: 1;
                filter: alpha(opacity=100);
                -webkit-filter: grayscale(100%);
                -moz-filter: grayscale(100%);
                -o-filter: grayscale(100%);
                -ms-filter: grayscale(100%);
                filter: grayscale(100%);
                filter: hue-rotate(190deg) brightness(1.5);
            }
        </style>
        <div class="col-12 col-sm-12 col-md-4 col-lg-5 d-flex justify-content-center justify-content-lg-end">
            <!-- <div id="arefb" class="fb-like" data-href="https://animere.net/ver/<?php // echo url($crow_cap['Id'], $crow_cap['StrNombre']); 
                                                                                    ?>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div> -->
            <?php
            try {
                $sql_votos = "SELECT likes,dislikes FROM capitulos WHERE capitulos.id='" . $id . "'";
                $resultado_votos = $base->prepare($sql_votos);
                $resultado_votos->execute(array());
                $crow_votos = $resultado_votos->fetch(PDO::FETCH_ASSOC);
                $c_likes = $crow_votos['likes'];
                $c_dislikes = $crow_votos['dislikes'];
            } catch (Exception $e) {
                echo "Fallo en la base datos" . $e->getMessage();
            }
            ?>
            <div id="are-votacion" class="d-flex">
                <div class="ml-3 mr-4 mr-lg-2">
                    <div class="col-12"> <button class="like are-vote"><i class="fas fa-thumbs-up"></i></button></div>
                    <div class="col-12 text-center"><span class="likes"><?php echo $c_likes; ?></span></div>
                </div>
                <div class="ml-3 mr-4 mr-lg-2">
                    <div class="col-12"><button class="dislike are-vote"><i class="fas fa-thumbs-down"></i></button></div>
                    <div class="col-12 text-center"><span class="dislikes"><?php echo $c_dislikes; ?></span></div>
                </div>
                <div class="ml-3 mr-4 mr-lg-2">
                    <div class="col-12 d-flex justify-content-center"><button class="are-vote"><i class="fas fa-history"></i></button></div>
                    <div class="col-12"><span style="white-space:nowrap;">Ver Luego</span></div>
                </div>
                <div class="ml-3 mr-4 mr-lg-2">
                    <div class="col-12 d-flex justify-content-center"><button class="are-vote"><i class="fas fa-share-alt"></i></button></div>
                    <div class="col-12"><span>Compartir</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reportarCap" tabindex="-1" role="dialog" aria-labelledby="reportarCap" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background-color:#222!important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reportar Capitulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php include 'report_form.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>