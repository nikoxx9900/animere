<div class="container">
    <div class="row">
        <?php
        try {
            $id = $_GET['Id'];
            $id_cookie = $_GET['Id'];
            $nombre = $_GET['StrNombre'];

            //Obtener información del capítulo actual
            $sql = "SELECT idRel, nCap FROM capitulos WHERE Id='" . $id . "'";
            $resultado = $base->prepare($sql);
            $resultado->execute(array());
            $capData = $resultado->fetch(PDO::FETCH_ASSOC);
            $capIdRel = $capData['idRel'];
            $capNCap = $capData['nCap'];

            //Capitulo anterior
            $sql = "SELECT capitulos.Id,capitulos.StrOpcion1,capitulos.StrImagen,capitulos.IdRel,capitulos.nCap,series.StrNombre FROM capitulos INNER JOIN series ON capitulos.Idrel = series.Id WHERE IdRel = " . $capIdRel . " AND nCap = " . (string) ($capNCap - 1);
            $resultado = $base->prepare($sql);
            $resultado->execute(array());
            $crow = $resultado->fetch(PDO::FETCH_ASSOC);
            $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));

            if ($capNCap == 1) {
                echo "<div class='btn btn-primary col-lg-1 col-md-3 col-sm-3 col-3 btn-block rounded-0'><span class=' pull-right fas fa-chevron-left' disabled></span></div>";
            } else {
                echo "<div class='btn-primary col-lg-1 col-md-3 col-sm-3 col-3 btn-block'><a class='btn btn-block' href='../" . url($crow['Id'], $nombreFiltrado) . "-" . $crow['nCap'] . "'>
                <span class=' pull-right fas fa-chevron-left'></span></a></div>";
            }

            //Lista de Capitulos
            $cod = $_GET['Id'];
            $nombre = $_GET['StrNombre'];

            $sql = "SELECT capitulos.Id,capitulos.StrOpcion1,capitulos.StrImagen,capitulos.IdRel,series.StrNombre,series.pageVersion FROM capitulos INNER JOIN series ON capitulos.Idrel = series.Id WHERE capitulos.id='" . $id . "'";
            $resultado = $base->query($sql);
            $resultado->execute(array());
            if ($crow_capList = $resultado->fetch(PDO::FETCH_ASSOC)) {
                $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow_capList['StrNombre']));
                if ($crow_capList['pageVersion'] == "2.0") {
                    $pageVersion_capList = "serieV2";
                } else {
                    $pageVersion_capList = "serie";
                }
                echo "<div style='padding-left:0;padding-right:0;border-left: solid 1px #290944;border-right:solid 1px #290944;' class='col-lg-10 col-md-6 col-sm-6 col-6'><a class='rounded-0 btn btn-primary btn-block' href='../../" . $pageVersion_capList . "/" . url($crow_capList['IdRel'], $nombreFiltrado) . "'<span class='glyphicon glyphicon-th-list'></span><i class='fas fa-th-list'></i> Lista de Capitulos</a></div>";
            }

            //Capitulo siguiente
            $sql = "SELECT capitulos.Id,capitulos.StrOpcion1,capitulos.StrImagen,capitulos.IdRel,capitulos.nCap,series.StrNombre FROM capitulos INNER JOIN series ON capitulos.Idrel = series.Id WHERE IdRel = $capIdRel AND nCap = " . (string) ($capNCap + 1);
            $resultado = $base->prepare($sql);
            $resultado->execute(array());
            $crow = $resultado->fetch(PDO::FETCH_ASSOC);
            $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
            if ($resultado->rowCount() > 0) {
                echo "<div class='col-lg-1 col-md-3 col-sm-3 col-3 btn-primary btn-block'><a class='btn btn-block' href='../" . url($crow['Id'], $nombreFiltrado) . "-" . $crow['nCap'] . "'><span class=' pull-right fas fa-chevron-right'></span></a></div>";
            } else {
                echo "<div class='col-lg-1 col-md-3 col-sm-3 col-3 btn btn-primary btn-block rounded-0'><span class=' pull-right fas fa-chevron-right' disabled></span></div>";
            }
        } catch (Exception $e) {
            echo "Fallo en la base de datos" . $e->getMessage();
        }
        ?>
    </div>
</div>