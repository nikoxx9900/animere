<div class="container btn_infoSerie">
    <div class="row">
        <div style="background-color:transparent !important;color:#ebcc43 !important;" class="btn btn-primary p-0 mt-2 rounded-0 ml-2"><i class="fas fa-info-circle"></i> Información del Anime <i class="fas fa-chevron-down"></i></div>
    </div>
</div>
<div id="infoSerie" class="container" style="display:none;">

    <?php
    $idRel_capFrame = $crow_cap['IdRel'];
    try {
        $sql_capFrame = "SELECT * FROM series WHERE Id='" . $idRel_capFrame . "'";
        $resultado_capFrame = $base->query($sql_capFrame);
        $resultado_capFrame->execute(array());
        if ($crow_capFrame = $resultado_capFrame->fetch(PDO::FETCH_ASSOC)) {
            if ($crow_capFrame['estado1'] == "En Emision") {
                $colorE_capFrame = "badge badge-pills badge-success";
                $titE_capFrame = "En Emision";
            } else {
                $colorE_capFrame = "badge badge-pills badge-danger";
                $titE_capFrame = "Finalizado";
            }
            if ($crow_capFrame['tipo'] == 0) {
                $tipoA_capFrame = "Serie";
            } else if ($crow_capFrame['tipo'] == 1) {
                $tipoA_capFrame = "Pelicula";
            } else if ($crow_capFrame['tipo'] == 2) {
                $tipoA_capFrame = "OVA";
            } else if ($crow_capFrame['tipo'] == 3) {
                $tipoA_capFrame = "ONA";
            } else {
                $tipoA_capFrame = "Especial";
            }
            $hiddenCat1 = "";
            $hiddenCat2 = "";
            $hiddenCat3 = "";
            $hiddenCat4 = "";
            $hiddenCat5 = "";
            if ($crow_capFrame['A5'] == 0) {
                $hiddenCat5 = "style='display:none;'";
            } else if ($crow_capFrame['A4'] == 0) {
                $hiddenCat4 = "style='display:none;'";
            } else if ($crow_capFrame['A3'] == 0) {
                $hiddenCat3 = "style='display:none;'";
            } else if ($crow_capFrame['A2'] == 0) {
                $hiddenCat2 = "style='display:none;'";
            } else if ($crow_capFrame['A1'] == 0) {
                $hiddenCat1 = "style='display:none;'";
            }
            $nombreFiltrado_capFrame = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow_capFrame['StrNombre']));
            if ($crow_capFrame['pageVersion'] == "2.0") {
                $pageVersion_capFrame = "serieV2";
            } else {
                $pageVersion_capFrame = "serie";
            }

            echo "
                        <div class='row'>
                        <div class='col-lg-3 col-md-3 col-sm-12'>
                            <div class='anime_imagen_capFrame'><img class='mx-auto d-block mt-3 w-100' src='" . $crow_capFrame['StrImagen'] . "'></div>
                        </div>
                        <div class='anime-info col-lg-9 col-md-9 col-sm-12'>
                            <div class='container-fluid'>
                                <div class='row d-flex titulo_anime'>
                                    <div style='border-bottom:solid 2px; border-color:#fff;max-width:80%;padding-bottom:15px;' class='col-lg-12'>
                                        <a href='https://renimation.com/" . $pageVersion_capFrame . "/" . url($crow_cap['IdRel'], $nombreFiltrado_capFrame) . "'><h1 style='color:#ebcc34;' itemprop='alternateName'>" . $crow_capFrame['StrNombre'] . "</h1></a>
                                    </div>
                                </div>
                                <div class='row d-flex descripcion_anime'>
                                <div class='col-lg-12 d-flex justify-content-sm-center justify-content-center justify-content-md-center justify-content-lg-start'>
                                        <div id='estado' class='are-in boton_estado p-2 bd-highlight'><p style='margin-bottom:0.5rem !important; margin:auto;' class='" . $colorE_capFrame . "' href='#' disabled>" . $titE_capFrame . "</p></div>
                                        <div class='are-in boton_estado p-2 bd-highlight'><p style='margin-bottom:0.5rem !important; margin:auto;' class='badge badge-pills badge-warning' disabled>Estreno: <span itemprop='datePublished'>" . $crow_capFrame['StrFechaEstreno'] . "</span></p></div>
                                        <div class='are-in boton_estado p-2 bd-highlight'><p style='margin-bottom:0.5rem !important; margin:auto;' class='badge badge-pills badge-warning' disabled>" . $tipoA_capFrame . "</p></div>
                                    </div>
                                <div class='d-flex flex-lg-row flex-md-row flex-sm-row bd-highlight mb-1 info_anime'>
                                    <div " . $hiddenCat1 . " class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=" . $crow_capFrame['A1'] . "'><span itemprop='genre'>" . $crow_capFrame['A1'] . "</span></a></div>
                                    <div " . $hiddenCat2 . " class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=" . $crow_capFrame['A2'] . "'><span itemprop='genre'>" . $crow_capFrame['A2'] . "</span></a></div>
                                    <div " . $hiddenCat3 . " class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=" . $crow_capFrame['A3'] . "'><span itemprop='genre'>" . $crow_capFrame['A3'] . "</span></a></div>
                                    <div " . $hiddenCat4 . " class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=" . $crow_capFrame['A4'] . "'><span itemprop='genre'>" . $crow_capFrame['A4'] . "</span></a></div>
                                    <div " . $hiddenCat5 . " class='p-2 bd-highlight'><a class='are_cat' href='https://renimation.com/animes.php?cat=" . $crow_capFrame['A5'] . "'><span itemprop='genre'>" . $crow_capFrame['A5'] . "</span></a></div>
                                </div>
                                    <div class='col-lg-12 caps_scrollbar' style='height:100px;overflow:auto;'>
                                        <p style='color:#ebcc34;' itemprop='about'>" . $crow_capFrame['StrSinopsis'] . "</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    ";
        }
    } catch (Exception $e) {
        echo "Error en linea: " . $e->getMessage();
    }
    ?>
    <div class="row">
        <?php
        $idRel_CapInfo = $crow_cap['Id'];
        try {
            $sql_CapInfo = "SELECT * FROM capitulos WHERE Id='" . $idRel_CapInfo . "'";
            $resultado_CapInfo = $base->query($sql_CapInfo);
            $resultado_CapInfo->execute(array());
            $crow_CapInfo = $resultado_CapInfo->fetch(PDO::FETCH_ASSOC);
            echo '
        <div class="container">
            <div class="row">
                <span><img class="p-2" itemprop="image" width="200" src="../' . $crow_CapInfo['StrImagen'] . '"/>
                <span>Episodio Numero: <span itemprop="episodeNumber">' . $crow_CapInfo['nCap'] . '</span></span>
            </div>
        </div>
        ';
        } catch (Exception $e) {
            echo "Error en linea: " . $e->getMessage();
        }
        ?>

    </div>
</div>