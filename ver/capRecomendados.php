<?php
try {
    $sql = "SELECT * FROM capitulos WHERE capitulos.oculto = 0 AND capitulos.fecha >= curdate() - INTERVAL DAYOFWEEK(curdate())+7 DAY
        AND capitulos.fecha < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY ORDER BY RAND() DESC LIMIT 15";
    $resultado = $base->prepare($sql);
    $resultado->execute(array());
    while ($crow = $resultado->fetch(PDO::FETCH_ASSOC)) {
        $visitas = $crow['visitas'];
        $fecUp = time_elapsed_string($crow['fecha']);
        if ($visitas > 0 && $visitas < 100) {
            $visita = "<span class='vistas-cap-badge badge-pills badge-are'>" . $visitas . " vistas  •  </span>";
        } else if ($visitas > 100) {
            $visita = "<span class='vistas-cap-badge badge-pills badge-are'>" . $visitas . " vistas  •  </span>";
        } else {
            $visita = "";
        }
        $nombreFiltrado = strtolower(preg_replace('/[\W\s\/]+/', '-', $crow['StrNombre']));
        $infoCap = "Capitulo " . $crow['nCap'];
        echo "
            <div class='dflex col-12 col-sm-12 col-md-12 col-lg-12 ml-lg-4 are_mobile p-0' style='margin-left:5px;margin-right:5px;'>
            <div class='container capitulo_are'>
            <a style='text-decoration:none !important;' class='row' href='../" . url($crow['Id'], $nombreFiltrado) . "'>
                <div class='col-6 img-fluid-are-div'><img  class='img-fluid-are' src='" . $crow['StrImagen'] . "'></div>
                <div class='col-6 title_cap m-0'>
                <div class='col-12'><h5 class='more3 are-h5'>" . $crow['StrNombre'] . "</h5></div>
                <div class='col'>" . $visita . "<span class='cap-badge'>" . $fecUp . "</span> <span class='cap-badge badge-pills badge-are'>  •  " . $infoCap . "</span></div>
                </div>
            </a>
            </div>
        </div>				
        ";
    }
} catch (Exception $e) {
    echo "Error temporal, por favor reporta esto a un Administrador" . $e->getMessage();
}
